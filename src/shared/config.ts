const config = {
  CELERITY_SERVER_PORT: 4503,
  WEBSERVER_PORT: 4505,

  /**
   * The timeout until a response by the WebSocket API is expected, in ms.
   */
  API_REQUEST_TIMEOUT: 5000,

  DISABLE_SAMPLING: true,
  SAMPLE_CONSTANT: 2000,
  SAMPLE_FACTOR: 1,
  SAMPLE_MIN_EXEC_TIME_RATIO: 0.6,
};

export default config;
