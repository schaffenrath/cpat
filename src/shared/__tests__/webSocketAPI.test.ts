import http from "http";

import io from "socket.io";
import socketIOClient, { Socket } from "socket.io-client";

import {
  NumberType,
  StringType,
  ArrayType,
  ObjectType,
  WebSocketAPI,
} from "../webSocketAPI";

// FIXME: We need to ensure every test suite uses a different port.
// => We should just mock the sockets altogether.
const TEST_WEBSOCKET_PORT = 7357;

const TEST_TIMEOUT = 500;

class CustomType {
  foo: number;
  bar: string[];
  constructor(foo: number, bar: string[]) {
    this.foo = foo;
    this.bar = bar;
  }
}

// NOTE We only test in one direction (client emits to server),
// but the two sides are implemented through the exact same class.
const apiConfig = {
  serverEvents: {},

  clientEvents: {
    myFirstEvent: {
      args: {
        myFirstArg: new NumberType(),
        mySecondArg: new StringType(),
      },
    },
    anotherEvent: {
      args: {
        arrayArg: new ArrayType<number>(),
      },
      returnType: new NumberType(),
    },
    customTypeEvent: {
      args: {
        customTypeArg: new ObjectType<CustomType>(),
      },
      returnType: new ObjectType<CustomType>(),
    },
  },
};

describe("WebSocketAPI", () => {
  let server: http.Server;
  let clientToServerSocket: typeof Socket;
  let apiServer: WebSocketAPI<
    typeof apiConfig["clientEvents"],
    typeof apiConfig["serverEvents"]
  >;
  let apiClient: WebSocketAPI<
    typeof apiConfig["serverEvents"],
    typeof apiConfig["clientEvents"]
  >;

  beforeEach((done) => {
    server = http.createServer();
    const ioServer = io(server, {
      serveClient: false,
      cookie: false,
    });
    server.listen(TEST_WEBSOCKET_PORT);
    ioServer.on("connection", (socket: io.Socket) => {
      apiServer = new WebSocketAPI(
        apiConfig.clientEvents,
        apiConfig.serverEvents,
        socket,
        { timeout: TEST_TIMEOUT }
      );
    });

    clientToServerSocket = socketIOClient.connect(
      `http://localhost:${TEST_WEBSOCKET_PORT}`
    );
    clientToServerSocket.once("connect", () => {
      apiClient = new WebSocketAPI(
        apiConfig.serverEvents,
        apiConfig.clientEvents,
        clientToServerSocket,
        { timeout: TEST_TIMEOUT }
      );
      done();
    });
  });

  afterEach((done) => {
    clientToServerSocket.once("disconnect", () => {
      done();
    });
    clientToServerSocket.disconnect();
    server.close();
  });

  test("allows to register event handlers", async () => {
    expect.assertions(2);
    apiServer.on("myFirstEvent", (args) => {
      const first: number = args.myFirstArg;
      const second: string = args.mySecondArg;
      expect(first).toEqual(42);
      expect(second).toEqual("hello");
    });
    await apiClient.emit("myFirstEvent", {
      myFirstArg: 42,
      mySecondArg: "hello",
    });
  });

  test("throws when trying to register a handler for an unknown event", () => {
    expect(() => {
      // @ts-ignore
      apiServer.on("doesNotExist", () => {});
    }).toThrow('Cannot register handler for unknown event "doesNotExist".');
  });

  test("optionally supports return types", async () => {
    apiServer.on("anotherEvent", (args) => {
      return args.arrayArg[0] + args.arrayArg[3];
    });
    const response = await apiClient.emit("anotherEvent", {
      arrayArg: [1, 2, 3, 4],
    });
    expect(response).toEqual(5);
  });

  test("supports custom argument and return types", async () => {
    apiServer.on("customTypeEvent", (args) => {
      const result = args.customTypeArg;
      result.foo += 10;
      result.bar.push("cool");
      return result;
    });
    const response = await apiClient.emit("customTypeEvent", {
      customTypeArg: new CustomType(23, ["this", "is"]),
    });
    expect(response.foo).toEqual(33);
    expect(response.bar).toEqual(["this", "is", "cool"]);
  });

  test(
    "throws if no response is received within a given timeout",
    async () => {
      expect.assertions(1);
      try {
        await apiClient.emit("anotherEvent", {
          arrayArg: [],
        });
      } catch (e) {
        expect(e).toEqual(
          new Error(
            `No response received for event "anotherEvent" within timeout (${TEST_TIMEOUT}ms).`
          )
        );
      }
    },
    2 * TEST_TIMEOUT
  );

  test("returns errors thrown on the handler side back to the emitter", async () => {
    expect.assertions(1);
    apiServer.on("myFirstEvent", () => {
      throw new Error("This is a server error");
    });
    try {
      await apiClient.emit("myFirstEvent", {
        myFirstArg: 42,
        mySecondArg: "hello",
      });
    } catch (e) {
      expect(e).toEqual(new Error("This is a server error"));
    }
  });

  test("throws when trying to emit an invalid argument", async () => {
    expect.assertions(1);
    try {
      await apiClient.emit("myFirstEvent", {
        // @ts-ignore
        myFirstArg: "fortytwo",
        mySecondArg: "",
      });
    } catch (e) {
      expect(e).toEqual(
        new Error(
          'Trying to emit an invalid argument "myFirstArg" for event "myFirstEvent".'
        )
      );
    }
  });
});
