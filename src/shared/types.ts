import { AccessMode } from "./__generated__/trace";
import { CommandType } from "./__generated__/trace";
import { DependencyKind } from "./__generated__/trace";
import { ExecutionTarget } from "./__generated__/trace";

export { AccessMode } from "./__generated__/trace";
export { CommandType } from "./__generated__/trace";
export { DependencyKind } from "./__generated__/trace";
export { ExecutionTarget } from "./__generated__/trace";

// ==================== General types ====================

export type RunId = string;
export type NodeId = number;
export type JobId = number;
export type TaskId = number;
export type CommandId = number;
export type BufferId = number;
export type KernelId = string;

export type JobIdsOfNode = Array<JobId>;
export type JobVisLinesOfNode = number;

export type Dict<T> = { [key: number]: T };
export type RunDict = { [key: string]: CelerityRun };

// TODO: Consider making these true phantom types, like so
// type CommandId = Number & {
//   __commandid__: never;
// };

export type JobType = keyof CmdValuePerType;

export type DependencyType = "Job" | "Command" | "Task";
export type DependencyDirection = "Dependee" | "Depender";

export interface DependencyInfo {
  runId: RunId;
  targetId: JobId;
  inRange: TimeSpan;
  requestType: DependencyType;
  dependencyDirection: DependencyDirection;
  node: NodeId;
}

export type BackendStatus =
  | "Unknown"
  | "Initial"
  | "Ready"
  | "Receiving"
  | "Processing"
  | "Transferring"
  | "MultipleRunsAvailable";

// TODO: Add more cases.
export type BackendErrorMsg = "Clear" | "Unknown" | "ConnectionInterrupted";

export interface RunIdWithName {
  id: RunId;
  name: string;
}

export interface TimeSpan {
  start: number;
  end: number;
}

export interface Point {
  x: number;
  y: number;
}

// ==================== Complete run types ====================

export interface PollStatistic {
  minDuration: number;
  maxDuration: number;
  avgDuration: number;
  sampleCount: number;
}

export interface Job {
  commandId: CommandId;
  startTime: number;
  finishTime: number;
  submitTime: number;
  submittedTime: number;
  pollStatistic: PollStatistic | null;
  lineInVis: number;
}

export interface Dependency<T extends CommandId | TaskId> {
  id: T;
  kind: DependencyKind;
}

export interface Task {
  name: string;
  target: ExecutionTarget;
  dependers: Array<Dependency<TaskId>>;
  dependees: Array<Dependency<TaskId>>;
}

export interface Subrange {
  min: number[];
  max: number[];
}

export interface BufferAccess {
  id: BufferId;
  range: Subrange;
  mode: AccessMode;
}

export interface Command {
  nodeId: NodeId;
  type: CommandType;
  // Only relevant for TASK commands
  taskId: TaskId | null;
  kernelId: string | null;
  // Only relevant for transfer commands
  otherNode: NodeId | null;
  // Only relevant for transfer commands
  bufferId: BufferId | null;
  // Only relevant for TASK commands
  executionRange: Subrange | null;
  bufferAccesses: Array<BufferAccess>;
  dependers: Array<Dependency<CommandId>>;
  dependees: Array<Dependency<CommandId>>;
}

// TODO: Add the additional run meta info we have within the trace
export interface CelerityRun {
  id: RunId;
  executableName: string;
  // These times are relative to some arbitrary reference time point and cannot
  // be used to align/compare/synchronize different runs in any meaningful way.
  // TODO: We might as well shift every run to start at 0 and get rid of the startTime.
  startTime: number;
  endTime: number;
  jobVisLines: Dict<JobVisLinesOfNode>;
  kernels: Array<KernelId>;
}

// ==================== Statistic types ====================

export interface CmdValuePerType {
  hostTask: number;
  deviceTask: number;
  push: number;
  awaitPush: number;
  horizon: number;
}

export type KernelExecTime = { [key: string]: number };
export interface KernelExecTimeOfTarget {
  device: KernelExecTime;
  host: KernelExecTime;
}

export interface StatisticPackage {
  runId: RunId;
  nodeJobCount: Array<CmdValuePerType>;
  totalJobCount: CmdValuePerType;
  nodeExecTime: Array<CmdValuePerType>;
  totalExecTime: CmdValuePerType;
  nodeKernelExecTime: Array<KernelExecTimeOfTarget>;
  totalKernelExecTime: KernelExecTimeOfTarget;
  nodeCmdTypeQuota: Array<CmdValuePerType>;
  totalCmdTypeQuota: CmdValuePerType;
  nodeAvgExec: Array<CmdValuePerType>;
  totalAvgExec: CmdValuePerType;
  cmdDeviation: CmdValuePerType;
}

interface JsonRunMetrics {
  Rank: string;
  InitialIdle: string;
  DeviceIdle: string;
  Starvation: string;
}

export interface MetricPackage {
  Content: string;
  Metrics: JsonRunMetrics[];
  Ranks: number;
  TaskCount: string;
}

// ==================== Sampling types ====================

export type ExecTimeOfKernel = { [key: string]: number };

export interface SampledJob {
  jobIdList: Array<JobId>;
  execSpan: TimeSpan;
  lineInVis: number;
  kernels: ExecTimeOfKernel;
  typeRatio: CmdValuePerType;
}

export interface FrameList<T> {
  frameLength: number;
  frames: Array<T>;
}

export type JobDict = Dict<Job>;
export type CommandDict = Dict<Command>;
export type TaskDict = Dict<Task>;

export type SampledDict = Dict<SampledJob>;

export interface JobFrame {
  jobs: JobDict;
  commands: CommandDict;
  tasks: TaskDict;
}

export type JobFrameDict = { [key: number]: JobFrame };
export type SampledFrameDict = { [key: number]: SampledDict };

export type JobFramesOfNode = { [key: number]: JobFrameDict };
export type SampledFramesOfNode = { [key: number]: SampledFrameDict };

interface TemplateFrames<T> {
  runId: RunId;
  baseFrameLength: number;
  frameLength: number;
  isSampled: boolean;
  framesPerNode: T;
}

export type JobFramePayload = TemplateFrames<JobFramesOfNode>;
export type SampledFramePayload = TemplateFrames<SampledFramesOfNode>;
