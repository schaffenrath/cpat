/**
 * This file specifies the shared WebSocket API between client and server,
 * ensuring type safety (statically and at run time) on both sides.
 */
import io from "socket.io";
import { Socket as ClientToServerSocket } from "socket.io-client";

import {
  CelerityRun,
  StatisticPackage,
  RunIdWithName,
  JobFramePayload,
  SampledFramePayload,
  TimeSpan,
  DependencyInfo,
} from "../shared/types";
import { BackendErrorMsg } from "../shared/types";

import {
  WebSocketAPI,
  NumberType,
  StringType,
  BufferType,
  ArrayType,
  ObjectType,
  makeAPIClient,
  makeAPIServer,
} from "./webSocketAPI";

export const celerityWebSocketAPIConfig = {
  serverEvents: {
    CelerityRunInfo: {
      args: {
        celerityRun: new ObjectType<CelerityRun>(),
      },
    },
    StatisticData: {
      args: {
        statistics: new ObjectType<StatisticPackage>(),
      },
    },
    AvailableRuns: {
      args: {
        availableRuns: new ArrayType<RunIdWithName>(),
      },
    },
    JobFrames: {
      args: {
        jobFrames: new ObjectType<JobFramePayload>(),
      },
    },
    SampledFrames: {
      args: {
        sampledFrames: new ObjectType<SampledFramePayload>(),
      },
    },
    BackendStatus: {
      args: {
        status: new StringType(),
      },
    },
    BackendError: {
      args: {
        errorMessage: new ObjectType<BackendErrorMsg>(),
      },
    },
  },
  clientEvents: {
    DroppedData: {
      args: {
        data: new BufferType(),
        totalDataSize: new NumberType(),
      },
    },
    RequestSingleTimeFrame: {
      args: {
        runId: new StringType(),
        viewSpan: new ObjectType<TimeSpan>(),
        sampleLevel: new NumberType(),
      },
    },
    RequestTimeFrames: {
      args: {
        runId: new StringType(),
        viewSpan: new ObjectType<TimeSpan>(),
      },
    },
    RequestSpecificTimeFrames: {
      args: {
        requestInfo: new ObjectType<DependencyInfo>(),
      },
    },
    RequestStatistics: {
      args: {
        runId: new StringType(),
        viewSpan: new ObjectType<TimeSpan>(),
      },
    },
    RequestListOfAvailableRuns: {
      args: {},
    },
    RequestRunInfo: {
      args: {
        runId: new StringType(),
        windowWidth: new NumberType(),
      },
    },
    RequestRunDeletion: {
      args: {
        runId: new StringType(),
      },
    },
  },
};

export function makeCelerityAPIServer(socket: io.Socket) {
  // TODO: Not sure why we need to cast here... (otherwise it infers the generic APIConfig type)
  return makeAPIServer(celerityWebSocketAPIConfig, socket) as WebSocketAPI<
    typeof celerityWebSocketAPIConfig["clientEvents"],
    typeof celerityWebSocketAPIConfig["serverEvents"]
  >;
}

export function makeCelerityAPIClient(socket: typeof ClientToServerSocket) {
  return makeAPIClient(celerityWebSocketAPIConfig, socket) as WebSocketAPI<
    typeof celerityWebSocketAPIConfig["serverEvents"],
    typeof celerityWebSocketAPIConfig["clientEvents"]
  >;
}
