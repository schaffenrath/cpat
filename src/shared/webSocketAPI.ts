/**
 * This file provides the framework that enables the specification
 * of a shared WebSocket API between client and server.
 */
import io from "socket.io";
import { Socket as ClientToServerSocket } from "socket.io-client";

import config from "./config";

export interface TypeValidator<T = any> {
  // NOTE: While this type should probably be "any", we have to use T
  // somewhere to make type inference work.
  validate(value: T): boolean;
}

export class StringType implements TypeValidator<string> {
  validate(value: string) {
    return typeof value === "string";
  }
}

export class NumberType implements TypeValidator<number> {
  validate(value: number) {
    return typeof value === "number";
  }
}

// Buffers are a bit special in that they are sent as ArrayBuffer from the browser,
// but received as NodeJS Buffer on the server.
export class BufferType implements TypeValidator<Buffer | ArrayBuffer> {
  validate(value: Buffer | ArrayBuffer) {
    return value instanceof Buffer || value instanceof ArrayBuffer;
  }
}

export class ArrayType<T> implements TypeValidator<T[]> {
  validate(value: T[]) {
    return typeof value === "object" && Array.isArray(value);
  }
}

export class ObjectType<T> implements TypeValidator<T> {
  validate(value: T) {
    return typeof value === "object" && !Array.isArray(value);
  }
}

type UnpackType<V> = V extends TypeValidator<infer T> ? T : never;

interface Event {
  args: {
    [name: string]: TypeValidator;
  };
  // Don't set this if the event has no return type
  returnType?: TypeValidator;
}

type EventConfig = {
  [name: string]: Event;
};

export interface APIConfig {
  // Events emitted by the server to a client
  serverEvents: EventConfig;
  // Events emitted by a client to the server
  clientEvents: EventConfig;
}

type EventNames<Events extends EventConfig> = Extract<keyof Events, string>;

type EventArgs<E extends Event> = {
  [A in keyof E["args"]]: UnpackType<E["args"][A]>;
};

type EventReturnType<E extends Event> = E["returnType"] extends TypeValidator
  ? UnpackType<E["returnType"]>
  : void;

type EventHandlerFn<E extends Event> = (
  args: EventArgs<E>
) => EventReturnType<E>;

type SocketIOSocket = io.Socket | typeof ClientToServerSocket;

interface Response<T> {
  result?: T;
  error?: string;
}

export class WebSocketAPI<
  HandleEvents extends EventConfig,
  EmitEvents extends EventConfig
> {
  private handleEventsConfig: HandleEvents;
  private emitEventsConfig: EmitEvents;
  private socket: SocketIOSocket;
  private timeout: number;

  /**
   * @param handleConfig The events this class can listen on().
   * @param emitConfig The events this class can emit().
   */
  constructor(
    handleConfig: HandleEvents,
    emitConfig: EmitEvents,
    socket: SocketIOSocket,
    options: {
      timeout: number;
    }
  ) {
    this.handleEventsConfig = handleConfig;
    this.emitEventsConfig = emitConfig;
    this.socket = socket;
    this.timeout = options.timeout;

    // TODO: Unfortunately the client doesn't support this in socket.io 2.x
    if (typeof (this.socket as io.Socket)["use"] === "function") {
      // TODO: In socket.io 3.0 this needs to be replaced by socket.onAny (which is also supported by client!)
      // @ts-ignore
      this.socket.use((packet, next) => {
        const event = packet[0];
        if (!Object.keys(this.handleEventsConfig).includes(event)) {
          // TODO: Turn this into an error after all events have been converted to the new API.
          // throw new Error(`Received unknown event "${event}".`);
          console.warn(`Received unknown event "${event}".`);
        }
        next();
      });
    }
  }

  on = <Event extends EventNames<HandleEvents>>(
    event: Event,
    handler: EventHandlerFn<HandleEvents[Event]>
  ) => {
    if (!Object.keys(this.handleEventsConfig).includes(event)) {
      throw new Error(`Cannot register handler for unknown event "${event}".`);
    }
    const eventConfig = this.handleEventsConfig[event];
    this.socket.on(event, (...args) => {
      const expectedNumArgs = Object.keys(eventConfig.args).length + 1;
      if (args.length !== expectedNumArgs) {
        throw new Error(
          `Received event "${event}" with unexpected number of arguments: Expected ${expectedNumArgs}, received ${args.length}.`
        );
      }

      const ackCallback = args[args.length - 1];
      if (typeof ackCallback !== "function") {
        throw new Error(
          `Received event "${event}" without ack callback. Was the event emitted manually through Socket.IO?`
        );
      }

      type ArgsType = EventArgs<HandleEvents[Event]>;
      const validatedArgs: Partial<ArgsType> = {};
      Object.entries(eventConfig.args).forEach(([arg, validator], i) => {
        if (!validator.validate(args[i])) {
          throw new Error(
            `Received invalid argument "${arg}" for event "${event}".`
          );
        }
        validatedArgs[arg as keyof ArgsType] = args[i];
      });

      const response: Response<EventReturnType<HandleEvents[Event]>> = {};
      try {
        const result = handler(validatedArgs as ArgsType);
        if (eventConfig.returnType !== undefined) {
          if (!eventConfig.returnType.validate(result)) {
            throw new Error(
              `Handler for event "${event}" returned invalid return type.`
            );
          }
        }

        // NOTE: The result might be undefined here
        response.result = result;
      } catch (e) {
        response.error = e.message;
      }
      ackCallback(response);
    });
  };

  emit = async <Event extends EventNames<EmitEvents>>(
    event: Event,
    args: EventArgs<EmitEvents[Event]>
  ): Promise<EventReturnType<EmitEvents[Event]>> => {
    const eventConfig = this.emitEventsConfig[event];
    const argValues = Object.values(args);
    const validatedArgs: any[] = [];
    Object.entries(eventConfig.args).forEach(([arg, validator], i) => {
      if (!validator.validate(argValues[i])) {
        throw new Error(
          `Trying to emit an invalid argument "${arg}" for event "${event}".`
        );
      }
      validatedArgs.push(argValues[i]);
    });

    const response: Response<
      EventReturnType<EmitEvents[Event]>
    > = await new Promise((resolve, reject) => {
      const timeoutHandle = setTimeout(() => {
        reject(
          new Error(
            `No response received for event "${event}" within timeout (${this.timeout}ms).`
          )
        );
      }, this.timeout);

      this.socket.emit(
        event,
        ...validatedArgs,
        (response: Response<EventReturnType<EmitEvents[Event]>>) => {
          clearTimeout(timeoutHandle);
          resolve(response);
        }
      );
    });

    if (response.error !== undefined) {
      throw new Error(response.error);
    }

    return response.result as EventReturnType<EmitEvents[Event]>;
  };
}

export function makeAPIServer<C extends APIConfig>(
  apiConfig: C,
  socket: io.Socket
) {
  return new WebSocketAPI(
    apiConfig.clientEvents,
    apiConfig.serverEvents,
    socket,
    { timeout: config.API_REQUEST_TIMEOUT }
  );
}

export function makeAPIClient<C extends APIConfig>(
  apiConfig: C,
  socket: typeof ClientToServerSocket
) {
  return new WebSocketAPI(
    apiConfig.serverEvents,
    apiConfig.clientEvents,
    socket,
    { timeout: config.API_REQUEST_TIMEOUT }
  );
}
