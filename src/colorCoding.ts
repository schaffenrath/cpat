import { Vector3 } from "three";

import { DependencyKind, JobType, KernelId } from "./shared/types";
import { SelectionType } from "./components/TimelineMouseHandler";

/**
 * Converts RGB vector to hex value
 * @param rgbVec Three dimensional vector containing RGB values
 */
export function convertRGBtoHex(rgbVec: Vector3) {
  function componentToHex(channel: number) {
    let hex = channel.toString(16);
    return hex.length === 1 ? "0" + hex : hex;
  }
  return (
    "#" +
    componentToHex(rgbVec.x) +
    componentToHex(rgbVec.y) +
    componentToHex(rgbVec.z)
  );
}

/**
 * Get RGB color value of job type
 * @param jobType
 */
export function getRGBColorOfType(jobType: JobType) {
  switch (jobType) {
    case "deviceTask":
      return new Vector3(102, 126, 234);
    case "hostTask":
      return new Vector3(103, 216, 104);
    case "push":
      return new Vector3(247, 61, 40);
    case "awaitPush":
      return new Vector3(250, 196, 114);
    case "horizon":
      return new Vector3(143, 100, 234);
  }
}

/**
 * Get HEX color value of job type
 */
export function getHEXColorOfType(jobType: JobType) {
  const rgbColor = getRGBColorOfType(jobType);
  return (
    "#" +
    ((1 << 24) + (rgbColor.x << 16) + (rgbColor.y << 8) + rgbColor.z)
      .toString(16)
      .slice(1)
  );
}

const COLOR_STEP = 20;

export function getKernelColor(
  targetKernel: KernelId,
  kernels: Array<KernelId>
) {
  let kernelIdx = kernels.findIndex((kernel) => kernel === targetKernel);
  if (kernelIdx === -1) return new Vector3(0);
  if (kernelIdx > COLOR_STEP ** 2) kernelIdx = kernelIdx % COLOR_STEP ** 2;
  const greenOffset = kernelIdx % 2 === 0 ? 0 : 100;
  const green = 200 - greenOffset - (kernelIdx % 200) * 10;
  const blue = 255 - Math.ceil(kernelIdx / COLOR_STEP) * COLOR_STEP;
  return new Vector3(0, green, blue);
}

export const HIGHLIGHT_HEX_COLOR = "#ff00c8";
export const HIGHLIGHT_RGB_COLOR = new Vector3(255, 0, 200);
export const HIGHLIGHT_PATTERN_COUNT = 100;

export function getSelectionColor(selectionType: SelectionType) {
  switch (selectionType) {
    case "Zoom":
      return "#0096ff";
    case "Statistic":
      return "#ff0004";
  }
}

export function getDependencyColor(dependencyKind: DependencyKind) {
  switch (dependencyKind) {
    case DependencyKind.ANTI_DEP:
      return "#000000";
    case DependencyKind.ORDER_DEP:
      return "#beaa00";
    case DependencyKind.TRUE_DEP:
      return "#00b400";
    case DependencyKind.UNRECOGNIZED:
      return "#bf0000";
  }
}
