import { v4 as uuidv4 } from "uuid";

import {
  PollStatistic,
  CommandType,
  NodeId,
  JobId,
  TimeSpan,
  Command,
  CommandId,
} from "../shared/types";

import { CelerityRun, Job } from "../server/types";

export function createZeroPollStatistics(): PollStatistic {
  return { minDuration: 0, maxDuration: 0, avgDuration: 0, sampleCount: 0 };
}

export function createEmptyJob(): Job {
  return {
    commandId: 0,
    finishTime: 0,
    startTime: 0,
    lineInVis: 0,
    pollStatistic: null,
    submitTime: 0,
    submittedTime: 0,
  };
}

/**
 * Export for sampler.test
 */
function createEmptyCommand(): Command {
  return {
    type: CommandType.AWAIT_PUSH,
    bufferAccesses: [],
    bufferId: 0,
    otherNode: 1,
    dependees: [],
    dependers: [],
    executionRange: { min: [0], max: [0] },
    kernelId: null,
    nodeId: 0,
    taskId: null,
  };
}

export function createRunFromJobs(
  testJobs: Map<NodeId, Map<JobId, Job>>
): CelerityRun {
  let runTimeSpan: TimeSpan = { start: Number.MAX_VALUE, end: 0 };
  let shortestJobExecTime = Number.MAX_VALUE;
  let jobVisLines: Map<NodeId, number> = new Map();
  let commandMap: Map<CommandId, Command> = new Map();
  testJobs.forEach((jobsOfNode, nodeId) => {
    let maxVisLine = 0;
    jobsOfNode.forEach((job, jobId) => {
      if (job.startTime < runTimeSpan.start) runTimeSpan.start = job.startTime;
      if (job.finishTime > runTimeSpan.end) runTimeSpan.end = job.finishTime;
      if (job.finishTime - job.startTime < shortestJobExecTime)
        shortestJobExecTime = job.finishTime - job.startTime;
      if (job.lineInVis > maxVisLine) maxVisLine = job.lineInVis;
      commandMap.set(jobId, createEmptyCommand());
    });
    jobVisLines.set(nodeId, maxVisLine + 1);
  });

  return {
    id: uuidv4(),
    executableName: "TestRun",
    startTime: runTimeSpan.start,
    endTime: runTimeSpan.end,
    shortestExecTime: shortestJobExecTime,
    jobs: testJobs,
    commands: commandMap,
    tasks: new Map(),
    kernels: new Set(),
    jobVisLines: jobVisLines,
  };
}
