import { useRef } from "react";

/**
 * Returns a throttler function that can be used to limit subsequent calls
 * to a provided callback function to a given maximum rate.
 *
 * @param delay Time between subsequent value updates, in ms.
 */
export function useThrottler<T>(delay: number) {
  const state = useRef<{ isThrottling: boolean; value: T | null }>({
    isThrottling: false,
    value: null,
  });

  return (nextValue: T, callback: (value: T) => void) => {
    state.current.value = nextValue;
    if (!state.current.isThrottling) {
      callback(nextValue);
      state.current.isThrottling = true;
      setTimeout(() => {
        if (state.current.value !== nextValue) {
          // @ts-ignore Value can't be null at this point
          callback(state.current.value);
        }
        state.current.isThrottling = false;
      }, delay);
    }
  };
}

/**
 * Returns a debouncer function that can be used to delay a callback
 * invocation until no update has been made for a given time.
 *
 * @param waitTime The time required since the last update, in ms.
 */
export function useDebouncer(waitTime: number) {
  const state = useRef<{ timeoutHandle: number | null }>({
    timeoutHandle: null,
  });

  return (callback: () => void) => {
    if (state.current.timeoutHandle !== null) {
      clearTimeout(state.current.timeoutHandle);
    }
    state.current.timeoutHandle = setTimeout(() => {
      callback();
    }, waitTime);
  };
}
