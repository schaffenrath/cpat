import socketIOClient from "socket.io-client";
import events from "events";

import {
  addJobFrames,
  addRunInfo,
  addSampledFrames,
  setAvailableRuns,
  setStatistics,
} from "./redux/reducers/celerityDataSlice";
import config from "./shared/config";
import { makeCelerityAPIClient } from "./shared/celerityWebSocketAPI";
import { BackendStatus, TimeSpan, RunId, DependencyInfo } from "./shared/types";

const MaxFileChunkSize = 1e6;

export type EventTypes = "BackendStatusChanged" | "BackendError";

/**
 * The WebSocketInstance holds the SocketIO socket and handles the entire communication with the backend.
 * Only a single instance of this class should exist and lives should live throughout the entire session.
 */
export class WebSocketInstance {
  // TODO: Migrate all raw socket uses to apiClient
  private socket: SocketIOClient.Socket;
  private apiClient: ReturnType<typeof makeCelerityAPIClient>;
  private eventEmitter: events.EventEmitter;
  private dispatch: (reducer: any) => void;

  constructor(dispatch: (reducer: any) => void) {
    this.socket = socketIOClient(`http://localhost:${config.WEBSERVER_PORT}`);
    this.apiClient = makeCelerityAPIClient(this.socket);
    this.eventEmitter = new events.EventEmitter();
    this.dispatch = dispatch;
    if (this.socket === undefined) return;
    this.addListeners();
    this.requestListOfAvailableRuns();
  }

  private addListeners() {
    this.socket.on("connect", () => {
      console.log("connected");
    });

    this.socket.on("disconnect", (reason: string) => {
      console.error("disconnected due to: ", reason);
    });

    this.apiClient.on("CelerityRunInfo", (args) => {
      this.dispatch(addRunInfo(args.celerityRun));
    });

    this.apiClient.on("JobFrames", (args) => {
      this.dispatch(addJobFrames(args.jobFrames));
    });

    this.apiClient.on("SampledFrames", (args) => {
      this.dispatch(addSampledFrames(args.sampledFrames));
    });

    this.apiClient.on("StatisticData", (args) => {
      this.dispatch(setStatistics(args.statistics));
    });

    this.apiClient.on("AvailableRuns", (args) => {
      if (args.availableRuns.length !== 0) {
        this.dispatch(setAvailableRuns(args.availableRuns));
      } else {
        console.info("Backend request status with 0 available runs!");
      }
    });

    this.apiClient.on("BackendStatus", (args) => {
      this.eventEmitter.emit(
        "BackendStatusChanged",
        args.status as BackendStatus
      );
    });

    this.apiClient.on("BackendError", (args) => {
      this.eventEmitter.emit("BackendError", args.errorMessage);
    });
  }

  requestListOfAvailableRuns = () => {
    this.apiClient.emit("RequestListOfAvailableRuns", {});
  };

  requestRun = (runId: string) => {
    this.apiClient.emit("RequestRunInfo", {
      runId,
      windowWidth: window.innerWidth,
    });
    this.requestTimeFrames(runId, {
      start: 0,
      end: 0,
    });
  };

  requestTimeFrames = (
    runId: RunId,
    viewSpan: TimeSpan,
    sampleLevel?: number
  ) => {
    if (sampleLevel !== undefined) {
      this.apiClient.emit("RequestSingleTimeFrame", {
        runId,
        viewSpan,
        sampleLevel,
      });
    } else {
      this.apiClient.emit("RequestTimeFrames", {
        runId,
        viewSpan,
      });
    }
  };

  requestSpecificFrames = (requestInfo: DependencyInfo) => {
    this.apiClient.emit("RequestSpecificTimeFrames", {
      requestInfo,
    });
  };

  requestStatistics = (runId: RunId, viewSpan: TimeSpan) => {
    this.apiClient.emit("RequestStatistics", {
      runId,
      viewSpan,
    });
  };

  requestRunDeletion = (runId: string) => {
    this.apiClient.emit("RequestRunDeletion", { runId: runId });
  };

  resetData = () => {
    this.socket.emit("ResetData");
  };

  private sendFileChunks = (fileBuffer: ArrayBuffer) => {
    const fileSize = fileBuffer.byteLength;
    let startOffset = 0;
    while (startOffset + MaxFileChunkSize < fileSize) {
      this.apiClient.emit("DroppedData", {
        data: fileBuffer.slice(startOffset, startOffset + MaxFileChunkSize),
        totalDataSize: fileSize,
      });
      startOffset += MaxFileChunkSize;
    }
    this.apiClient.emit("DroppedData", {
      data: fileBuffer.slice(startOffset, fileSize),
      totalDataSize: fileSize,
    });
  };

  private sendCompleteFile = (fileBuffer: ArrayBuffer) => {
    this.apiClient.emit("DroppedData", {
      data: fileBuffer,
      totalDataSize: fileBuffer.byteLength,
    });
  };

  sendDroppedTraceFile = (fileBuffer: ArrayBuffer) => {
    if (fileBuffer.byteLength > MaxFileChunkSize) {
      this.sendFileChunks(fileBuffer);
    } else {
      this.sendCompleteFile(fileBuffer);
    }
  };

  // EventListener is currently used for 'BackendStatus' and 'BackendError'
  addEventListener<T>(event: EventTypes, handler: (message: T) => void) {
    this.eventEmitter.addListener(event, handler);
  }
}
