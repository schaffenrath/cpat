import React, { useEffect, useCallback, useRef, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { message } from "antd";

import { RootState } from "../redux/store";
import { WebSocketInstance } from "../WebSocket";
import InitialView from "./InitialView";
import VisualizeView from "./VisualizeView";
import RunSelectionView from "./RunSelectionView";
import { BackendErrorMsg, BackendStatus } from "../shared/types";

export type ViewType = "Initial" | "RunSelection" | "Visualization";

export default function ViewHandler() {
  const webSocketRef = useRef<WebSocketInstance | null>(null);

  /**
   * Backend messages.
   */
  const showBackendStatusMsg = useCallback((backendStatus: BackendStatus) => {
    if (backendStatus !== "Initial" && backendStatus !== "Ready")
      message.info(`Backend ${backendStatus} data`);
  }, []);

  const showBackendError = useCallback((backendError: BackendErrorMsg) => {
    message.error(`Backend error: ${backendError}`);
  }, []);

  const dispatch = useDispatch();

  const addEventListeners = useCallback(
    (webSocket: WebSocketInstance) => {
      webSocket.addEventListener<BackendStatus>(
        "BackendStatusChanged",
        showBackendStatusMsg
      );
      webSocket.addEventListener<BackendErrorMsg>(
        "BackendError",
        showBackendError
      );
    },
    [showBackendError, showBackendStatusMsg]
  );

  /**
   * Init web socket.
   */
  if (webSocketRef.current === null) {
    webSocketRef.current = new WebSocketInstance(dispatch);
    addEventListeners(webSocketRef.current);
  }
  const webSocket = webSocketRef.current;

  const [displayedView, setDisplayedView] = useState<ViewType>("Initial");
  const availableRuns = useSelector(
    (state: RootState) => state.celerityRun.availableRuns
  );

  const viewChangeByUser = useRef<boolean>(false);

  // Handles which view should be displayed depending on the backend state and available runs
  useEffect(() => {
    if (viewChangeByUser.current) {
      viewChangeByUser.current = false;
      return;
    }
    if (displayedView !== "Visualization") {
      if (Object.keys(availableRuns).length === 1) {
        setDisplayedView("Visualization");
      } else if (Object.keys(availableRuns).length > 1) {
        setDisplayedView("RunSelection");
      } else {
        setDisplayedView("Initial");
      }
    }
  }, [displayedView, availableRuns]);

  const changeView = useCallback((newView: ViewType) => {
    viewChangeByUser.current = true;
    setDisplayedView(newView);
  }, []);

  switch (displayedView) {
    case "Initial":
      return (
        <div>
          <InitialView
            droppedTraceFile={webSocket.sendDroppedTraceFile}
            showView={changeView}
          />
        </div>
      );
    case "RunSelection":
      return (
        <RunSelectionView
          showView={changeView}
          requestRun={webSocket.requestRun}
        />
      );
    case "Visualization":
      return (
        <VisualizeView
          droppedTraceFile={webSocket.sendDroppedTraceFile}
          showView={changeView}
          requestRun={webSocket.requestRun}
          requestRunDeletion={webSocket.requestRunDeletion}
          requestTimeFrames={webSocket.requestTimeFrames}
          requestSpecificFrames={webSocket.requestSpecificFrames}
          requestStatistics={webSocket.requestStatistics}
        />
      );
  }
}
