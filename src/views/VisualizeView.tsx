import React, { useCallback, useMemo } from "react";
import { Button } from "antd";
import { useDropzone } from "react-dropzone";

import TimelineContainer from "../components/TimelineContainer";
import { ViewType } from "./ViewHandler";
import { RunId, DependencyInfo, TimeSpan } from "../shared/types";

interface Props {
  droppedTraceFile: (fileBuffer: ArrayBuffer) => void;
  showView: (newView: ViewType) => void;
  requestRun: (runId: RunId) => void;
  requestRunDeletion: (runId: RunId) => void;
  requestTimeFrames: (
    runId: RunId,
    viewSpan: TimeSpan,
    sampleLevel?: number
  ) => void;
  requestSpecificFrames: (requestInfo: DependencyInfo) => void;
  requestStatistics: (runId: RunId, timeSpan: TimeSpan) => void;
}

export default function VisualizeView(props: Props) {
  // Callback on dropped trace file in browser, trigger transfer to backend.
  const onDrop = useCallback(
    (files: any) => {
      files.forEach((file: File) => {
        // @ts-ignore
        file.arrayBuffer().then((buffer: ArrayBuffer) => {
          props.droppedTraceFile(buffer);
        });
      });
    },
    [props]
  );

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
  });

  /**
   * Area to select and add run or drop trace files.
   */
  const buttons = useMemo(
    () => (
      <div style={{ display: "flex", marginTop: 50 }}>
        <Button
          onClick={() => props.showView("RunSelection")}
          style={{ position: "relative" }}
        >
          Select Run
        </Button>
        <Button
          onClick={() => props.showView("Initial")}
          style={{ position: "relative" }}
        >
          Add Run
        </Button>
        <div style={{ border: "1px solid black", width: 600, height: 32 }}>
          <div
            {...getRootProps()}
            style={{ textAlign: "center", marginTop: 4 }}
          >
            <input {...getInputProps()} />
            {isDragActive ? (
              <p>Drop the file here ...</p>
            ) : (
              <p>
                Drag 'n' drop the profiling file here, or click to select the
                file
              </p>
            )}
          </div>
        </div>
      </div>
    ),
    [props, getRootProps, getInputProps, isDragActive]
  );

  return (
    <div>
      <TimelineContainer
        showView={props.showView}
        requestRun={props.requestRun}
        requestRunDeletion={props.requestRunDeletion}
        requestTimeFrames={props.requestTimeFrames}
        requestSpecificFrames={props.requestSpecificFrames}
        requestStatistics={props.requestStatistics}
      />
      {buttons}
    </div>
  );
}
