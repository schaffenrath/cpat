import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Select } from "antd";

import { RootState } from "../redux/store";
import { ViewType } from "./ViewHandler";
import { RunId } from "../shared/types";
import { setMainRun } from "../redux/reducers/celerityDataSlice";

const { Option } = Select;

interface Props {
  showView: (newView: ViewType) => void;
  requestRun: (runId: RunId) => void;
}

export default function RunSelectionView(props: Props) {
  const dispatch = useDispatch();

  // On selecting a run from the select menu, set the related
  // run as main run in redux.
  const handleChange = useCallback(
    (selectedRun: string) => {
      dispatch(setMainRun(selectedRun));
      props.showView("Visualization");
    },
    [props, dispatch]
  );

  const availableRuns = useSelector(
    (state: RootState) => state.celerityRun.availableRuns
  );
  // Convert available runs to select options.
  const options: Array<JSX.Element> = [];
  Object.keys(availableRuns).forEach((runId) => {
    options.push(
      <Option key={runId} value={runId}>
        {availableRuns[runId]}
      </Option>
    );
  });

  return (
    <div>
      <h2>Select one of the available runs to display: </h2>
      <Select
        onChange={handleChange}
        style={{ width: 300, marginTop: 20 }}
        placeholder={"Select an available run"}
      >
        {options}
      </Select>

      <Button type={"primary"} onClick={() => props.showView("Initial")}>
        Back
      </Button>
    </div>
  );
}
