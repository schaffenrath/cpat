import React, { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import { Button } from "antd";
import { ViewType } from "./ViewHandler";

interface Props {
  droppedTraceFile: (fileBuffer: ArrayBuffer) => void;
  showView: (newView: ViewType) => void;
}

export default function InitialView(props: Props) {
  // Callback on dropped trace file in browser, trigger transfer to backend.
  const onDrop = useCallback(
    (files: any) => {
      files.forEach((file: File) => {
        // @ts-ignore
        file.arrayBuffer().then((buffer: ArrayBuffer) => {
          props.droppedTraceFile(buffer);
        });
      });
    },
    [props]
  );

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
  });

  return (
    <div>
      <h1>Waiting on connection from Celerity-Runtime... </h1>
      <h3>
        Celerity allows two methods to transfer a trace to the tooling
        application. Set the environment variable either to:{" "}
        <mark>tcp://&lt;ip-address&gt;</mark> or{" "}
        <mark>file://&lt;filename&gt;</mark>. <br />
        If you choose the socket method and use tunneling, make sure to forward
        to the port: <mark>4503</mark>!
      </h3>
      <div style={{ border: "1px solid black", width: 600 }}>
        <div {...getRootProps()}>
          <input {...getInputProps()} />
          {isDragActive ? (
            <p>Drop the file here ...</p>
          ) : (
            <p>
              Drag 'n' drop the profiling file here, or click to select the file
            </p>
          )}
        </div>
      </div>

      <Button
        onClick={() => props.showView("RunSelection")}
        style={{ position: "relative", marginTop: "50px" }}
      >
        Select Run (if available)
      </Button>
    </div>
  );
}
