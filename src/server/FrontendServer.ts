import express from "express";
import http from "http";
import io from "socket.io";

import { makeCelerityAPIServer } from "../shared/celerityWebSocketAPI";
import TelemetryServer from "./TelemetryServer";
import DataStorage from "./DataStorage";
import DataReceiver from "./DataReceiver";
import { getStatisticsInRange } from "./statisticsCalculation";
import { CelerityRun } from "./types";
import {
  createJobFramesPerNode,
  getJobFramesContainingJob,
  getJobFramesInTimeSpan,
  getJobFramesOfCommandDependencies,
  getJobFramesOfTaskDependencies,
} from "./timeFrameProcessing";
import {
  getSampledFramesInTime,
  getSampledFrame,
  shouldSample,
} from "./sampler";
import config from "../shared/config";

/**
 * The server component responsible for handling all interactions with the frontend.
 */
export default class FrontendServer {
  private ioSocket: io.Server | undefined;
  private dataStorage: DataStorage;
  private httpServer: http.Server;
  private apiServer: any | null = null;

  constructor(dataStorage: DataStorage, telemetryServer: TelemetryServer) {
    this.init = this.init.bind(this);
    this.start = this.start.bind(this);
    this.changeStatusProcessing = this.changeStatusProcessing.bind(this);
    this.changeStatusReceiving = this.changeStatusReceiving.bind(this);
    this.changeStatusTransferring = this.changeStatusTransferring.bind(this);
    this.notify = this.notify.bind(this);
    this.notifyError = this.notifyError.bind(this);

    this.dataStorage = dataStorage;
    const app = express();
    this.httpServer = http.createServer(app);

    telemetryServer.addListener("DataReceived", this.notify);
    telemetryServer.addListener("Receiving", this.changeStatusReceiving);
    telemetryServer.addListener("ConnectionError", this.notifyError);

    this.init();
  }

  init = () => {
    this.ioSocket = io(this.httpServer);
    this.ioSocket.on("connection", (socket: io.Socket) => {
      const apiServer = makeCelerityAPIServer(socket);
      this.apiServer = apiServer;
      const dataReceiver = new DataReceiver();
      let stream: ReturnType<DataReceiver["createStream"]> | null = null;
      if (this.apiServer !== null)
        this.apiServer.emit("BackendStatus", { status: "Initial" });

      /**
       * Notify frontend if a new run became available
       */
      dataReceiver.on("RunReceived", (run) => {
        stream = null;
        this.dataStorage.addRun(run);
        this.dataStorage.addJobFrames(run.id, createJobFramesPerNode(run));
        apiServer.emit("AvailableRuns", {
          availableRuns: this.dataStorage.getAvailableRunIdsWithExecName(),
        });
      });

      if (this.dataStorage.areRunsAvailable()) {
        this.notify();
      }

      /**
       * Transfer tracing files from frontend and handover to stream
       */
      // TODO: Need a way of differentiating multiple files
      apiServer.on("DroppedData", (args) => {
        if (stream === null) {
          stream = dataReceiver.createStream();
        }
        stream.append(Buffer.from(args.data));
      });

      /**
       * Send general information about a specific run.
       */
      apiServer.on("RequestRunInfo", (args) => {
        const runData = this.dataStorage.getCelerityRunInfo(args.runId);
        if (runData !== null)
          apiServer.emit("CelerityRunInfo", { celerityRun: runData });
      });

      /**
       * Send a single timeframe or sampledframe for a specific run and a given view span.
       */
      apiServer.on("RequestSingleTimeFrame", (args) => {
        const run = this.dataStorage.getRun(args.runId);
        const framesOfRun = this.dataStorage.getJobFrames(args.runId);
        if (framesOfRun === null || run === null)
          throw new Error(`Job frames of run ${args.runId} are not available!`);
        if (
          config.DISABLE_SAMPLING ||
          args.sampleLevel === framesOfRun.baseFrameLength
        ) {
          apiServer.emit("JobFrames", {
            jobFrames: getJobFramesInTimeSpan(
              args.runId,
              framesOfRun,
              args.viewSpan
            ),
          });
        } else {
          apiServer.emit("SampledFrames", {
            sampledFrames: getSampledFrame(
              framesOfRun,
              run,
              args.viewSpan,
              args.sampleLevel
            ),
          });
        }
      });

      /**
       * Send timeFrames or sampledFrames of a specific run and view span.
       * If the view span is not set (indicated by end === 0), the entire length
       * of the run is used as the view span.
       */
      apiServer.on("RequestTimeFrames", (args) => {
        const run = this.dataStorage.getRun(args.runId);
        const framesOfRun = this.dataStorage.getJobFrames(args.runId);
        if (framesOfRun === null || run === null)
          throw new Error(`Job frames of run ${args.runId} are not available!`);
        let viewSpan = args.viewSpan;
        if (viewSpan.end === 0)
          viewSpan = {
            start: args.viewSpan.start,
            end: (run.endTime - run.startTime) * 1.05,
          };
        if (shouldSample(framesOfRun, viewSpan)) {
          const associatedRun = this.dataStorage.getRun(
            args.runId
          ) as CelerityRun;
          const sampledFrames = getSampledFramesInTime(
            framesOfRun,
            associatedRun,
            viewSpan
          );
          apiServer.emit("SampledFrames", { sampledFrames: sampledFrames });
        } else {
          const jobFrames = getJobFramesInTimeSpan(
            args.runId,
            framesOfRun,
            viewSpan
          );
          apiServer.emit("JobFrames", { jobFrames: jobFrames });
        }
      });

      /**
       * Sending a single timeFrame containing either a specific job, command or task.
       * Mainly required for dependency graphs and showing details information about a job in
       * the timeline.
       */
      apiServer.on("RequestSpecificTimeFrames", (args) => {
        const run = this.dataStorage.getRun(args.requestInfo.runId);
        const framesOfRun = this.dataStorage.getJobFrames(
          args.requestInfo.runId
        );
        if (framesOfRun === null || run === null)
          throw new Error(
            `Job frames of run ${args.requestInfo.runId} are not available!`
          );

        if (args.requestInfo.requestType === "Job") {
          apiServer.emit("JobFrames", {
            jobFrames: getJobFramesContainingJob(framesOfRun, args.requestInfo),
          });
        } else if (args.requestInfo.requestType === "Command") {
          apiServer.emit("JobFrames", {
            jobFrames: getJobFramesOfCommandDependencies(
              framesOfRun,
              args.requestInfo,
              run
            ),
          });
        } else {
          apiServer.emit("JobFrames", {
            jobFrames: getJobFramesOfTaskDependencies(
              framesOfRun,
              args.requestInfo,
              run
            ),
          });
        }
      });

      apiServer.on("RequestStatistics", (args) => {
        const statistics = getStatisticsInRange(
          this.dataStorage,
          args.runId,
          args.viewSpan
        );
        apiServer.emit("StatisticData", { statistics });
      });

      apiServer.on("RequestListOfAvailableRuns", () => {
        if (this.ioSocket === undefined) return;
        apiServer.emit("AvailableRuns", {
          availableRuns: this.dataStorage.getAvailableRunIdsWithExecName(),
        });
      });

      apiServer.on("RequestRunDeletion", (args) => {
        this.dataStorage.deleteRun(args.runId);
        if (this.ioSocket === undefined) return;
        apiServer.emit("AvailableRuns", {
          availableRuns: this.dataStorage.getAvailableRunIdsWithExecName(),
        });
      });
    });
    this.ioSocket.on("disconnect", () => {
      console.info("Client disconnected");
    });
  };

  start(port: number) {
    this.httpServer.listen(port);
  }

  stop() {
    this.httpServer.close();
  }

  changeStatusReceiving() {
    if (this.apiServer !== null)
      this.apiServer.emit("BackendStatus", { status: "Receiving" });
  }

  changeStatusProcessing() {
    if (this.apiServer !== null)
      this.apiServer.emit("BackendStatus", { status: "Processing" });
  }

  changeStatusTransferring() {
    if (this.apiServer !== null)
      this.apiServer.emit("BackendStatus", { status: "Transferring" });
  }

  // Notifies the frontend that new data is available and sends it
  notify() {
    try {
      if (this.apiServer !== null) {
        if (this.dataStorage.areRunsAvailable()) {
          this.apiServer.emit("AvailableRuns", {
            availableRuns: this.dataStorage.getAvailableRunIdsWithExecName(),
          });
          this.apiServer.emit("BackendStatus", { status: "Ready" });
        } else {
          this.apiServer.emit("BackendStatus", { status: "Receiving" });
          return;
        }
      } else {
        console.error("Tried to notify clients, but socket not yet set!");
      }
    } catch (e) {
      console.error("Failed to notify frontend: " + e);
    }
  }

  notifyError() {
    try {
      if (this.ioSocket !== undefined) {
        // TODO: Implement more accurate error messages.
        this.apiServer.emit("BackendError", {
          errorMessage: "ConnectionInterrupted",
        });
      }
    } catch (e) {
      console.error("Failed to notify frontend in notifyError: " + e);
    }
  }
}
