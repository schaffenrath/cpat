import config from "../shared/config";
import {
  JobId,
  CmdValuePerType,
  TimeSpan,
  CommandType,
  ExecTimeOfKernel,
  SampledDict,
  SampledFramesOfNode,
  SampledFrameDict,
  JobFramePayload,
  SampledFramePayload,
  SampledJob,
} from "../shared/types";
import {
  getJobsInTimeSpan,
  getJobFramesInTimeSpan,
  JobInSpan,
} from "./timeFrameProcessing";
import { CelerityRun, Job } from "./types";
import { ExecutionTarget } from "../shared/__generated__/trace";
import { extractKernelIdFromTaskName } from "./payloadProcessing";

interface JobEntry {
  jobIdx: number;
  jobId: number;
}

interface JobFusion {
  jobIds: Array<JobEntry>;
  timeSpan: TimeSpan;
  execTimeSum: number;
  kernels: ExecTimeOfKernel;
  types: CmdValuePerType;
}

function getInitTypeValues(): CmdValuePerType {
  return { awaitPush: 0, deviceTask: 0, horizon: 0, hostTask: 0, push: 0 };
}

/**
 * Get type of job from corresponding command and kernel if type is task.
 *
 * @param jobId The id of the corresponding job.
 * @param job The job for which the type and kernel is looked for.
 * @param run The run related to the provided job.
 */
function getJobTypeAndKernel(jobId: number, job: Job, run: CelerityRun) {
  let jobType: keyof CmdValuePerType = "hostTask";
  const commandOfJob = run.commands.get(job.commandId);
  if (commandOfJob === undefined)
    throw new Error(`Command ${job.commandId} of job ${jobId} was not found.`);
  switch (commandOfJob.type) {
    case CommandType.HORIZON:
      jobType = "horizon";
      break;
    case CommandType.AWAIT_PUSH:
      jobType = "awaitPush";
      break;
    case CommandType.PUSH:
      jobType = "push";
      break;
    case CommandType.TASK:
      jobType = "hostTask";
      break;
  }
  let kernelId: string = "";
  if (commandOfJob.type === CommandType.TASK) {
    if (commandOfJob.taskId === null)
      throw new Error(
        `Command ${job.commandId} is missing the corresponding task.`
      );
    const taskOfCommand = run.tasks.get(commandOfJob.taskId);
    if (taskOfCommand === undefined)
      throw new Error(
        `Task ${commandOfJob.taskId} of job ${jobId} was not found.`
      );
    kernelId = extractKernelIdFromTaskName(taskOfCommand.name);

    if (taskOfCommand.target === ExecutionTarget.DEVICE) jobType = "deviceTask";
  }
  return [jobType, kernelId];
}

/**
 * Create new fused job from a raw job. A fused job is used to add/remove
 * raw jobs until sampling conditions are met or otherwise the fused job
 * is omitted.
 *
 * @param jobId The jod id from the raw job.
 * @param jobIdx The job index from the raw job.
 * @param job The job used to create a new fused job.
 * @param run The run related to the provided job.
 */
function createJobFusion(
  jobId: number,
  jobIdx: number,
  job: Job,
  run: CelerityRun
): JobFusion | null {
  const jobExecTime = job.finishTime - job.startTime;
  let newVisLineTraceEntry: JobFusion = {
    jobIds: [{ jobId, jobIdx }],
    timeSpan: { start: job.startTime, end: job.finishTime },
    execTimeSum: jobExecTime,
    kernels: {},
    types: getInitTypeValues(),
  };
  if (newVisLineTraceEntry !== null) {
    let [jobType, kernelId] = getJobTypeAndKernel(jobId, job, run);
    if (kernelId !== "") {
      newVisLineTraceEntry.kernels[kernelId] = jobExecTime;
    }
    newVisLineTraceEntry.types[jobType as keyof CmdValuePerType] += jobExecTime;
  }
  return newVisLineTraceEntry;
}

/**
 * Create a sampled job from a single raw job in case the sampling conditions are met.
 * The returned sampled job can be added to a sampledFrame.
 *
 * @param job The job converted to a fused job.
 * @param jobId The id of the job converted to a fused job.
 * @param run The run related to the provided job.
 */
function createSampledJob(
  job: Job,
  jobId: number,
  run: CelerityRun
): SampledJob {
  const [newJobType, newKernelId] = getJobTypeAndKernel(
    Number(jobId),
    job,
    run
  );
  let newTypeRation: CmdValuePerType = getInitTypeValues();
  newTypeRation[newJobType as keyof CmdValuePerType] =
    job.finishTime - job.startTime;
  let newKernel: ExecTimeOfKernel = {};
  if (newKernelId !== "")
    newKernel[newKernelId] = job.finishTime - job.startTime;
  return {
    jobIdList: [Number(jobId)],
    execSpan: { start: job.startTime, end: job.finishTime },
    lineInVis: job.lineInVis,
    kernels: newKernel,
    typeRatio: newTypeRation,
  };
}

/**
 * Compute ratio of the provided command types.
 *
 * @param typeValue The command types for which the ratio is computed.
 */
function getTypeRatios(typeValue: CmdValuePerType): CmdValuePerType {
  const totalType =
    typeValue.awaitPush +
    typeValue.push +
    typeValue.deviceTask +
    typeValue.hostTask +
    typeValue.horizon;
  return {
    awaitPush: typeValue.awaitPush / totalType,
    push: typeValue.push / totalType,
    deviceTask: typeValue.deviceTask / totalType,
    hostTask: typeValue.hostTask / totalType,
    horizon: typeValue.horizon / totalType,
  };
}

/**
 * Converts the temporary job fusion to a sampled job which can be
 * added to a sampledFrame.
 *
 * @param lineInVis The visualization line related to the job fusion.
 * @param jobFusion The job fusion that is converted an
 */
function getSampledJobFromVisLine(
  lineInVis: number,
  jobFusion: JobFusion
): SampledJob {
  return {
    jobIdList: jobFusion.jobIds.map((entry) => entry.jobId),
    execSpan: jobFusion.timeSpan,
    typeRatio: jobFusion.types,
    kernels: jobFusion.kernels,
    lineInVis: lineInVis,
  };
}

/**
 * Add new raw job to a fused job.
 *
 * @param jobId The id of the added job.
 * @param job The added job.
 * @param jobFusion The fused job.
 * @param run The run related to the jobs.
 */
function addJobInfoToFusion(
  jobId: JobId,
  job: Job,
  jobFusion: JobFusion,
  run: CelerityRun
) {
  const jobExecTime = job.finishTime - job.startTime;
  const [jobType, jobKernelId] = getJobTypeAndKernel(Number(jobId), job, run);
  if (jobKernelId !== "") {
    if (jobFusion.kernels[jobKernelId] === undefined)
      jobFusion.kernels[jobKernelId] = jobExecTime;
    else jobFusion.kernels[jobKernelId] += jobExecTime;
  }
  if (jobFusion.types[jobType as keyof CmdValuePerType] === undefined)
    jobFusion.types[jobType as keyof CmdValuePerType] = jobExecTime;
  else jobFusion.types[jobType as keyof CmdValuePerType] += jobExecTime;
}

/**
 * If the sample conditions for the fused job are not met, start to remove
 * jobs from the fused job in FIFO order until condition is satisfied or
 * execution time does not exceed SAMPLE_MIN_EXEC_TIME_RATIO, such that other
 * jobs can be added.
 *
 * This ensures the fused jobs don't become to long, but also leads to loss of
 * information.
 *
 * @param jobs List of jobs in time span.
 * @param jobFusion Fused job, where jobs are potentially removed.
 * @param run Run that holds jobs.
 * @param minTimeUnit Minimal time a fused job must have to be visible on screen.
 */
function removeTooShortJobsFromFusion(
  jobs: Array<JobInSpan>,
  jobFusion: JobFusion,
  run: CelerityRun,
  minTimeUnit: number
) {
  const collapsedExecTime = jobFusion.timeSpan.end - jobFusion.timeSpan.start;
  let jobExecLongEnough = collapsedExecTime >= minTimeUnit;
  let prevCombinedJobsLongEnough =
    jobFusion.execTimeSum / collapsedExecTime >
    config.SAMPLE_MIN_EXEC_TIME_RATIO;

  while (jobExecLongEnough && !prevCombinedJobsLongEnough) {
    const toRemoveJobId = jobFusion.jobIds[0].jobId;
    const toRemoveJob = jobs[jobFusion.jobIds[0].jobIdx].job;
    if (toRemoveJob === undefined)
      throw new Error(
        `Job ${toRemoveJobId} is not available, even though it existed previously!`
      );

    jobFusion.jobIds.splice(0, 1);
    if (jobFusion.jobIds.length === 0) break;
    jobFusion.timeSpan.end = jobs[jobFusion.jobIds[0].jobIdx].job.finishTime;
    const toRemoveExecTime = toRemoveJob.finishTime - toRemoveJob.startTime;
    jobFusion.execTimeSum -= toRemoveExecTime;
    const [toRemoveType, toRemoveKernel] = getJobTypeAndKernel(
      toRemoveJobId,
      toRemoveJob,
      run
    );
    if (toRemoveKernel !== "")
      jobFusion.kernels[toRemoveKernel] -= toRemoveExecTime;
    jobFusion.types[toRemoveType as keyof CmdValuePerType] -= toRemoveExecTime;

    jobFusion.timeSpan.start = jobs[jobFusion.jobIds[0].jobIdx].job.startTime;

    prevCombinedJobsLongEnough =
      jobFusion.execTimeSum /
        (jobFusion.timeSpan.end - jobFusion.timeSpan.start) >
      config.SAMPLE_MIN_EXEC_TIME_RATIO;
    jobExecLongEnough =
      jobFusion.timeSpan.end - jobFusion.timeSpan.start > minTimeUnit;
  }
}

/**
 * Creating set of sampledFrames for the provided jobs.
 *
 * @param jobs List of jobs in a specified time span.
 * @param run The corresponding run of the jobs.
 * @param sampleLevel The length of a sampledFrame.
 * @param sampledFrameSpan The time span covered by sampledFrames.
 * @param maxVisLines The number of visualization lines without sampling.
 */
function createSampledFrames(
  jobs: Array<JobInSpan>,
  run: CelerityRun,
  sampleLevel: number,
  sampledFrameSpan: TimeSpan,
  maxVisLines: number
): SampledFrameDict {
  const minTimeUnit =
    Math.ceil(sampleLevel / config.SAMPLE_CONSTANT) * config.SAMPLE_FACTOR;

  let frameEnd = sampledFrameSpan.end;
  let sampleFrameList: SampledFrameDict = {};
  let newSampledFrame: SampledDict = {};
  let jobFusionPerVisLine: Array<JobFusion | null> = [];
  for (let i = 0; i < maxVisLines; i++) {
    jobFusionPerVisLine.push(null);
  }
  const jobsLength = jobs.length;
  // Iterate backwards over jobs, since jobs are ordered after their finish time.
  for (let jobIdx = jobsLength - 1; jobIdx >= 0; jobIdx--) {
    const jobId = jobs[jobIdx].jobId;
    const job = jobs[jobIdx].job;

    const jobOutsideCurrentFrame =
      job.finishTime - run.startTime < frameEnd - sampleLevel;

    // If job is outside the current sampledFrame, add current sampledFrame to set.
    if (jobOutsideCurrentFrame) {
      sampleFrameList[frameEnd - sampleLevel] = Object.assign(
        {},
        newSampledFrame
      );
      // If job is outside the time span covered by sampledFrames -> stop.
      if (job.finishTime - run.startTime < sampledFrameSpan.start) {
        break;
      }
      frameEnd -= sampleLevel;
      newSampledFrame = {};
    }

    const jobExecTime = job.finishTime - job.startTime;
    const jobFusion = jobFusionPerVisLine[job.lineInVis];
    // If job is shorter than minimal requirement, try to fuse jobs together.
    if (jobExecTime < minTimeUnit) {
      if (jobFusion === null) {
        jobFusionPerVisLine[job.lineInVis] = createJobFusion(
          Number(jobId),
          jobIdx,
          job,
          run
        );
      } else {
        jobFusion.timeSpan.start = job.startTime;
        jobFusion.execTimeSum += jobExecTime;
        jobFusion.jobIds.push({ jobId: Number(jobId), jobIdx });
        addJobInfoToFusion(Number(jobId), job, jobFusion, run);

        const collapsedExecTime =
          jobFusion.timeSpan.end - jobFusion.timeSpan.start;
        let jobExecLongEnough = collapsedExecTime >= minTimeUnit;
        // If fused job is long enough, check sample condition.
        if (jobExecLongEnough) {
          let jobFusionHasAcceptableIdle =
            jobFusion.execTimeSum / collapsedExecTime >
            config.SAMPLE_MIN_EXEC_TIME_RATIO;
          // If sample condition satisfied, add fused job to sampledFrame.
          if (jobFusionHasAcceptableIdle) {
            jobFusion.types = getTypeRatios(jobFusion.types);
            // Reverse the array, due to backward iteration over jobs.
            jobFusion.jobIds.reverse();
            newSampledFrame[jobFusion.jobIds[0].jobId] =
              getSampledJobFromVisLine(job.lineInVis, jobFusion);
            jobFusionPerVisLine[job.lineInVis] = null;
          } else {
            // Remove jobs until either too short or enough exec time in time span
            removeTooShortJobsFromFusion(jobs, jobFusion, run, minTimeUnit);
          }
        }
      }
    } else {
      jobFusionPerVisLine[job.lineInVis] = null;
      newSampledFrame[Number(jobId)] = createSampledJob(
        job,
        Number(jobId),
        run
      );
    }
  }

  if (Object.keys(newSampledFrame).length !== 0) {
    sampleFrameList[frameEnd - sampleLevel] = Object.assign(
      {},
      newSampledFrame
    );
  }
  frameEnd -= sampleLevel;

  /*
   * If Celerity initialization takes long, possibly no jobs occur in the
   * beginning on some nodes. Create empty sampledframes in that case.
   */
  while (frameEnd > sampledFrameSpan.start) {
    sampleFrameList[frameEnd - sampleLevel] = {};
    frameEnd -= sampleLevel;
  }
  return sampleFrameList;
}

/**
 * Determine the optimal length of sampledFrames based on the length on timeFrames
 * and a given view span.
 *
 * @interal Only exported for testing.
 *
 * @param baseLength The length timeFrames.
 * @param viewSpan The given view span in the frontend.
 */
export function getTargetSampleLevel(baseLength: number, viewSpan: TimeSpan) {
  let targetFrameLength = baseLength;
  while (targetFrameLength * 2 < viewSpan.end - viewSpan.start)
    targetFrameLength *= 2;
  return targetFrameLength;
}

/**
 * Determine if sampling should be applied based on the view span.
 *
 * @param frames Set of timeFrames used to determine if should be sampled.
 * @param viewSpan The view span given by the frontend.
 */
export function shouldSample(
  frames: JobFramePayload,
  viewSpan: TimeSpan
): boolean {
  if (config.DISABLE_SAMPLING) return false;
  if (
    Object.keys(frames.framesPerNode).length === 0 ||
    Object.keys(frames.framesPerNode[0]).length === 0
  )
    throw new Error(
      `The provided "jobFrames" argument does not contain any frames!`
    );
  if (
    viewSpan.start < 0 ||
    viewSpan.end < 0 ||
    viewSpan.end - viewSpan.start <= 0
  )
    throw new Error(
      `The view span [${viewSpan.start} - ${viewSpan.end}] is invalid!`
    );

  let targetFrameLength = getTargetSampleLevel(frames.frameLength, viewSpan);
  if (targetFrameLength === frames.frameLength) return false;
  return true;
}

/**
 * Get time span covered by sampledFrames for the given view span.
 *
 * @param sampleFrameLength Length of a single sampledFrame.
 * @param viewSpan The view span for which the sampledTimeSpan should be returned.
 */
export function getSampledTimeSpan(
  sampleFrameLength: number,
  viewSpan: TimeSpan
): TimeSpan {
  const frameStartTime =
    Math.floor(viewSpan.start / sampleFrameLength) * sampleFrameLength;
  let frameEndTime = frameStartTime;
  while (frameEndTime < viewSpan.end) frameEndTime += sampleFrameLength;
  return { start: frameStartTime, end: frameEndTime };
}

/**
 * Get single sampledFrame for a given time span.
 *
 * @param framesOfRun The set of timeFrames used to create a sampledFrame.
 * @param run The related run of the provided timeFrames.
 * @param viewSpan The view span covered by the generated sampledFrame.
 * @param sampleLevel The target sample level for the generated sampledFrame.
 */
export function getSampledFrame(
  framesOfRun: JobFramePayload,
  run: CelerityRun,
  viewSpan: TimeSpan,
  sampleLevel: number
) {
  let frameList: SampledFramesOfNode = {};
  const frames = getJobFramesInTimeSpan(run.id, framesOfRun, viewSpan);
  for (const nodeId in frames.framesPerNode) {
    const jobsInSpan = getJobsInTimeSpan(
      frames.framesPerNode[Number(nodeId)],
      frames.frameLength,
      viewSpan
    );

    frameList[Number(nodeId)] = createSampledFrames(
      jobsInSpan,
      run,
      sampleLevel,
      viewSpan,
      run.jobVisLines.get(Number(nodeId)) as number
    );
  }
  return {
    runId: run.id,
    baseFrameLength: frames.frameLength,
    frameLength: sampleLevel,
    isSampled: true,
    framesPerNode: frameList,
  };
}

/**
 * Get sampledFrames of a given time span.
 *
 * @param framesOfRun The set of timeFrames used to create sampledFrames
 * @param run The related run of the provided timeFrames.
 * @param viewSpan The view span covered by the generated sampledFrames.
 */
export function getSampledFramesInTime(
  framesOfRun: JobFramePayload,
  run: CelerityRun,
  viewSpan: TimeSpan
): SampledFramePayload {
  let frameList: SampledFramesOfNode = {};
  const sampleLevel = getTargetSampleLevel(framesOfRun.frameLength, viewSpan);
  const sampledFrameSpan = getSampledTimeSpan(sampleLevel, viewSpan);

  const frames = getJobFramesInTimeSpan(run.id, framesOfRun, sampledFrameSpan);

  for (const nodeId in frames.framesPerNode) {
    const jobsInSpan = getJobsInTimeSpan(
      frames.framesPerNode[Number(nodeId)],
      frames.frameLength,
      sampledFrameSpan
    );

    frameList[Number(nodeId)] = createSampledFrames(
      jobsInSpan,
      run,
      sampleLevel,
      sampledFrameSpan,
      run.jobVisLines.get(Number(nodeId)) as number
    );
  }
  return {
    runId: run.id,
    baseFrameLength: frames.frameLength,
    frameLength: sampleLevel,
    isSampled: true,
    framesPerNode: frameList,
  };
}
