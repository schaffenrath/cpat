import TelemetryServer from "./TelemetryServer";
import DataStorage from "./DataStorage";
import FrontendServer from "./FrontendServer";
import config from "../shared/config";

const dataStorage = new DataStorage();
const telemetryServer = new TelemetryServer(dataStorage);
const frontendServer = new FrontendServer(dataStorage, telemetryServer);

telemetryServer.start(config.CELERITY_SERVER_PORT);
frontendServer.start(config.WEBSERVER_PORT);
