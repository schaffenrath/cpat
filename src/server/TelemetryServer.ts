import net from "net";

import { TypedEmitter } from "tiny-typed-emitter";
import makeDebug from "debug";

import DataReceiver from "./DataReceiver";
import DataStorage from "./DataStorage";
import { createJobFramesPerNode } from "./timeFrameProcessing";

const debug = makeDebug("cpat:server:TelemetryServer");

interface TelemetryServerEvents {
  Receiving: () => void;
  DataReceived: () => void;
  ConnectionError: () => void;
}

export default class TelemetryServer extends TypedEmitter<TelemetryServerEvents> {
  private connected: Boolean;
  private dataStorage: DataStorage;
  private server: net.Server;

  constructor(dataStorage: DataStorage) {
    super();
    this.connected = false;
    this.dataStorage = dataStorage;
    this.server = net.createServer();
  }

  isConnected() {
    return this.connected;
  }

  start(port: number) {
    this.server.listen(port, () => {
      // Use single data receiver across multiple connections, so we can gather data from several Celerity workers.
      // Note that we currently don't support multiple Celerity runs transmitting data at the same time.
      // TODO: This needs to be reset once a full run has been received.
      const dataReceiver = new DataReceiver();

      dataReceiver.on("RunReceived", (run) => {
        this.dataStorage.addRun(run);
        this.dataStorage.addJobFrames(run.id, createJobFramesPerNode(run));
        this.emit("DataReceived");
      });

      this.server.on("connection", (socket) => {
        this.emit("Receiving");

        debug(
          `Incoming connection from ${socket.remoteAddress}:${socket.remotePort}`
        );

        const stream = dataReceiver.createStream();

        const timeoutSeconds = 5 * 60;
        socket.setTimeout(timeoutSeconds * 1000);
        socket.on("timeout", () => {
          console.error(
            `Socket for ${socket.remoteAddress}:${socket.remotePort} timed out after ${timeoutSeconds} seconds.`
          );
          this.emit("ConnectionError");
          stream.close();
        });

        socket.on("data", (data) => {
          stream.append(data);
        });

        socket.on("close", () => {
          debug(
            `Connection to ${socket.remoteAddress}:${socket.remotePort} closed`
          );
          stream.close();
        });
      });
    });

    console.info(
      `Telemetry server listening on ${
        (this.server.address() as net.AddressInfo).address
      }:${port}.`
    );
  }

  stop() {
    this.server.close();
  }
}
