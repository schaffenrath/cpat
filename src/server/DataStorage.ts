import {
  JobFramePayload,
  RunId,
  RunIdWithName,
  JobVisLinesOfNode,
  CelerityRun as SharedCelerityRun,
  Dict,
} from "../shared/types";
import { CelerityRun } from "./types";

/**
 * The main data storage component of the backend. Holds each run stored in maps and the related jobFrames.
 */
export default class DataStorage {
  private runsById: Map<RunId, CelerityRun> = new Map();
  private jobFramesById: Map<RunId, JobFramePayload> = new Map();

  addJobFrames = (runId: RunId, jobFrames: JobFramePayload) => {
    this.jobFramesById.set(runId, jobFrames);
  };

  getJobFrames = (runId: RunId) => {
    const jobFrames = this.jobFramesById.get(runId);
    if (jobFrames !== undefined) {
      return jobFrames;
    }
    return null;
  };

  addRun = (run: CelerityRun) => {
    this.runsById.set(run.id, run);
  };

  getRun = (runId: RunId) => {
    const celerityRun = this.runsById.get(runId);
    if (celerityRun !== undefined) {
      return celerityRun;
    }
    return null;
  };

  /**
   * Returns general information about a specific run that is not contained in timeFrames, necessary for the frontend.
   * @param runId The ID of a specific run
   */
  getCelerityRunInfo(runId: string): SharedCelerityRun | null {
    const data = this.runsById.get(runId);

    const jobVisLinesForTransfer: Dict<JobVisLinesOfNode> = {};
    data?.jobVisLines.forEach((visLines, nodeId) => {
      jobVisLinesForTransfer[nodeId] = visLines;
    });

    if (data !== undefined) {
      return {
        id: data.id,
        executableName: data.executableName,
        startTime: data.startTime,
        endTime: data.endTime,
        jobVisLines: jobVisLinesForTransfer,
        kernels: Array.from(data.kernels),
      };
    } else {
      return null;
    }
  }

  /**
   * Returns a list of all stored runs in the backend containing the runId and the executable name.
   */
  getAvailableRunIdsWithExecName = (): Array<RunIdWithName> => {
    let runIdsWithExecNames: Array<RunIdWithName> = [];
    this.runsById.forEach((run, key) => {
      runIdsWithExecNames.push({ id: key, name: run.executableName });
    });
    return runIdsWithExecNames;
  };

  areRunsAvailable = () => {
    return this.runsById.size !== 0;
  };

  deleteRun = (runId: RunId) => {
    this.runsById.delete(runId);
    this.jobFramesById.delete(runId);
  };
}
