import { Box3D } from "../shared/__generated__/trace";
import { v4 as uuidv4 } from "uuid";

import {
  TracePayload,
  Command,
  Task,
  Subrange,
  Job,
  CelerityRun,
} from "./types";
import {
  NodeId,
  KernelId,
  PollStatistic,
  CommandType,
  ExecutionTarget,
  TaskId,
  CommandId,
  JobId,
  BufferId,
} from "../shared/types";

/**
 * Assigns different lines to parallel jobs, so that they don't overlap.
 * This is required for visualization.
 *
 * @internal Only exported for testing.
 *
 * @param jobs The list of jobs to assign, in ascending finish time order.
 * @returns The number of different lines required to fit all jobs.
 */
export function assignJobsToLines(jobs: Array<Job>): number {
  const earliestJobStartPerLine: Array<number> = [];

  // Iterate in reverse, as jobs are ordered by finish time.
  for (let i = jobs.length - 1; i >= 0; --i) {
    const job = jobs[i];

    let foundFreeLine = false;
    for (const [line, startTime] of Array.from(
      earliestJobStartPerLine.entries()
    )) {
      // Find the first free line
      // TODO: Instead try to find the one with the smallest gap
      if (job.finishTime < startTime) {
        earliestJobStartPerLine[line] = job.startTime;
        job.lineInVis = line;
        foundFreeLine = true;
        break;
      }
    }

    // If no free line was found, add a new one.
    if (!foundFreeLine) {
      earliestJobStartPerLine.push(job.startTime);
      job.lineInVis = earliestJobStartPerLine.length - 1;
    }
  }

  return earliestJobStartPerLine.length;
}

/**
 * Computes the time offsets for each node in a run, relative to the earliest reference time point.
 *
 * @internal Only exported for testing.
 *
 * @param payloads The list of payloads to be processed. Exactly one run-info meta package should exist per node.
 */
export function computePerNodeTimeOffsets(payloads: Array<TracePayload>) {
  const timePoints: Map<NodeId, number> = new Map();
  let earliest = Number.MAX_VALUE;

  for (const payload of payloads) {
    if (payload.runInfo == null) continue;
    if (timePoints.has(payload.sourceNodeId)) {
      throw new Error(
        `Received more than one run-info payload for node ${payload.sourceNodeId}.`
      );
    }

    const rtp = payload.runInfo.referenceTimePoint;
    timePoints.set(payload.sourceNodeId, rtp);
    earliest = Math.min(earliest, rtp);
  }

  const offsets = new Map(
    Array.from(timePoints.entries(), ([nid, rtp]) => [nid, rtp - earliest])
  );
  return offsets;
}

/**
 * Processes all jobs contained within a set of payloads, by converting them to the {@link Job | Job type},
 * splitting them according to their associated node, as well as computing the overall start and end times
 * for this run, across all jobs.
 *
 * @internal Only exported for testing.
 *
 * @param payloads The list of payloads to be processed. Payloads that do not contain any jobs are skipped.
 * @param timeOffsets The per-node time offsets which should be subtracted from job time points,
 *                    as calculated by {@link computePerNodeTimeOffsets}.
 */
export function processJobPayloads(
  payloads: Array<TracePayload>,
  timeOffsets: Map<NodeId, number>
) {
  let jobsPerNode: Map<NodeId, Map<JobId, Job>> = new Map();

  let runStartTime: number = Number.MAX_VALUE;
  let runEndTime: number = 0;
  let shortestExecTime: number = Number.MAX_VALUE;

  for (const payload of payloads) {
    if (payload.jobs.length === 0) continue;

    // We assume that each payload only contains jobs for a single node.
    const nodeId = payload.sourceNodeId;
    if (!jobsPerNode.has(nodeId)) {
      jobsPerNode.set(nodeId, new Map());
    }

    if (!timeOffsets.has(nodeId)) {
      throw new Error(`No time offset provided for node ${nodeId}.`);
    }
    const timeOffset = timeOffsets.get(nodeId) as number;
    const nodeJobs = jobsPerNode.get(nodeId) as Map<JobId, Job>;

    for (const job of payload.jobs) {
      const startTime = job.startTime - timeOffset;
      const finishTime = job.finishTime - timeOffset;
      const submitTime = job.submitTime - timeOffset;
      const submittedTime = job.submittedTime - timeOffset;
      let pollStatistic: PollStatistic | null = null;
      if (job.pollStatistic !== undefined) {
        pollStatistic = {
          minDuration: job.pollStatistic.minDuration,
          maxDuration: job.pollStatistic.maxDuration,
          avgDuration: job.pollStatistic.avgDuration,
          sampleCount: job.pollStatistic.sampleCount,
        };
      }
      const createTime =
        job.createTime !== undefined ? job.createTime - timeOffset : undefined;

      // For now we can assume each job's id corresponds to its command's id
      // NOTE: This may not always remain this way
      nodeJobs.set(job.commandId, {
        commandId: job.commandId,
        startTime,
        finishTime,
        submitTime,
        submittedTime,
        pollStatistic,
        lineInVis: 0,
      });

      // Also compute bounds for job run times
      runStartTime = Math.min(runStartTime, startTime);
      runEndTime = Math.max(runEndTime, finishTime);
      if (finishTime - startTime < shortestExecTime)
        shortestExecTime = finishTime - startTime;
    }
  }

  return { jobsPerNode, runStartTime, runEndTime, shortestExecTime };
}

/**
 * Processes all tasks contained within a set of payloads, by converting them to the {@link Task | Task type}.
 * Tasks are expected to be in topological order.
 *
 * @internal Only exported for testing.
 *
 * @param payloads The list of payloads to be processed. Payloads that do not contain any tasks are skipped.
 */
export function processTaskPayloads(payloads: Array<TracePayload>) {
  const tasks: Map<TaskId, Task> = new Map();
  for (const payload of payloads) {
    if (payload.tasks.length === 0) continue;

    for (const t of payload.tasks) {
      const task: Task = {
        name: t.name,
        target: t.target,
        dependees: [],
        dependers: [],
      };
      for (const dep of t.dependencies) {
        task.dependees.push({
          id: dep.id,
          kind: dep.kind,
        });

        // We assume tasks to be sorted topologically, so any dependees listed here should already exist.
        // However, we have to make an exception for task 0, which is the INIT task, that is not included in the trace.
        // TODO: Change that on Celerity side instead?
        if (!tasks.has(dep.id) && dep.id !== 0) {
          throw new Error(
            `Encountered definition for task ${t.id} with dependency on ${dep.id} before encountering definition for task ${dep.id}.`
          );
        }
        tasks.get(dep.id)?.dependers.push({
          id: t.id,
          kind: dep.kind,
        });
      }

      tasks.set(t.id, task);
    }
  }
  return tasks;
}

function makeSubrange(box: Box3D): Subrange {
  return {
    min: [box.min0, box.min1, box.min2],
    max: [box.max0, box.max1, box.max2],
  };
}

export function extractKernelIdFromTaskName(taskName: string): string {
  return taskName.substring(taskName.indexOf("(") + 1, taskName.indexOf(")"));
}

/**
 * Processes all commands contained within a set of payloads, by converting them to the {@link Command | Command type} and
 * extracting all unique kernels and storing it in an array.
 *
 * @internal Only exported for testing.
 *
 * @param payloads The list of payloads to be processed. Payloads that do not contain any commands are skipped.
 * @param tasks The set of tasks referenced by commands in the given payloads.
 */
export function processCommandPayloads(
  payloads: Array<TracePayload>,
  tasks: Map<TaskId, Task>
): Map<number, Command> {
  const commands: Map<CommandId, Command> = new Map();
  for (const payload of payloads) {
    if (payload.commands.length === 0) continue;

    for (const c of payload.commands) {
      const isTransferCommand =
        c.type === CommandType.PUSH || c.type === CommandType.AWAIT_PUSH;
      const isTaskCommand = c.type === CommandType.TASK;

      if (isTaskCommand && c.executionRange == null) {
        throw new Error("Received TASK command without execution range");
      }

      const cmd: Command = {
        nodeId: c.nodeId,
        type: c.type,
        taskId: isTaskCommand ? c.taskId : null,
        kernelId: null, // We'll figure this out later
        otherNode: isTransferCommand ? c.otherNodeId : null,
        // NOTE: This is a bit redundant, we could also determine this from buffer accesses.
        bufferId: isTransferCommand ? c.bufferId : null,
        executionRange: isTaskCommand ? makeSubrange(c.executionRange!) : null,
        bufferAccesses: [],
        dependers: [],
        dependees: [],
      };

      // Figure out kernel id from task name
      // TODO: At some point this should be replaced by more sophisticated kernel information
      if (c.type === CommandType.TASK) {
        const relatedTask = tasks.get(cmd.taskId as TaskId) as Task;
        if (relatedTask.target === ExecutionTarget.DEVICE) {
          cmd.kernelId = extractKernelIdFromTaskName(relatedTask.name);
        }
      }

      if (!c.bufferAccesses.every((ba) => ba.range != null)) {
        throw new Error("Received command buffer access without range");
      }

      cmd.bufferAccesses = c.bufferAccesses.map((ba) => ({
        id: ba.bufferId,
        mode: ba.mode,
        range: makeSubrange(ba.range!),
      }));

      for (const dep of c.dependencies) {
        cmd.dependees.push({
          id: dep.id,
          kind: dep.kind,
        });

        // We assume commands to be sorted topologically, so any dependees listed here should already exist
        if (!commands.has(dep.id)) {
          throw new Error(
            `Encountered definition for command ${c.id} with dependency on ${dep.id} before encountering definition for command ${dep.id}.`
          );
        }
        commands.get(dep.id)?.dependers.push({ id: c.id, kind: dep.kind });
      }
      commands.set(c.id, cmd);
    }
  }

  return commands;
}

/**
 * Kernels need to be store separately form the commands, since the frontend needs to know all kernels before knowing all commands.
 * @param commands The commands processed in {@link processCommandPayloads}.
 */
function getKernelsFromCommands(commands: Map<number, Command>): Set<KernelId> {
  let kernels: Set<KernelId> = new Set();
  commands.forEach((command) => {
    if (command.kernelId !== null && !kernels.has(command.kernelId))
      kernels.add(command.kernelId);
  });
  return kernels;
}

/**
 * Processes a set of payloads belonging to the same Celerity run into the {@link CelerityRun} data structure.
 */
export function processRunPayloads(payloads: Array<TracePayload>): CelerityRun {
  const timeOffsets = computePerNodeTimeOffsets(payloads);
  const {
    jobsPerNode,
    runStartTime,
    runEndTime,
    shortestExecTime,
  } = processJobPayloads(payloads, timeOffsets);

  // Assign parallel jobs to different lines for visualization
  const jobVisLinesPerNode: Map<NodeId, number> = new Map();
  for (const [nodeId, nodeJobs] of Array.from(jobsPerNode.entries())) {
    jobVisLinesPerNode.set(
      nodeId,
      assignJobsToLines(Array.from(nodeJobs.values()))
    );
  }

  const tasks = processTaskPayloads(payloads);
  const commands = processCommandPayloads(payloads, tasks);
  const kernels = getKernelsFromCommands(commands);

  return {
    id: uuidv4(),
    executableName:
      payloads[0].runInfo === undefined
        ? "Exec name not found"
        : payloads[0].runInfo.executableName,
    jobs: jobsPerNode,
    jobVisLines: jobVisLinesPerNode,
    tasks,
    commands: commands,
    kernels: kernels,
    startTime: runStartTime,
    endTime: runEndTime,
    shortestExecTime: shortestExecTime,
  };
}
