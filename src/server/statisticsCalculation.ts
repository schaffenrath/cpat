import { std } from "mathjs";

import DataStorage from "./DataStorage";
import {
  StatisticPackage,
  CommandType,
  CmdValuePerType,
  TimeSpan,
  ExecutionTarget,
  KernelExecTimeOfTarget,
} from "../shared/types";
import { CelerityRun, Command, Job } from "../server/types";

enum UpdateType {
  HOST = "hostTask",
  DEVICE = "deviceTask",
  PUSH = "push",
  AWAIT_PUSH = "awaitPush",
  HORIZON = "horizon",
}

interface ExecTimesPerType {
  hostTask: Array<number>;
  deviceTask: Array<number>;
  push: Array<number>;
  awaitPush: Array<number>;
  horizon: Array<number>;
}

interface CmdCountAndTime {
  cmdTypeCountPerNode: Array<CmdValuePerType>;
  cmdTypeCountTotal: CmdValuePerType;
  cmdTypeExecTimePerNode: Array<CmdValuePerType>;
  cmdTypeExecTimeTotal: CmdValuePerType;
  execTimesPerType: ExecTimesPerType;
  execTimePerNode: Array<number>;
  execTimeTotal: number;
  kernelExecTimePerNode: Array<KernelExecTimeOfTarget>;
  kernelExecTimeTotal: KernelExecTimeOfTarget;
}

function getInitCmdValues(): CmdValuePerType {
  return {
    hostTask: 0,
    deviceTask: 0,
    push: 0,
    awaitPush: 0,
    horizon: 0,
  };
}

function initCmdCountAndTime(runData: CelerityRun) {
  const initCmdValues: CmdValuePerType = getInitCmdValues();
  const initExecTimes: ExecTimesPerType = {
    hostTask: [],
    deviceTask: [],
    push: [],
    awaitPush: [],
    horizon: [],
  };
  let cmdCountAndTime: CmdCountAndTime = {
    cmdTypeCountPerNode: [],
    cmdTypeCountTotal: { ...initCmdValues },
    cmdTypeExecTimePerNode: [],
    cmdTypeExecTimeTotal: { ...initCmdValues },
    execTimesPerType: { ...initExecTimes },
    execTimePerNode: [],
    execTimeTotal: 0,
    kernelExecTimePerNode: [],
    kernelExecTimeTotal: { device: {}, host: {} },
  };

  const nrOfNodes = runData.jobs.size;
  for (let initIdx = 0; initIdx < nrOfNodes; initIdx++) {
    cmdCountAndTime.cmdTypeCountPerNode.push({ ...initCmdValues });
    cmdCountAndTime.cmdTypeExecTimePerNode.push({ ...initCmdValues });
    cmdCountAndTime.execTimePerNode.push(0);
    cmdCountAndTime.kernelExecTimePerNode.push({ device: {}, host: {} });
  }
  return cmdCountAndTime;
}

interface IncCountAndTimeArgs {
  trackedValues: CmdCountAndTime;
  executionTime: number;
  nodeId: number;
}

function incCountAndTimeOfType(typeKey: UpdateType, args: IncCountAndTimeArgs) {
  let updateValues = args.trackedValues;
  updateValues.cmdTypeCountPerNode[args.nodeId][
    typeKey as keyof CmdValuePerType
  ]++;
  updateValues.cmdTypeCountTotal[typeKey as keyof CmdValuePerType]++;
  updateValues.cmdTypeExecTimePerNode[args.nodeId][
    typeKey as keyof CmdValuePerType
  ] += args.executionTime;
  updateValues.cmdTypeExecTimeTotal[typeKey as keyof CmdValuePerType] +=
    args.executionTime;
  updateValues.execTimesPerType[typeKey as keyof ExecTimesPerType].push(
    args.executionTime
  );
  return updateValues;
}

function getExecutionTarget(cmd: Command, runData: CelerityRun) {
  if (cmd !== null && cmd.taskId !== null) {
    const taskOfCmd = runData.tasks.get(cmd.taskId);
    return taskOfCmd?.target;
  }
}

/**
 * Get the execution time of a job within a specific time span.
 *
 * @param job The job for which the exec time is determined.
 * @param timeSpan The time span in which the exec time is determined.
 * @param runData The run related to the provided job.
 * @returns
 */
function getExecTimeInTimeSpan(
  job: Job,
  timeSpan: TimeSpan,
  runData: CelerityRun
) {
  const jobStart = job.startTime - runData.startTime;
  const startInRange = jobStart < timeSpan.start ? timeSpan.start : jobStart;
  const jobEnd = job.finishTime - runData.startTime;
  const endInRange = jobEnd > timeSpan.end ? timeSpan.end : jobEnd;
  return endInRange - startInRange;
}

function getCmdCountAndTimeInRange(
  timeSpan: TimeSpan,
  runData: CelerityRun
): CmdCountAndTime {
  let cmdCountAndTime: CmdCountAndTime = initCmdCountAndTime(runData);
  let incCountAndTimeArgs: IncCountAndTimeArgs = {
    trackedValues: cmdCountAndTime,
    executionTime: 0,
    nodeId: 0,
  };
  runData.jobs.forEach((jobsPerNode, nodeId) => {
    jobsPerNode.forEach((job) => {
      const adaptedStartTime = job.startTime - runData.startTime;
      const adaptedEndTime = job.finishTime - runData.startTime;
      // If cmd starts after view area the loop can stop
      if (adaptedStartTime > timeSpan.end) return;
      if (
        adaptedStartTime > timeSpan.start ||
        adaptedEndTime > timeSpan.start
      ) {
        const executionTime = getExecTimeInTimeSpan(job, timeSpan, runData);

        cmdCountAndTime.execTimePerNode[nodeId] += executionTime;
        cmdCountAndTime.execTimeTotal += executionTime;

        incCountAndTimeArgs.nodeId = nodeId;
        incCountAndTimeArgs.executionTime = executionTime;

        const cmdOfJob = runData.commands.get(job.commandId);
        if (cmdOfJob === undefined)
          throw new Error("Command of job is not available");

        switch (cmdOfJob.type) {
          case CommandType.TASK:
            const execTarget = getExecutionTarget(cmdOfJob, runData);
            let kernelTarget!: keyof KernelExecTimeOfTarget;
            if (execTarget === ExecutionTarget.DEVICE) {
              kernelTarget = "device";
              incCountAndTimeArgs.trackedValues = incCountAndTimeOfType(
                UpdateType.DEVICE,
                incCountAndTimeArgs
              );
            } else if (execTarget === ExecutionTarget.HOST) {
              kernelTarget = "host";
              incCountAndTimeArgs.trackedValues = incCountAndTimeOfType(
                UpdateType.HOST,
                incCountAndTimeArgs
              );
            }
            if (cmdOfJob.kernelId !== null && cmdOfJob.kernelId !== "") {
              if (
                cmdCountAndTime.kernelExecTimeTotal[kernelTarget][
                  cmdOfJob.kernelId
                ] === undefined
              ) {
                cmdCountAndTime.kernelExecTimeTotal[kernelTarget][
                  cmdOfJob.kernelId
                ] = executionTime;
              } else {
                cmdCountAndTime.kernelExecTimeTotal[kernelTarget][
                  cmdOfJob.kernelId
                ] += executionTime;
              }
              if (
                cmdCountAndTime.kernelExecTimePerNode[nodeId][kernelTarget][
                  cmdOfJob.kernelId
                ] === undefined
              ) {
                cmdCountAndTime.kernelExecTimePerNode[nodeId][kernelTarget][
                  cmdOfJob.kernelId
                ] = executionTime;
              } else {
                cmdCountAndTime.kernelExecTimePerNode[nodeId][kernelTarget][
                  cmdOfJob.kernelId
                ] += executionTime;
              }
            }
            break;
          case CommandType.PUSH:
            incCountAndTimeArgs.trackedValues = incCountAndTimeOfType(
              UpdateType.PUSH,
              incCountAndTimeArgs
            );
            break;
          case CommandType.AWAIT_PUSH:
            incCountAndTimeArgs.trackedValues = incCountAndTimeOfType(
              UpdateType.AWAIT_PUSH,
              incCountAndTimeArgs
            );
            break;
          case CommandType.HORIZON:
            incCountAndTimeArgs.trackedValues = incCountAndTimeOfType(
              UpdateType.HORIZON,
              incCountAndTimeArgs
            );
            break;
        }
      }
    });
  });
  return cmdCountAndTime;
}

/**
 * Generic function to perform division on each member of CmdValuePerType either with
 * a scalar or another CmdValuePerType value.
 *
 * @param cmdValues The dividend.
 * @param divisor The divisor.
 */
function dividePerType<T extends CmdValuePerType, K extends CmdValuePerType>(
  cmdValues: T,
  divisor: K | number
): CmdValuePerType {
  let ratio: CmdValuePerType = getInitCmdValues();
  if (typeof divisor === "number") {
    if (divisor === 0) return getInitCmdValues();
    ratio.hostTask =
      cmdValues.hostTask === 0 ? 0 : cmdValues.hostTask / divisor;
    ratio.deviceTask =
      cmdValues.deviceTask === 0 ? 0 : cmdValues.deviceTask / divisor;
    ratio.push = cmdValues.push === 0 ? 0 : cmdValues.push / divisor;
    ratio.awaitPush =
      cmdValues.awaitPush === 0 ? 0 : cmdValues.awaitPush / divisor;
    ratio.horizon = cmdValues.horizon === 0 ? 0 : cmdValues.horizon / divisor;
  } else {
    if (cmdValues.hostTask === 0 || divisor.hostTask === 0) ratio.hostTask = 0;
    else ratio.hostTask = cmdValues.hostTask / divisor.hostTask;
    if (cmdValues.deviceTask === 0 || divisor.deviceTask === 0)
      ratio.deviceTask = 0;
    else ratio.deviceTask = cmdValues.deviceTask / divisor.deviceTask;
    if (cmdValues.push === 0 || divisor.push === 0) ratio.push = 0;
    else ratio.push = cmdValues.push / divisor.push;
    if (cmdValues.awaitPush === 0 || divisor.awaitPush === 0)
      ratio.awaitPush = 0;
    else ratio.awaitPush = cmdValues.awaitPush / divisor.awaitPush;
    if (cmdValues.horizon === 0 || divisor.horizon === 0) ratio.horizon = 0;
    else ratio.horizon = cmdValues.horizon / divisor.horizon;
  }
  return ratio;
}

/**
 * Compute the command type ratio and average execution time per node.
 *
 * @param cmdCountAndTime The number of commands and the corresponding execution times.
 */
function getCmdTypeRatiosAndAvgExecTimes(cmdCountAndTime: CmdCountAndTime) {
  let nodeCmdRatio: Array<CmdValuePerType> = [];
  let nodeAvgExec: Array<CmdValuePerType> = [];
  const initCmdValues = getInitCmdValues();
  const nrOfNodes = cmdCountAndTime.cmdTypeCountPerNode.length;
  for (let nodeId = 0; nodeId < nrOfNodes; nodeId++) {
    nodeCmdRatio.push({ ...initCmdValues });
    nodeAvgExec.push({ ...initCmdValues });

    const cmdTypeExecTimeOfNode =
      cmdCountAndTime.cmdTypeExecTimePerNode[nodeId];
    const execTimeOfNode = cmdCountAndTime.execTimePerNode[nodeId];
    nodeCmdRatio[nodeId] = dividePerType(cmdTypeExecTimeOfNode, execTimeOfNode);

    const cmdTypeCountOfNode = cmdCountAndTime.cmdTypeCountPerNode[nodeId];
    nodeAvgExec[nodeId] = dividePerType(
      cmdTypeExecTimeOfNode,
      cmdTypeCountOfNode
    );
  }
  return { nodeCmdRatio, nodeAvgExec };
}

/**
 * Compute the number of jobs, accumulated job execution time,
 * average job execution time, the execution ratio per command type
 * and the execution time per kernel for each node and all nodes combined.
 *
 * @param dataStorage The data storage object holding all saved runs.
 * @param runId The id of the run for which the statistics should be calculated.
 * @param timeSpan The time span for which the statistics should be calculated.
 */
export function getStatisticsInRange(
  dataStorage: DataStorage,
  runId: string,
  timeSpan: TimeSpan
): StatisticPackage {
  const runData = dataStorage.getRun(runId);
  const emptyCmdValues = {
    hostTask: 0,
    deviceTask: 0,
    push: 0,
    awaitPush: 0,
    horizon: 0,
  };
  const emptyArray: Array<any> = [];
  if (runData === null)
    return {
      runId,
      nodeJobCount: emptyArray,
      totalJobCount: emptyCmdValues,
      nodeExecTime: emptyArray,
      totalExecTime: emptyCmdValues,
      nodeCmdTypeQuota: emptyArray,
      totalCmdTypeQuota: emptyCmdValues,
      nodeAvgExec: emptyArray,
      totalAvgExec: emptyCmdValues,
      cmdDeviation: emptyCmdValues,
      nodeKernelExecTime: emptyArray,
      totalKernelExecTime: { device: {}, host: {} },
    };

  const cmdCountAndTime = getCmdCountAndTimeInRange(timeSpan, runData);
  let { nodeCmdRatio, nodeAvgExec } =
    getCmdTypeRatiosAndAvgExecTimes(cmdCountAndTime);

  let totalCmdRatio = dividePerType(
    cmdCountAndTime.cmdTypeExecTimeTotal,
    cmdCountAndTime.execTimeTotal
  );
  let totalAvgExec = dividePerType(
    cmdCountAndTime.cmdTypeExecTimeTotal,
    cmdCountAndTime.cmdTypeCountTotal
  );

  for (const execTypes in cmdCountAndTime.execTimesPerType) {
    if (
      cmdCountAndTime.execTimesPerType[execTypes as keyof CmdValuePerType]
        .length === 0
    )
      cmdCountAndTime.execTimesPerType[execTypes as keyof CmdValuePerType].push(
        0
      );
  }
  let cmdDeviation = { ...emptyCmdValues };
  cmdDeviation.hostTask = std(cmdCountAndTime.execTimesPerType.hostTask);
  cmdDeviation.deviceTask = std(cmdCountAndTime.execTimesPerType.deviceTask);
  cmdDeviation.push = std(cmdCountAndTime.execTimesPerType.push);
  cmdDeviation.awaitPush = std(cmdCountAndTime.execTimesPerType.awaitPush);

  return {
    runId,
    nodeJobCount: cmdCountAndTime.cmdTypeCountPerNode,
    totalJobCount: cmdCountAndTime.cmdTypeCountTotal,
    nodeKernelExecTime: cmdCountAndTime.kernelExecTimePerNode,
    totalKernelExecTime: cmdCountAndTime.kernelExecTimeTotal,
    nodeExecTime: cmdCountAndTime.cmdTypeExecTimePerNode,
    totalExecTime: cmdCountAndTime.cmdTypeExecTimeTotal,
    nodeCmdTypeQuota: nodeCmdRatio,
    totalCmdTypeQuota: totalCmdRatio,
    nodeAvgExec: nodeAvgExec,
    totalAvgExec: totalAvgExec,
    cmdDeviation: cmdDeviation,
  };
}
