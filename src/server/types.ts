import {
  AccessMode,
  BufferId,
  CommandId,
  CommandType,
  DependencyKind,
  ExecutionTarget,
  JobId,
  KernelId,
  NodeId,
  TaskId,
} from "../shared/types";
import { PollStatistic } from "../shared/__generated__/trace";
export { TracePayload } from "../shared/__generated__/trace";

export interface Job {
  commandId: CommandId;
  startTime: number;
  finishTime: number;
  submitTime: number;
  submittedTime: number;
  pollStatistic: PollStatistic | null;
  lineInVis: number;
}

interface Dependency<T extends CommandId | TaskId> {
  id: T;
  kind: DependencyKind;
}

export interface Task {
  name: string;
  target: ExecutionTarget;
  dependers: Array<Dependency<TaskId>>;
  dependees: Array<Dependency<TaskId>>;
}

export interface Subrange {
  min: number[];
  max: number[];
}

interface BufferAccess {
  id: BufferId;
  range: Subrange;
  mode: AccessMode;
}

export interface Command {
  nodeId: NodeId;
  type: CommandType;
  // Only relevant for TASK commands
  taskId: TaskId | null;
  kernelId: string | null;
  // Only relevant for transfer commands
  otherNode: NodeId | null;
  // Only relevant for transfer commands
  bufferId: BufferId | null;
  // Only relevant for TASK commands
  executionRange: Subrange | null;
  bufferAccesses: Array<BufferAccess>;
  dependers: Array<Dependency<CommandId>>;
  dependees: Array<Dependency<CommandId>>;
}

// TODO: Add the additional run meta info we have within the trace
export interface CelerityRun {
  id: string;
  executableName: string;
  // These times are relative to some arbitrary reference time point and cannot
  // be used to align/compare/synchronize different runs in any meaningful way.
  // TODO: We might as well shift every run to start at 0 and get rid of the startTime.
  startTime: number;
  endTime: number;
  jobs: Map<NodeId, Map<JobId, Job>>;
  jobVisLines: Map<NodeId, number>;
  tasks: Map<TaskId, Task>;
  commands: Map<CommandId, Command>;
  kernels: Set<KernelId>;
  shortestExecTime: number;
}
