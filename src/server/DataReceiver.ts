import protobuf from "protobufjs/minimal";
import long from "long";
import { TypedEmitter } from "tiny-typed-emitter";
import makeDebug from "debug";

import { TracePayload } from "../shared/__generated__/trace";
import { NodeId } from "../shared/types";
import { CelerityRun } from "./types";
import { processRunPayloads } from "./payloadProcessing";

// There currently appears to be a bug in ts-proto 1.41.0 (or protobufjs 6.10.2 which is a dependency of ts-proto),
// where the "Long" library is not properly loaded internally. It looks like it may also be related to ES6 modules
// (i.e., default exports in combination with require()). In any case, we can work around this by manually setting
// the Long library here, before using any ts-proto (or protobufjs) functionalities.
protobuf.util.Long = long;
protobuf.configure();

const debug = makeDebug("cpat:server:DataReceiver");

interface DataReceiverEvents {
  RunReceived: (run: CelerityRun) => void;
}

type PayloadCallback = (payload: TracePayload) => void;
type OnStreamComplete = () => void;

class ReceiverStream {
  private payloadCallback: PayloadCallback;
  private onComplete: OnStreamComplete;
  private nextPayloadSize: number = 0;
  private recvBuf = Buffer.from([]);
  // A stream may optionally decode payloads from multiple nodes,
  // and in that case we have to keep track which nodes have already been finalized.
  private finalizedNodes: Set<NodeId> = new Set();
  private isDone: boolean = false;

  constructor(payloadCallback: PayloadCallback, onComplete: OnStreamComplete) {
    this.payloadCallback = payloadCallback;
    this.onComplete = onComplete;
  }

  append(data: Buffer) {
    const PAYLOAD_SIZE_INFO_SIZE = 4; // uint32
    if (this.nextPayloadSize === 0) {
      this.nextPayloadSize = data.readUInt32LE(0);
    }

    this.recvBuf = Buffer.concat([this.recvBuf, data]);
    debug("Current recvBuf size is", this.recvBuf.byteLength);

    // Do this in a loop, as we may receive multiple completed payloads at once.
    while (
      this.recvBuf.byteLength >=
      this.nextPayloadSize + PAYLOAD_SIZE_INFO_SIZE
    ) {
      const payload = TracePayload.decode(
        this.recvBuf.slice(PAYLOAD_SIZE_INFO_SIZE),
        this.nextPayloadSize
      );
      debug(
        `Received payload with ${payload.jobs.length} jobs, ${payload.tasks.length} tasks, ${payload.commands.length} commands`
      );

      if (this.finalizedNodes.has(payload.sourceNodeId)) {
        throw new Error("Received payload for already finalized node");
      }

      this.payloadCallback(payload as TracePayload);

      this.recvBuf = this.recvBuf.slice(
        this.nextPayloadSize + PAYLOAD_SIZE_INFO_SIZE
      );

      if (payload.isFinalPayload) {
        this.finalizedNodes.add(payload.sourceNodeId);
        if (this.recvBuf.byteLength === 0) {
          this.isDone = true;
          this.onComplete();
        }
      }

      if (this.recvBuf.byteLength > 0) {
        this.nextPayloadSize = this.recvBuf.readUInt32LE(0);
      } else {
        this.nextPayloadSize = 0;
      }
    }
  }

  close() {
    // TODO: Send message to frontend instead of throwing an error.
    if (this.recvBuf.byteLength !== 0) {
      throw new Error("Stream closed before all data has been consumed");
    }
    if (!this.isDone) {
      throw new Error("Stream closed before final payload was received");
    }
  }
}

export default class DataReceiver extends TypedEmitter<DataReceiverEvents> {
  private activeStreams: Set<ReceiverStream> = new Set();
  private receivedData: Array<TracePayload> = [];

  createStream(): ReceiverStream {
    const stream = new ReceiverStream(this.append, () => {
      this.activeStreams.delete(stream);
      if (this.activeStreams.size === 0) this.finalize();
    });
    this.activeStreams.add(stream);
    return stream;
  }

  private append = (payload: TracePayload) => {
    this.receivedData.push(payload);
  };

  private finalize() {
    this.emit("RunReceived", processRunPayloads(this.receivedData));
    this.receivedData = [];
  }
}
