import { TracePayload } from "../../shared/__generated__/trace";
import DataReceiver from "../DataReceiver";

type PartialDeep<T> = {
  [P in keyof T]?: PartialDeep<T[P]>;
};

function wireEncodePayload(payload: PartialDeep<TracePayload>) {
  const encodedPayload = TracePayload.encode(
    TracePayload.fromJSON(payload)
  ).finish();
  const lengthEncoding = Buffer.alloc(4);
  lengthEncoding.writeUInt32LE(encodedPayload.byteLength, 0);
  return Buffer.concat([lengthEncoding, encodedPayload]);
}

describe("DataReceiver", () => {
  const payloadBuf1 = wireEncodePayload({
    sourceNodeId: 1,
    commands: [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }],
    runInfo: {
      referenceTimePoint: 4,
    },
    isFinalPayload: true,
  });
  const payloadBuf2 = wireEncodePayload({
    sourceNodeId: 2,
    jobs: [{ commandId: 1 }, { commandId: 2 }, { commandId: 3 }],
    runInfo: {
      referenceTimePoint: 13,
    },
    isFinalPayload: true,
  });

  test("supports the creation of streams to ingest chunked payloads from different sources", () => {
    const dr = new DataReceiver();
    const stream1 = dr.createStream();
    const stream2 = dr.createStream();
    stream1.append(payloadBuf1.slice(0, payloadBuf1.byteLength / 2));
    stream2.append(payloadBuf2.slice(0, payloadBuf2.byteLength / 2));
    stream1.append(payloadBuf1.slice(payloadBuf1.byteLength / 2));
    stream2.append(payloadBuf2.slice(payloadBuf2.byteLength / 2));
    stream1.close(); // Optional
    stream2.close(); // Optional
  });

  test("streams throw when closed before having received their final payload", () => {
    const payloadBuf = wireEncodePayload({
      isFinalPayload: false,
    });
    const dr = new DataReceiver();
    const stream = dr.createStream();
    stream.append(payloadBuf);
    expect(() => {
      stream.close();
    }).toThrow("Stream closed before final payload was received");
  });

  test("streams throw when closed before all data has been consumed", () => {
    const partial = Buffer.concat([
      payloadBuf1,
      payloadBuf2.slice(0, payloadBuf2.byteLength / 2),
    ]);
    const dr = new DataReceiver();
    const stream = dr.createStream();
    stream.append(partial);
    expect(() => {
      stream.close();
    }).toThrow("Stream closed before all data has been consumed");
  });

  // This is required for reading traces from files, which can contain trace data from any number of nodes.
  test("streams support reading payloads from multiple nodes", (done) => {
    expect.assertions(3);

    const combined = Buffer.concat([
      wireEncodePayload({
        sourceNodeId: 0,
        isFinalPayload: false,
        runInfo: {
          referenceTimePoint: 6,
        },
      }),
      wireEncodePayload({
        jobs: [{ commandId: 1 }, { commandId: 2 }],
        sourceNodeId: 1,
        isFinalPayload: true,
        runInfo: {
          referenceTimePoint: 7,
        },
      }),
      wireEncodePayload({
        jobs: [{ commandId: 3 }, { commandId: 4 }],
        sourceNodeId: 0,
        isFinalPayload: true,
      }),
    ]);

    const dr = new DataReceiver();
    dr.on("RunReceived", (run) => {
      expect(run.jobs.size).toEqual(2);
      expect(run.jobs.get(0)).toBeDefined();
      expect(run.jobs.get(1)).toBeDefined();
      done();
    });

    const stream = dr.createStream();
    stream.append(combined);
  });

  test("streams throw when receiving a payload for a node after having received a final payload", () => {
    const combined = Buffer.concat([
      wireEncodePayload({
        sourceNodeId: 0,
        isFinalPayload: true,
      }),
      wireEncodePayload({
        sourceNodeId: 1,
        isFinalPayload: true,
      }),
      wireEncodePayload({
        sourceNodeId: 0,
      }),
    ]);

    const dr = new DataReceiver();
    const stream = dr.createStream();
    expect(() => {
      stream.append(combined);
    }).toThrow(`Received payload for already finalized node`);
  });

  test("emits event with run containing data from all streams", (done) => {
    expect.assertions(4);

    const dr = new DataReceiver();
    dr.on("RunReceived", (run) => {
      expect(run.commands.size).toEqual(4);
      expect(run.jobs.size).toEqual(1);
      expect(run.jobs.get(2)).toBeDefined();
      expect(run.jobs.get(2)?.size).toEqual(3);
      done();
    });

    const stream1 = dr.createStream();
    const stream2 = dr.createStream();
    stream1.append(payloadBuf1);
    stream2.append(payloadBuf2);
  });

  // NOTE: We currently DON'T support CONCURRENTLY receiving runs
  test("does not conflate data from consecutively received runs", async () => {
    expect.assertions(4);

    const dr = new DataReceiver();

    await new Promise<void>((resolve) => {
      dr.once("RunReceived", (run) => {
        expect(run.commands.size).toEqual(4);
        expect(run.jobs.size).toEqual(0);
        resolve();
      });
      const stream1 = dr.createStream();
      stream1.append(payloadBuf1);
    });

    await new Promise<void>((resolve) => {
      dr.once("RunReceived", (run) => {
        expect(run.commands.size).toEqual(0);
        expect(run.jobs.size).toEqual(1);
        resolve();
      });
      const stream2 = dr.createStream();
      stream2.append(payloadBuf2);
    });
  });
});
