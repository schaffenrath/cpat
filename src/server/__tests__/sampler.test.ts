import {
  createJobFramesPerNode,
  getJobFramesInTimeSpan,
} from "../timeFrameProcessing";

import {
  getSampledFramesInTime,
  shouldSample,
  getTargetSampleLevel,
} from "../sampler";
import { JobId, TimeSpan, NodeId } from "../../shared/types";
import { Job } from "../types";
import config from "../../shared/config";
import { createEmptyJob, createRunFromJobs } from "../../util/testDataCreator";

describe("sampler", () => {
  let jobMap: Map<JobId, Job>;
  let jobsPerNode: Map<NodeId, Map<JobId, Job>>;

  beforeAll(() => {
    config.DISABLE_SAMPLING = false;
    config.SAMPLE_CONSTANT = 1000;
    config.SAMPLE_FACTOR = 1;
    config.SAMPLE_MIN_EXEC_TIME_RATIO = 0.8;
  });

  beforeEach(() => {
    jobMap = new Map();
    jobsPerNode = new Map();
  });

  describe("getSampledJobsInTime", () => {
    test("throws when no frames are provided", () => {
      expect(() => {
        shouldSample(
          {
            runId: "testRun",
            baseFrameLength: 0,
            frameLength: 0,
            isSampled: false,
            framesPerNode: {},
          },
          { start: 0, end: 2 }
        );
      }).toThrow(
        `The provided "jobFrames" argument does not contain any frames!`
      );
    });

    test("throws when view span is invalid", () => {
      const testJob = createEmptyJob();
      testJob.startTime = 1;
      testJob.finishTime = 2;
      jobMap.set(0, testJob);
      jobsPerNode.set(0, jobMap);
      const testRun = createRunFromJobs(jobsPerNode);
      const testFrame = createJobFramesPerNode(testRun);
      const validView: TimeSpan = { start: 0, end: 2 };

      // Check if it works with valid view span
      expect(shouldSample(testFrame, validView)).toEqual(expect.anything());

      let invalidView: TimeSpan = { start: -1, end: 3 };
      expect(() => {
        shouldSample(testFrame, invalidView);
      }).toThrow(
        `The view span [${invalidView.start} - ${invalidView.end}] is invalid!`
      );

      invalidView = { start: -2, end: -1 };
      expect(() => {
        shouldSample(testFrame, invalidView);
      }).toThrow(
        `The view span [${invalidView.start} - ${invalidView.end}] is invalid!`
      );

      invalidView = { start: 1, end: 1 };
      expect(() => {
        shouldSample(testFrame, invalidView);
      }).toThrow(
        `The view span [${invalidView.start} - ${invalidView.end}] is invalid!`
      );
    });

    test("returns non-sampled frames when jobs are small enough", () => {
      const jobCount = 4;
      for (let jobId = 0; jobId < jobCount; jobId++) {
        let newJob = createEmptyJob();
        newJob.startTime = jobId;
        newJob.finishTime = jobId + 2;
        jobMap.set(jobId, newJob);
      }
      const nodeId = 0;
      jobsPerNode.set(nodeId, jobMap);
      const testRun = createRunFromJobs(jobsPerNode);
      const testFrames = createJobFramesPerNode(testRun);
      const testViewSpan: TimeSpan = { start: 0, end: 100 };
      const willSample = shouldSample(testFrames, testViewSpan);
      expect(willSample).toEqual(false);
      const returnedFrames = getJobFramesInTimeSpan(
        testRun.id,
        testFrames,
        testViewSpan
      );
      expect(Object.keys(returnedFrames.framesPerNode[nodeId]).length).toEqual(
        1
      );
      expect(
        Object.keys(returnedFrames.framesPerNode[nodeId][0].jobs).length
      ).toEqual(jobCount);
    });

    test("returns non-sampled frames when view span is small enough", () => {
      const viewSpan: TimeSpan = { start: 7000, end: 9000 };
      const jobsInView = [7, 8];
      const jobCount = 10;
      for (let jobId = 0; jobId < jobCount; jobId++) {
        let newJob = createEmptyJob();
        newJob.startTime = jobId * 1000;
        newJob.finishTime = newJob.startTime + 1;
        jobMap.set(jobId, newJob);
      }
      const nodeId = 0;
      jobsPerNode.set(nodeId, jobMap);
      const testRun = createRunFromJobs(jobsPerNode);
      const testFrames = createJobFramesPerNode(testRun);
      const willSample = shouldSample(testFrames, viewSpan);
      expect(willSample).toEqual(false);
      const returnedFrames = getJobFramesInTimeSpan(
        testRun.id,
        testFrames,
        viewSpan
      );

      expect(Object.keys(returnedFrames.framesPerNode[nodeId]).length).toEqual(
        2
      );

      let combinedJobsIdsOfFrames: Array<string> = [];
      for (const frameStartTime in returnedFrames.framesPerNode[nodeId]) {
        combinedJobsIdsOfFrames = combinedJobsIdsOfFrames.concat(
          Object.keys(returnedFrames.framesPerNode[nodeId][frameStartTime].jobs)
        );
      }
      expect(combinedJobsIdsOfFrames).toEqual(jobsInView.map(String));
    });

    test("returns sampled frames in view span", () => {
      const viewSpan: TimeSpan = { start: 0, end: 5001 };
      const initFrameLength = config.SAMPLE_CONSTANT;
      const frameLength = getTargetSampleLevel(initFrameLength, viewSpan);
      const minSampleTimeUnit =
        Math.ceil(frameLength / config.SAMPLE_CONSTANT) * config.SAMPLE_FACTOR;
      const sampleExecTime = Math.floor(minSampleTimeUnit / 3) + 1;

      let shouldCollapseJobs: Array<JobId> = [0, 1, 2];
      for (let jobId of shouldCollapseJobs) {
        let shouldCollapseJob = createEmptyJob();
        shouldCollapseJob.startTime = jobId;
        shouldCollapseJob.finishTime =
          shouldCollapseJob.startTime + sampleExecTime;
        jobMap.set(jobId, shouldCollapseJob);
      }
      // Calculate minimal time between earlier jobs to avoid job fusing together
      const startTimeAfterMinExecRatioOffset = Math.ceil(
        (shouldCollapseJobs[shouldCollapseJobs.length - 1] + sampleExecTime) *
          (1 + (1 - config.SAMPLE_MIN_EXEC_TIME_RATIO))
      );

      let reduceFrameSizeJob = createEmptyJob();
      reduceFrameSizeJob.startTime = startTimeAfterMinExecRatioOffset;
      reduceFrameSizeJob.finishTime = startTimeAfterMinExecRatioOffset + 1;
      jobMap.set(3, reduceFrameSizeJob);
      let extendFrameJob = createEmptyJob();
      extendFrameJob.startTime = 5000;
      extendFrameJob.finishTime = extendFrameJob.startTime + 1;
      jobMap.set(4, extendFrameJob);

      const nodeId = 0;
      jobsPerNode.set(nodeId, jobMap);
      let testRun = createRunFromJobs(jobsPerNode);
      const testFrames = createJobFramesPerNode(testRun);
      const willSample = shouldSample(testFrames, viewSpan);
      expect(willSample).toEqual(true);
      const returnedSampleFrames = getSampledFramesInTime(
        testFrames,
        testRun,
        viewSpan
      );

      expect(
        Object.keys(returnedSampleFrames.framesPerNode[nodeId]).length
      ).toEqual(2);
      expect(
        returnedSampleFrames.framesPerNode[nodeId][0][0].jobIdList
      ).toEqual(shouldCollapseJobs);
    });

    // TODO: Add test for sampling at frame borders!
    // TODO: Add test for type ratio
    // TODO: Add test with more realistic data
  });
});
