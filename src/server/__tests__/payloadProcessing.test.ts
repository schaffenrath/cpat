import {
  assignJobsToLines,
  computePerNodeTimeOffsets,
  processJobPayloads,
  processTaskPayloads,
  processCommandPayloads,
} from "../payloadProcessing";
import { Task, Job } from "../types";
import {
  AccessMode,
  BufferId,
  CommandId,
  CommandType,
  DependencyKind,
  ExecutionTarget,
  NodeId,
  TaskId,
} from "../../shared/types";
import { createZeroPollStatistics } from "../../util/testDataCreator";

import { TracePayload } from "../types";
import { PollStatistic } from "../../shared/__generated__/trace";

function makePayload(data: Partial<TracePayload>): TracePayload {
  return {
    commands: [],
    jobs: [],
    tasks: [],
    isFinalPayload: false,
    sourceNodeId: 0,
    runInfo: undefined,
    ...data,
  };
}

describe("dataProcessing", () => {
  describe("assingJobsToLines", () => {
    test("assigns sequential jobs to the same line", () => {
      const jobs: Array<Job> = [
        {
          commandId: 1,
          startTime: 1,
          finishTime: 3,
          lineInVis: 99,
          submitTime: 0,
          submittedTime: 1,
          pollStatistic: createZeroPollStatistics(),
        },
        {
          commandId: 2,
          startTime: 4,
          finishTime: 5,
          lineInVis: 98,
          submitTime: 1,
          submittedTime: 2,
          pollStatistic: createZeroPollStatistics(),
        },
      ];
      assignJobsToLines(jobs);
      expect(jobs[0].lineInVis).toEqual(0);
      expect(jobs[1].lineInVis).toEqual(0);
    });

    test("assigns parallel jobs different lines", () => {
      const jobs: Array<Job> = [
        {
          commandId: 1,
          startTime: 1,
          finishTime: 3,
          lineInVis: 99,
          submitTime: 0,
          submittedTime: 1,
          pollStatistic: createZeroPollStatistics(),
        },
        {
          commandId: 2,
          startTime: 2,
          finishTime: 5,
          lineInVis: 98,
          submitTime: 1,
          submittedTime: 2,
          pollStatistic: createZeroPollStatistics(),
        },
      ];
      assignJobsToLines(jobs);
      // Jobs are processed in reverse order, which is why this one has the higher line.
      expect(jobs[0].lineInVis).toEqual(1);
      expect(jobs[1].lineInVis).toEqual(0);
    });

    test("returns the total number of lines required", () => {
      const jobs: Array<Job> = [
        {
          commandId: 1,
          startTime: 1,
          finishTime: 3,
          lineInVis: 99,
          submitTime: 0,
          submittedTime: 1,
          pollStatistic: createZeroPollStatistics(),
        },
        {
          commandId: 2,
          startTime: 2,
          finishTime: 5,
          lineInVis: 98,
          submitTime: 1,
          submittedTime: 2,
          pollStatistic: createZeroPollStatistics(),
        },
        {
          commandId: 3,
          startTime: 2,
          finishTime: 6,
          lineInVis: 97,
          submitTime: 2,
          submittedTime: 3,
          pollStatistic: createZeroPollStatistics(),
        },
        {
          commandId: 2,
          startTime: 6,
          finishTime: 7,
          lineInVis: 96,
          submitTime: 5,
          submittedTime: 6,
          pollStatistic: createZeroPollStatistics(),
        },
      ];
      expect(assignJobsToLines(jobs)).toEqual(3);
    });
  });

  describe("computePerNodeTimeOffsets", () => {
    const makeTimePointPayload = (
      sourceNodeId: NodeId,
      referenceTimePoint: number
    ): TracePayload =>
      makePayload({
        sourceNodeId,
        runInfo: {
          executableArgs: [],
          executableName: "",
          referenceTimePoint,
        },
      });

    test("returns offsets relative to the earliest reference time point", () => {
      const offsets = computePerNodeTimeOffsets([
        makeTimePointPayload(0, 100),
        makeTimePointPayload(1, 50),
        makeTimePointPayload(2, 230),
        makeTimePointPayload(3, 20),
      ]);
      expect(offsets.get(0)).toEqual(80);
      expect(offsets.get(1)).toEqual(30);
      expect(offsets.get(2)).toEqual(210);
      expect(offsets.get(3)).toEqual(0);
    });

    test("throws when receiving run-infos for the same node more than once", () => {
      expect(() => {
        computePerNodeTimeOffsets([
          makeTimePointPayload(0, 100),
          makeTimePointPayload(0, 50),
        ]);
      }).toThrow("Received more than one run-info payload for node 0.");
    });
  });

  describe("processJobPayloads", () => {
    type RawPollStatistic = {
      minDuration: number;
      maxDuration: number;
      avgDuration: number;
      sampleCount: number;
    };
    type RawJob = {
      commandId: CommandId;
      startTime: number;
      finishTime: number;
      submitTime: number;
      submittedTime: number;
      pollStatistic: PollStatistic;
    };
    const makeJobsPayload = (nodeId: NodeId, rawJobs: Array<RawJob>) =>
      makePayload({
        sourceNodeId: nodeId,
        jobs: rawJobs,
      });

    test("returns a map of processed jobs indexed by node id", () => {
      const { jobsPerNode } = processJobPayloads(
        [
          makeJobsPayload(0, [
            {
              commandId: 0,
              startTime: 33,
              finishTime: 44,
              submitTime: 32,
              submittedTime: 33,
              pollStatistic: createZeroPollStatistics(),
            },
          ]),
          makeJobsPayload(1, [
            {
              commandId: 1,
              startTime: 29,
              finishTime: 50,
              submitTime: 28,
              submittedTime: 29,
              pollStatistic: createZeroPollStatistics(),
            },
          ]),
          makeJobsPayload(0, [
            {
              commandId: 2,
              startTime: 12,
              finishTime: 45,
              submitTime: 11,
              submittedTime: 12,
              pollStatistic: createZeroPollStatistics(),
            },
          ]),
        ],
        new Map([
          [0, 0],
          [1, 0],
        ])
      );

      expect(jobsPerNode.size).toEqual(2);
      const jobs0 = jobsPerNode.get(0);
      expect(jobs0).toBeDefined();
      expect(jobs0?.size).toEqual(2);
      expect(jobs0?.get(0)).toMatchObject({
        commandId: 0,
        startTime: 33,
        finishTime: 44,
      });
      expect(jobs0?.get(2)).toMatchObject({
        commandId: 2,
        startTime: 12,
        finishTime: 45,
      });
      const jobs1 = jobsPerNode.get(1);
      expect(jobs1).toBeDefined();
      expect(jobs1?.size).toEqual(1);
      expect(jobs1?.get(1)).toMatchObject({
        commandId: 1,
        startTime: 29,
        finishTime: 50,
      });
    });

    test("correctly shifts time points by the provided per-node offset", () => {
      const { jobsPerNode } = processJobPayloads(
        [
          makeJobsPayload(0, [
            {
              commandId: 0,
              startTime: 33,
              finishTime: 44,
              submitTime: 32,
              submittedTime: 33,
              pollStatistic: createZeroPollStatistics(),
            },
          ]),
          makeJobsPayload(1, [
            {
              commandId: 1,
              startTime: 29,
              finishTime: 50,
              submitTime: 27,
              submittedTime: 28,
              pollStatistic: createZeroPollStatistics(),
            },
          ]),
          makeJobsPayload(0, [
            {
              commandId: 2,
              startTime: 12,
              finishTime: 45,
              submitTime: 10,
              submittedTime: 11,
              pollStatistic: createZeroPollStatistics(),
            },
          ]),
        ],
        new Map([
          [0, 10],
          [1, 0],
        ])
      );

      expect(jobsPerNode.get(0)?.get(0)).toMatchObject({
        startTime: 23,
        finishTime: 34,
      });
      expect(jobsPerNode.get(0)?.get(2)).toMatchObject({
        startTime: 2,
        finishTime: 35,
      });
      expect(jobsPerNode.get(1)?.get(1)).toMatchObject({
        startTime: 29,
        finishTime: 50,
      });
    });

    test("returns the overall start and end time of the run, taking shifted time points into account", () => {
      const { runStartTime, runEndTime } = processJobPayloads(
        [
          makeJobsPayload(0, [
            {
              commandId: 0,
              startTime: 33,
              finishTime: 44,
              submitTime: 32,
              submittedTime: 33,
              pollStatistic: createZeroPollStatistics(),
            },
          ]),
          makeJobsPayload(1, [
            {
              commandId: 1,
              startTime: 29,
              finishTime: 50,
              submitTime: 27,
              submittedTime: 28,
              pollStatistic: createZeroPollStatistics(),
            },
          ]),
          makeJobsPayload(0, [
            {
              commandId: 2,
              startTime: 12,
              finishTime: 45,
              submitTime: 10,
              submittedTime: 11,
              pollStatistic: createZeroPollStatistics(),
            },
          ]),
        ],
        new Map([
          [0, 10],
          [1, 0],
        ])
      );
      expect(runStartTime).toEqual(2);
      expect(runEndTime).toEqual(50);
    });

    test("throws if for some nodes no offset is provided", () => {
      expect(() => {
        processJobPayloads(
          [
            makeJobsPayload(0, [
              {
                commandId: 0,
                startTime: 33,
                finishTime: 44,
                submitTime: 31,
                submittedTime: 32,
                pollStatistic: createZeroPollStatistics(),
              },
            ]),
          ],
          new Map()
        );
      }).toThrow("No time offset provided for node 0.");
    });
  });

  describe("processTaskPayloads", () => {
    type RawTask = {
      id: TaskId;
      target: ExecutionTarget;
      dependencies: Array<{
        id: TaskId;
        kind: DependencyKind;
      }>;
    };
    const makeTasksPayload = (rawTasks: Array<RawTask>) =>
      makePayload({
        tasks: rawTasks.map((rt) => ({
          id: rt.id,
          target: rt.target,
          name: `Task ${rt.id}`,
          dependencies: rt.dependencies,
        })),
      });

    test("returns a map of tasks indexed by their id, with correctly inferred dependency information", () => {
      const tasks = processTaskPayloads([
        makeTasksPayload([
          { id: 1, target: ExecutionTarget.DEVICE, dependencies: [] },
          {
            id: 2,
            target: ExecutionTarget.HOST,
            dependencies: [{ id: 1, kind: DependencyKind.TRUE_DEP }],
          },
        ]),
        makeTasksPayload([
          {
            id: 3,
            target: ExecutionTarget.DEVICE,
            dependencies: [
              { id: 1, kind: DependencyKind.ANTI_DEP },
              { id: 2, kind: DependencyKind.TRUE_DEP },
            ],
          },
        ]),
      ]);

      expect(tasks.size).toEqual(3);
      expect(tasks.get(1)).toMatchObject({
        target: ExecutionTarget.DEVICE,
        dependees: [],
        dependers: [
          {
            id: 2,
            kind: DependencyKind.TRUE_DEP,
          },
          {
            id: 3,
            kind: DependencyKind.ANTI_DEP,
          },
        ],
      });
      expect(tasks.get(2)).toMatchObject({
        target: ExecutionTarget.HOST,
        dependees: [
          {
            id: 1,
            kind: DependencyKind.TRUE_DEP,
          },
        ],
        dependers: [
          {
            id: 3,
            kind: DependencyKind.TRUE_DEP,
          },
        ],
      });
      expect(tasks.get(3)).toMatchObject({
        target: ExecutionTarget.DEVICE,
        dependees: [
          {
            id: 1,
            kind: DependencyKind.ANTI_DEP,
          },
          {
            id: 2,
            kind: DependencyKind.TRUE_DEP,
          },
        ],
        dependers: [],
      });
    });

    test("throws if tasks are not provided in topological order", () => {
      expect(() => {
        processTaskPayloads([
          makeTasksPayload([
            {
              id: 2,
              target: ExecutionTarget.HOST,
              dependencies: [{ id: 1, kind: DependencyKind.TRUE_DEP }],
            },
            { id: 1, target: ExecutionTarget.DEVICE, dependencies: [] },
          ]),
        ]);
      }).toThrow(
        "Encountered definition for task 2 with dependency on 1 before encountering definition for task 1."
      );
    });
  });

  describe("processCommandPayloads", () => {
    type Box3D = {
      min0: number;
      min1: number;
      min2: number;
      max0: number;
      max1: number;
      max2: number;
    };

    type RawCommand = {
      id: CommandId;
      nodeId: NodeId;
      type: CommandType;
      taskId?: TaskId;
      otherNodeId?: NodeId;
      bufferId?: BufferId;
      executionRange?: Box3D;
      bufferAccesses?: Array<{
        bufferId: BufferId;
        range: Box3D;
        mode: AccessMode;
      }>;
      dependencies: Array<{
        id: TaskId;
        kind: DependencyKind;
      }>;
    };
    const makeCommandsPayload = (rawCommands: Array<RawCommand>) =>
      makePayload({
        commands: rawCommands.map((rc) => ({
          id: rc.id,
          nodeId: rc.nodeId,
          type: rc.type,
          taskId: rc.taskId || 0,
          otherNodeId: rc.otherNodeId || 0,
          bufferId: rc.bufferId || 0,
          executionRange: {
            min0: rc.executionRange?.min0 || 0,
            min1: rc.executionRange?.min1 || 0,
            min2: rc.executionRange?.min2 || 0,
            max0: rc.executionRange?.max0 || 0,
            max1: rc.executionRange?.max1 || 0,
            max2: rc.executionRange?.max2 || 0,
          },
          bufferAccesses: rc.bufferAccesses || [],
          dependencies: rc.dependencies,
        })),
      });

    test("returns a map of commands indexed by their id, with correctly inferred dependency information", () => {
      const commands = processCommandPayloads(
        [
          makeCommandsPayload([
            { id: 1, nodeId: 1, type: CommandType.NOP, dependencies: [] },
            {
              id: 2,
              nodeId: 1,
              type: CommandType.NOP,
              dependencies: [{ id: 1, kind: DependencyKind.TRUE_DEP }],
            },
          ]),
          makeCommandsPayload([
            {
              id: 3,
              nodeId: 1,
              type: CommandType.NOP,
              dependencies: [
                { id: 1, kind: DependencyKind.ANTI_DEP },
                { id: 2, kind: DependencyKind.TRUE_DEP },
              ],
            },
          ]),
        ],
        new Map()
      );
      expect(commands.size).toEqual(3);
      expect(commands.get(1)).toMatchObject({
        type: CommandType.NOP,
        taskId: null,
        bufferId: null,
        otherNode: null,
        executionRange: null,
        bufferAccesses: [],
        dependees: [],
        dependers: [
          {
            id: 2,
            kind: DependencyKind.TRUE_DEP,
          },
          {
            id: 3,
            kind: DependencyKind.ANTI_DEP,
          },
        ],
      });
      expect(commands.get(2)).toMatchObject({
        type: CommandType.NOP,
        taskId: null,
        bufferId: null,
        otherNode: null,
        executionRange: null,
        bufferAccesses: [],
        dependees: [
          {
            id: 1,
            kind: DependencyKind.TRUE_DEP,
          },
        ],
        dependers: [
          {
            id: 3,
            kind: DependencyKind.TRUE_DEP,
          },
        ],
      });
      expect(commands.get(3)).toMatchObject({
        type: CommandType.NOP,
        taskId: null,
        bufferId: null,
        otherNode: null,
        executionRange: null,
        bufferAccesses: [],
        dependees: [
          {
            id: 1,
            kind: DependencyKind.ANTI_DEP,
          },
          {
            id: 2,
            kind: DependencyKind.TRUE_DEP,
          },
        ],
        dependers: [],
      });
    });

    test("sets task id, execution range, buffer accesses; infers kernel name for task commands", () => {
      const tasks = new Map<TaskId, Task>([
        [
          1,
          {
            target: ExecutionTarget.DEVICE,
            name: "COMPUTE (MyKernel)",
            dependers: [],
            dependees: [],
          },
        ],
        [
          2,
          {
            target: ExecutionTarget.HOST,
            name: "Some host task",
            dependers: [],
            dependees: [],
          },
        ],
      ]);
      const makeBox3D = (x: number) => ({
        min0: (x + 0) * 10,
        min1: (x + 1) * 10,
        min2: (x + 2) * 10,
        max0: (x + 3) * 10,
        max1: (x + 4) * 10,
        max2: (x + 5) * 10,
      });
      const commands = processCommandPayloads(
        [
          makeCommandsPayload([
            {
              id: 1,
              nodeId: 1,
              type: CommandType.TASK,
              taskId: 1,
              executionRange: makeBox3D(1),
              bufferAccesses: [
                {
                  bufferId: 1,
                  mode: AccessMode.READ_WRITE,
                  range: makeBox3D(3),
                },
              ],
              dependencies: [],
            },
            {
              id: 2,
              nodeId: 1,
              type: CommandType.TASK,
              taskId: 2,
              executionRange: makeBox3D(2),
              dependencies: [],
            },
          ]),
        ],
        tasks
      );
      const makeSubrange = (box: ReturnType<typeof makeBox3D>) => ({
        min: [box.min0, box.min1, box.min2],
        max: [box.max0, box.max1, box.max2],
      });
      expect(commands.size).toEqual(2);
      expect(commands.get(1)).toMatchObject({
        taskId: 1,
        executionRange: makeSubrange(makeBox3D(1)),
        bufferAccesses: [
          {
            id: 1,
            mode: AccessMode.READ_WRITE,
            range: makeSubrange(makeBox3D(3)),
          },
        ],
        kernelId: "MyKernel",
      });
      expect(commands.get(2)).toMatchObject({
        taskId: 2,
        executionRange: makeSubrange(makeBox3D(2)),
        bufferAccesses: [],
        kernelId: null,
      });
    });

    test("throws if a task command does not include an execution range", () => {
      const tasks = new Map<TaskId, Task>([
        [
          1,
          {
            target: ExecutionTarget.DEVICE,
            name: "COMPUTE (MyKernel)",
            dependers: [],
            dependees: [],
          },
        ],
      ]);
      const payload = makeCommandsPayload([
        {
          id: 1,
          nodeId: 1,
          type: CommandType.TASK,
          taskId: 1,
          bufferAccesses: [],
          dependencies: [],
        },
      ]);
      delete payload.commands[0].executionRange;
      expect(() => {
        processCommandPayloads([payload], tasks);
      }).toThrow("Received TASK command without execution range");
    });

    test("throws if a command's buffer accesses do not include a range", () => {
      const tasks = new Map<TaskId, Task>([
        [
          1,
          {
            target: ExecutionTarget.DEVICE,
            name: "COMPUTE (MyKernel)",
            dependers: [],
            dependees: [],
          },
        ],
      ]);
      const payload = makeCommandsPayload([
        {
          id: 1,
          nodeId: 1,
          type: CommandType.TASK,
          taskId: 1,
          bufferAccesses: [
            {
              bufferId: 1,
              mode: AccessMode.READ_WRITE,
              // @ts-ignore
              range: undefined,
            },
          ],
          dependencies: [],
        },
      ]);
      expect(() => {
        processCommandPayloads([payload], tasks);
      }).toThrow("Received command buffer access without range");
    });

    test("sets other node id and buffer id for transfer commands", () => {
      const commands = processCommandPayloads(
        [
          makeCommandsPayload([
            {
              id: 1,
              nodeId: 1,
              otherNodeId: 2,
              bufferId: 4,
              type: CommandType.PUSH,
              dependencies: [],
            },
            {
              id: 2,
              nodeId: 2,
              otherNodeId: 1,
              bufferId: 4,
              type: CommandType.AWAIT_PUSH,
              dependencies: [],
            },
          ]),
        ],
        new Map()
      );

      expect(commands.size).toEqual(2);
      expect(commands.get(1)).toMatchObject({
        otherNode: 2,
        bufferId: 4,
      });
      expect(commands.get(2)).toMatchObject({
        otherNode: 1,
        bufferId: 4,
      });
    });

    test("throws if commands are not provided in topological order", () => {
      expect(() => {
        processCommandPayloads(
          [
            makeCommandsPayload([
              {
                id: 2,
                nodeId: 1,
                type: CommandType.NOP,
                dependencies: [{ id: 1, kind: DependencyKind.TRUE_DEP }],
              },
              { id: 1, nodeId: 1, type: CommandType.NOP, dependencies: [] },
            ]),
          ],
          new Map()
        );
      }).toThrow(
        "Encountered definition for command 2 with dependency on 1 before encountering definition for command 1."
      );
    });
  });
});
