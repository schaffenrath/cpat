import config from "../../shared/config";
import { createJobFramesPerNode } from "../timeFrameProcessing";
import { Job, NodeId, JobId } from "../../shared/types";
import { createEmptyJob, createRunFromJobs } from "../../util/testDataCreator";

describe("timeFrameProcessing", () => {
  let jobMap: Map<JobId, Job>;
  let jobsPerNode: Map<NodeId, Map<JobId, Job>>;
  beforeEach(() => {
    jobMap = new Map();
    jobsPerNode = new Map();
  });

  describe("getJobsInFrames", () => {
    test("throws when no nodes are available", () => {
      const testRun = createRunFromJobs(jobsPerNode);
      expect(() => {
        createJobFramesPerNode(testRun);
      }).toThrow("Cannot create job frames, since no nodes are available.");
    });

    test("throws when node has no jobs", () => {
      jobsPerNode.set(0, jobMap);
      const testRun = createRunFromJobs(jobsPerNode);
      expect(() => {
        createJobFramesPerNode(testRun);
      }).toThrow("Cannot create job frames, since node 0 has no jobs.");
    });

    test("create frame for a single job", () => {
      let testJob: Job = createEmptyJob();
      testJob.startTime = 1;
      testJob.finishTime = 2;
      const testJobId = 0;
      const testNode = 0;
      jobMap.set(testJobId, testJob);
      jobsPerNode.set(testNode, jobMap);
      const testRun = createRunFromJobs(jobsPerNode);
      const jobFramePerNode = createJobFramesPerNode(testRun);
      expect(Object.keys(jobFramePerNode.framesPerNode).length).toEqual(1);
      expect(
        Object.keys(jobFramePerNode.framesPerNode[testNode]).length
      ).toEqual(1);
      expect(
        Object.keys(jobFramePerNode.framesPerNode[testNode][0].jobs)
      ).toEqual([testJobId.toString()]);
    });

    test("correctly compute job frame length", () => {
      const shortestJobExecTime = 1;
      let shortJob = createEmptyJob();
      shortJob.startTime = 1;
      shortJob.finishTime = 1 + shortestJobExecTime;
      let longJob = createEmptyJob();
      longJob.startTime = 3;
      longJob.finishTime = 9;
      jobMap.set(0, shortJob);
      jobMap.set(1, longJob);
      const testNode = 0;
      jobsPerNode.set(testNode, jobMap);
      const testRun = createRunFromJobs(jobsPerNode);
      const jobFramePerNode = createJobFramesPerNode(testRun);
      const targetFrameLength = shortestJobExecTime * config.SAMPLE_CONSTANT;
      expect(jobFramePerNode.frameLength).toEqual(targetFrameLength);
    });

    test("create the correct amount of job frames", () => {
      const shortestJobExecTime = 1;
      const earliestJobStart = 1;
      const latestJobFinish = 3000;
      let shortJob = createEmptyJob();
      shortJob.startTime = earliestJobStart;
      shortJob.finishTime = earliestJobStart + shortestJobExecTime;
      let longJob = createEmptyJob();
      longJob.startTime = 3;
      longJob.finishTime = latestJobFinish;
      jobMap.set(0, shortJob);
      jobMap.set(1, longJob);
      const testNode = 0;
      jobsPerNode.set(testNode, jobMap);
      const testRun = createRunFromJobs(jobsPerNode);
      const jobFramesPerNode = createJobFramesPerNode(testRun);
      const targetFrameLength = shortestJobExecTime * config.SAMPLE_CONSTANT;
      const targetFrameCount = Math.ceil(
        (latestJobFinish - earliestJobStart) / targetFrameLength
      );
      expect(
        Object.keys(jobFramesPerNode.framesPerNode[testNode]).length
      ).toEqual(targetFrameCount);
    });

    test("create frames for multiple nodes", () => {
      const nodeCount = 3;
      const jobPerNodeCount = 4;
      for (let nodeId = 0; nodeId < nodeCount; nodeId++) {
        for (let jobId = 0; jobId < jobPerNodeCount; jobId++) {
          let newJob = createEmptyJob();
          newJob.startTime = jobId;
          newJob.finishTime = jobId + jobPerNodeCount;
          newJob.lineInVis = jobId;
          jobMap.set(jobId, newJob);
        }
        jobsPerNode.set(nodeId, new Map(jobMap));
        jobMap = new Map();
      }
      const testRun = createRunFromJobs(jobsPerNode);
      const jobFramesPerNode = createJobFramesPerNode(testRun);
      expect(Object.keys(jobFramesPerNode.framesPerNode).length).toEqual(
        nodeCount
      );
    });
  });
});
