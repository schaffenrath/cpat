import config from "../shared/config";
import {
  TimeSpan,
  JobDict,
  JobFrameDict,
  JobFramesOfNode,
  JobFramePayload,
  CommandDict,
  TaskDict,
  DependencyInfo,
  CommandId,
  TaskId,
} from "../shared/types";
import { CelerityRun, Job, Command } from "./types";
import { JobId, RunId, Task, Dependency } from "../shared/types";

export interface JobInSpan {
  jobId: JobId;
  job: Job;
}

/**
 * Assign each job to a time frame, depending on its start and finish time.
 * A job is stored in a frame, if it is running in the time span of that frame.
 *
 * A frame covers a time span of: [x, x+frameLength)
 *
 * Celerity sends jobs ordered by the finish time! We therefore need to build
 * the time frames in reverse order!
 *
 * @param jobsOfNode The map of jobs as processed by {@link processJobPayload}.
 * @param runStart The run, obtained by the {@link DataStorage}.
 * @param frameLength The target length of a single frame.
 */

function splitJobsInTimeFrames(
  run: CelerityRun,
  jobsOfNode: Map<JobId, Job>,
  frameLength: number
): JobFrameDict {
  let jobFrames: JobFrameDict = {};

  // NOTE: The job map has to be converted to an array, since
  // we need to build the time frames in reverse order due to
  // sorting after finish time!
  let jobList = Array.from(jobsOfNode);

  let frameStart =
    Math.floor((run.endTime - run.startTime) / frameLength) * frameLength;
  let jobDict: JobDict = {};
  let commandDict: CommandDict = {};
  let taskDict: TaskDict = {};
  let jobsExceedingFrame: Array<JobId> = [];

  /**
   * Add jobs that exceed the previous timeFrame span into dictionaries.
   * Such jobs are stored multiple times to simplify access and avoid searching.
   */
  function addExceedingJobs() {
    let jobsStartedBefore: Array<JobId> = [];
    for (let exceedJobId of jobsExceedingFrame) {
      const exceedJob = jobsOfNode.get(exceedJobId) as Job;
      jobDict[exceedJobId] = exceedJob;
      const command = run.commands.get(exceedJob.commandId) as Command;
      commandDict[exceedJob.commandId] = command;
      if (command.taskId !== null) {
        taskDict[command.taskId] = run.tasks.get(command.taskId) as Task;
      }
      if (
        (jobsOfNode.get(exceedJobId) as Job).startTime - run.startTime <
        frameStart
      ) {
        jobsStartedBefore.push(exceedJobId);
      }
    }
    return jobsStartedBefore;
  }

  const jobListLength = jobList.length;
  let jobIdx = jobListLength - 1;
  let jobId = jobList[jobIdx][0];
  let job = jobList[jobIdx][1];
  while (frameStart >= 0) {
    while (jobIdx >= 0 && job.finishTime - run.startTime >= frameStart) {
      // If job exceeds current timeFrame span, save it for the next timeFrame.
      if (job.startTime - run.startTime < frameStart) {
        jobsExceedingFrame.push(jobId);
      }
      jobDict[jobId] = job;
      const commandOfJob = run.commands.get(job.commandId) as Command;
      if (commandOfJob === undefined)
        throw new Error(
          `Command ${job.commandId} of job ${jobId} was not found!`
        );
      commandDict[job.commandId] = commandOfJob;
      if (commandOfJob.taskId !== null) {
        taskDict[commandOfJob.taskId] = run.tasks.get(
          commandOfJob.taskId
        ) as Task;
      }
      jobIdx -= 1;
      if (jobIdx < 0) break;
      jobId = jobList[jobIdx][0];
      job = jobList[jobIdx][1];
    }
    jobFrames[frameStart] = Object.assign(
      {},
      { jobs: jobDict, commands: commandDict, tasks: taskDict }
    );
    jobDict = {};
    commandDict = {};
    taskDict = {};
    jobsExceedingFrame = addExceedingJobs();
    frameStart -= frameLength;
  }
  return jobFrames;
}

/**
 * Build timeFrames from a run.
 *
 * @param run The run converted into timeFrames.
 */
export function createJobFramesPerNode(run: CelerityRun): JobFramePayload {
  if (run.jobs.size === 0)
    throw new Error("Cannot create job frames, since no nodes are available.");
  let initialFrameLength = config.SAMPLE_CONSTANT * run.shortestExecTime;
  let jobFramesPerNode: JobFramesOfNode = {};
  run.jobs.forEach((jobs, nodeId) => {
    if (jobs.size === 0)
      throw new Error(
        `Cannot create job frames, since node ${nodeId} has no jobs.`
      );
    const jobFrames = splitJobsInTimeFrames(run, jobs, initialFrameLength);
    jobFramesPerNode[nodeId] = jobFrames;
  });
  return {
    runId: run.id,
    baseFrameLength: initialFrameLength,
    frameLength: initialFrameLength,
    isSampled: false,
    framesPerNode: jobFramesPerNode,
  };
}

/**
 * Puts all jobs from timeFrames within the given view span into an array.
 * An array is necessary, since we have to process jobs in reverse
 * order, since they are ordered after finishTime!
 *
 * @param jobFrames The timeFrames holding the jobs.
 * @param frameLength The length of each timeFrame.
 * @param viewSpan The view span in which the jobs have to be.
 */
export function getJobsInTimeSpan(
  jobFrames: JobFrameDict,
  frameLength: number,
  viewSpan: TimeSpan
): Array<JobInSpan> {
  let frameStartTime = Math.floor(viewSpan.start / frameLength) * frameLength;
  let jobsInSpan: Array<JobInSpan> = [];
  let frameInViewSpan = true;
  while (frameInViewSpan) {
    const jobsInFrame = jobFrames[frameStartTime];
    if (jobsInFrame === undefined) {
      break;
    }
    for (const jobId in jobsInFrame.jobs) {
      jobsInSpan.push({
        jobId: Number(jobId),
        job: jobsInFrame.jobs[Number(jobId)],
      });
    }
    if (frameStartTime + frameLength >= viewSpan.end) {
      frameInViewSpan = false;
    } else {
      frameStartTime += frameLength;
    }
  }
  return jobsInSpan;
}

/**
 * Get set of timeFrames that cover a specific time span.
 *
 * @param runId The run related to the timeFrames.
 * @param frames The set of all timeFrames of a run.
 * @param viewSpan The view span to determine the set of timeFrames.
 */
export function getJobFramesInTimeSpan(
  runId: RunId,
  frames: JobFramePayload,
  viewSpan: TimeSpan
): JobFramePayload {
  const frameStartTime =
    Math.floor(viewSpan.start / frames.frameLength) * frames.frameLength;
  let framesPerNode: JobFramesOfNode = {};

  for (const nodeId in frames.framesPerNode) {
    let nodeFrameList: JobFrameDict = {};
    let nodeFrameStart = frameStartTime;
    while (nodeFrameStart < viewSpan.end) {
      nodeFrameList[nodeFrameStart] =
        frames.framesPerNode[nodeId][nodeFrameStart];
      nodeFrameStart += frames.frameLength;
    }
    framesPerNode[nodeId] = nodeFrameList;
  }
  return {
    runId,
    baseFrameLength: frames.frameLength,
    frameLength: frames.frameLength,
    isSampled: false,
    framesPerNode: framesPerNode,
  };
}

/**
 * Get all timeFrames that contain a specific job.
 *
 * @param frames The set of all timeFrames for a run.
 * @param requestInfo The information for searched job.
 */
export function getJobFramesContainingJob(
  frames: JobFramePayload,
  requestInfo: DependencyInfo
): JobFramePayload {
  const possibleFrames = getJobFramesInTimeSpan(
    requestInfo.runId,
    frames,
    requestInfo.inRange
  );
  const framesOfNode = possibleFrames.framesPerNode[requestInfo.node];
  let targetFrameStartTime = -1;
  for (const frameStartTime in framesOfNode) {
    if (framesOfNode[frameStartTime].jobs[requestInfo.targetId] !== undefined) {
      targetFrameStartTime = Number(frameStartTime);
      break;
    }
  }
  if (targetFrameStartTime !== -1) {
    let jobFramesOfNode: JobFramesOfNode = {};
    for (const nodeId in frames.framesPerNode) {
      jobFramesOfNode[nodeId] = {};
      jobFramesOfNode[nodeId][targetFrameStartTime] =
        framesOfNode[targetFrameStartTime];
    }
    return {
      baseFrameLength: frames.baseFrameLength,
      frameLength: frames.baseFrameLength,
      runId: requestInfo.runId,
      isSampled: false,
      framesPerNode: jobFramesOfNode,
    };
  }
  throw new Error(
    `Data got lost! Could not find frame with job ${requestInfo.targetId}.`
  );
}

/**
 * Get the set of timeFrames containing either the dependee or depender of
 * a specific command.
 *
 * @param frames The set of all timeFrames for a run.
 * @param requestInfo The information about the searched target, describing
 * if searching for dependee or depender.
 * @param run The run related to the given timeFrames.
 */
export function getJobFramesOfCommandDependencies(
  frames: JobFramePayload,
  requestInfo: DependencyInfo,
  run: CelerityRun
): JobFramePayload {
  let framesPerNode: JobFramesOfNode = {};
  let targetCommand: Command | null = null;

  const searchDependee = requestInfo.dependencyDirection === "Dependee";

  // Determine the search area based on the search target.
  let frameStartTimeIter = searchDependee
    ? Math.ceil(requestInfo.inRange.end / frames.baseFrameLength) *
        frames.baseFrameLength -
      frames.baseFrameLength
    : Math.floor(requestInfo.inRange.start / frames.baseFrameLength) *
      frames.baseFrameLength;
  let frameStartCondition = searchDependee
    ? frameStartTimeIter >= 0
    : frameStartTimeIter < run.endTime - run.startTime;
  // Search for timeFrame containing the search target.
  while (frameStartCondition) {
    if (
      frames.framesPerNode[requestInfo.node][frameStartTimeIter].commands[
        requestInfo.targetId
      ] !== undefined
    ) {
      targetCommand =
        frames.framesPerNode[requestInfo.node][frameStartTimeIter].commands[
          requestInfo.targetId
        ];
      break;
    }
    if (searchDependee) frameStartTimeIter -= frames.baseFrameLength;
    else frameStartTimeIter += frames.baseFrameLength;
    frameStartCondition = searchDependee
      ? frameStartTimeIter >= 0
      : frameStartTimeIter < run.endTime - run.startTime;
  }

  if (targetCommand === null || frameStartTimeIter < 0)
    throw new Error(`Command ${requestInfo.targetId} not found in frames!`);

  let dependencies = searchDependee
    ? [...targetCommand.dependees]
    : [...targetCommand.dependers];

  // If correct timeFrame is found, search until all dependees/dependers are found.
  while (dependencies.length !== 0) {
    let dependenciesRemain: Array<Dependency<CommandId>> = [];
    let shouldAddFrame = false;
    const jobsInFrame =
      frames.framesPerNode[requestInfo.node][frameStartTimeIter].commands;
    const depLength = dependencies.length;
    for (let depIdx = 0; depIdx < depLength; depIdx++) {
      if (jobsInFrame[dependencies[depIdx].id] !== undefined) {
        shouldAddFrame = true;
      } else {
        dependenciesRemain.push(dependencies[depIdx]);
      }
    }
    if (shouldAddFrame) {
      for (const nodeId in frames.framesPerNode) {
        if (framesPerNode[nodeId] === undefined) framesPerNode[nodeId] = {};
        framesPerNode[nodeId][frameStartTimeIter] =
          frames.framesPerNode[nodeId][frameStartTimeIter];
      }
    }
    if (searchDependee) frameStartTimeIter -= frames.baseFrameLength;
    else frameStartTimeIter += frames.baseFrameLength;
    if (
      frameStartTimeIter < 0 ||
      frameStartTimeIter >= run.endTime - run.startTime
    )
      break;
    dependencies = dependenciesRemain;
  }

  return {
    baseFrameLength: frames.baseFrameLength,
    frameLength: frames.baseFrameLength,
    isSampled: false,
    runId: requestInfo.runId,
    framesPerNode,
  };
}

/**
 * Get set of timeFrames containing the dependee or depender of
 * a specific task.
 *
 * @param frames The set of all timeFrames of a run.
 * @param requestInfo The information about the searched target, describing
 * if searching for depndee or depender.
 * @param run The run related to the given timeFrames.
 */
export function getJobFramesOfTaskDependencies(
  frames: JobFramePayload,
  requestInfo: DependencyInfo,
  run: CelerityRun
): JobFramePayload {
  let framesPerNode: JobFramesOfNode = {};
  let targetTask: Task | null = null;
  const searchDependee = requestInfo.dependencyDirection === "Dependee";

  // Determine the search area based on the search target.
  let frameStartTimeIter = searchDependee
    ? Math.ceil(requestInfo.inRange.end / frames.baseFrameLength) *
        frames.baseFrameLength -
      frames.baseFrameLength
    : Math.floor(requestInfo.inRange.start / frames.baseFrameLength) *
      frames.baseFrameLength;
  let frameStartCondition = searchDependee
    ? frameStartTimeIter >= 0
    : frameStartTimeIter < run.endTime - run.startTime;
  // Search for timeFrame containing the search target.
  while (frameStartCondition) {
    if (
      frames.framesPerNode[requestInfo.node][frameStartTimeIter].tasks[
        requestInfo.targetId
      ] !== undefined
    ) {
      targetTask =
        frames.framesPerNode[requestInfo.node][frameStartTimeIter].tasks[
          requestInfo.targetId
        ];
      break;
    }
    if (searchDependee) frameStartTimeIter -= frames.baseFrameLength;
    else frameStartTimeIter += frames.baseFrameLength;
    frameStartCondition = searchDependee
      ? frameStartTimeIter >= 0
      : frameStartTimeIter < run.endTime - run.startTime;
  }

  if (targetTask === null || frameStartTimeIter < 0)
    throw new Error(`Task ${requestInfo.targetId} not found in frames!`);

  let dependencies = searchDependee
    ? [...targetTask.dependees]
    : [...targetTask.dependers];

  // If correct timeFrame is found, search until all dependees/dependers are found.
  while (dependencies.length !== 0) {
    let dependenciesRemain: Array<Dependency<TaskId>> = [];
    let shouldAddFrame = false;
    let isDependencyInit = false;
    for (const nodeId in frames.framesPerNode) {
      const tasksInFrame =
        frames.framesPerNode[nodeId][frameStartTimeIter].tasks;
      const dependeesLength = dependencies.length;
      for (let depIdx = 0; depIdx < dependeesLength; depIdx++) {
        if (dependencies[depIdx].id === 0) {
          isDependencyInit = true;
        } else if (tasksInFrame[dependencies[depIdx].id] !== undefined) {
          shouldAddFrame = true;
        } else {
          dependenciesRemain.push(dependencies[depIdx]);
        }
      }
      if (isDependencyInit) break;
    }
    if (shouldAddFrame) {
      for (const nodeId in frames.framesPerNode) {
        if (framesPerNode[nodeId] === undefined) framesPerNode[nodeId] = {};
        framesPerNode[nodeId][frameStartTimeIter] =
          frames.framesPerNode[nodeId][frameStartTimeIter];
      }
    }

    if (searchDependee) frameStartTimeIter -= frames.baseFrameLength;
    else frameStartTimeIter += frames.baseFrameLength;
    if (
      frameStartTimeIter < 0 ||
      frameStartTimeIter > run.endTime - run.startTime
    ) {
      break;
    }
    dependencies = dependenciesRemain;
  }

  return {
    baseFrameLength: frames.baseFrameLength,
    frameLength: frames.baseFrameLength,
    isSampled: false,
    runId: requestInfo.runId,
    framesPerNode,
  };
}
