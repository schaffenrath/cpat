import React from "react";

import ViewHandler from "./views/ViewHandler";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ViewHandler />
      </header>
    </div>
  );
}

export default App;
