import React, {
  ReactNode,
  useCallback,
  useState,
  useRef,
  useEffect,
} from "react";
import { useDispatch } from "react-redux";
import { setHighlightTimeSpan } from "../redux/reducers/timelinesSlice";

import { TimeSpan, Point } from "../shared/types";
import { TimelineXOffset } from "./Timeline";
import { TimeSpanManager } from "./TimeSpanManager";

export type PixelSpan = TimeSpan;
export type SelectionType = "Zoom" | "Statistic";

interface Props {
  rendererSize: Point;
  timeSpanManager: TimeSpanManager;
  changeSelectionSpan: (selection: PixelSpan) => void;
  mouseHoverPos: (pos: Point) => void;
  changeSelectionType: (selectionType: SelectionType) => void;
  children: ReactNode;
}

/**
 * Handles mouse interactions within the timeline (zoom, shift, time span selection)
 * and forwards view changes to the TimeSpanManager.
 *
 * @param props
 */
export default function TimelineMouseHandler(props: Props) {
  const dispatch = useDispatch();
  const divRef = useRef<HTMLDivElement>(null);

  const [isMouseDown, setMouseDown] = useState<boolean>(false);
  const [mouseDownPosX, setMouseDownPosX] = useState<number>(0);
  const [selectionTimeSpan, setSelectedTimeSpan] = useState<PixelSpan>({
    start: 0,
    end: 0,
  });

  const [zoomStart, setZoomStart] = useState<number>(-1);
  const zoomLevel = useRef<number>(0);

  /**
   * Handle mouse events related to with mouse down, including:
   * view shifting, zooming, area selection for statistics, area
   * selection for zooming.
   */
  const handleDown = useCallback(
    (event: React.MouseEvent<HTMLDivElement, globalThis.MouseEvent>) => {
      if (isMouseDown) return;
      setMouseDown(true);
      const mousePosXWithOffset = event.clientX - TimelineXOffset;
      props.timeSpanManager.createReferenceTimeSpan();
      setMouseDownPosX(mousePosXWithOffset);
      let shouldSetSelection = false;

      if (event.shiftKey) {
        props.changeSelectionType("Zoom");
        shouldSetSelection = true;
      } else if (event.ctrlKey) {
        dispatch(setHighlightTimeSpan(null));
        props.changeSelectionType("Statistic");
        shouldSetSelection = true;
      } else if (event.button === 2) {
        const relativeMousePos =
          (event.clientX - TimelineXOffset) / props.rendererSize.x;
        if (zoomStart === -1) setZoomStart(relativeMousePos);
      }
      if (shouldSetSelection) {
        setSelectedTimeSpan({
          start: mousePosXWithOffset,
          end: mousePosXWithOffset,
        });
      }
    },
    [isMouseDown, props, dispatch, zoomStart]
  );

  /**
   * Handles all mouse events related to mouse up. Mainly
   * important for end of area selection.
   */
  const handleUp = useCallback(
    (event: React.MouseEvent<HTMLDivElement, globalThis.MouseEvent>) => {
      if (!isMouseDown) return;
      setMouseDown(false);
      if (event.shiftKey) {
        if (selectionTimeSpan.start !== selectionTimeSpan.end)
          props.timeSpanManager.setTimeSpanFromMouseSelection(
            selectionTimeSpan
          );
      } else if (event.ctrlKey) {
        if (selectionTimeSpan.start !== selectionTimeSpan.end)
          props.timeSpanManager.setStatisticsSpanFromMouse(selectionTimeSpan);
      }
      //Reset zoom start point on mouse up
      setZoomStart(-1);
      zoomLevel.current = 0;
      setSelectedTimeSpan({ start: 0, end: 0 });
      props.changeSelectionSpan({ start: 0, end: 0 });
    },
    [selectionTimeSpan, props, isMouseDown]
  );

  /**
   * Set new time span based on mouse selection.
   */
  const selectTimeSpan = useCallback(
    (newTimePoint: number) => {
      if (newTimePoint > selectionTimeSpan.start) {
        setSelectedTimeSpan((prevState) => ({
          ...prevState,
          end: newTimePoint,
        }));
      } else {
        setSelectedTimeSpan((prevState) => ({
          ...prevState,
          start: newTimePoint,
        }));
      }
      props.changeSelectionSpan(selectionTimeSpan);
    },
    [selectionTimeSpan, props]
  );

  /**
   * Shift time span based on mouse offset.
   */
  const shiftTimeSpan = useCallback(
    (newMousePosX: number) => {
      const shiftOffset = mouseDownPosX - newMousePosX;
      props.timeSpanManager.shiftTimeSpan(shiftOffset);
    },
    [props, mouseDownPosX]
  );

  const handleMove = useCallback(
    (event: React.MouseEvent<HTMLDivElement, globalThis.MouseEvent>) => {
      props.mouseHoverPos({ x: event.pageX, y: event.pageY });
      if (!isMouseDown) return;
      const mousePosXWithOffset = event.clientX - TimelineXOffset;
      if (event.shiftKey || event.ctrlKey) {
        selectTimeSpan(mousePosXWithOffset);
      } else if (zoomStart !== -1) {
        const relativeMousePos =
          (event.clientX - TimelineXOffset) / props.rendererSize.x;
        zoomLevel.current = (zoomStart - relativeMousePos) * -1;
      } else if (isMouseDown) {
        shiftTimeSpan(mousePosXWithOffset);
      }
    },
    [selectTimeSpan, shiftTimeSpan, isMouseDown, props, zoomStart]
  );

  const handleLeave = useCallback(
    (event: React.MouseEvent<HTMLDivElement, globalThis.MouseEvent>) => {
      handleUp(event);
    },
    [handleUp]
  );

  const zoomIntervalRef = useRef<number | null>(null);
  /**
   * Set continuous zooming while mouse is down.
   */
  useEffect(() => {
    if (zoomStart !== -1 && zoomIntervalRef.current === null) {
      zoomIntervalRef.current = setInterval(() => {
        props.timeSpanManager.zoomTimeSpan(zoomLevel.current, zoomStart);
      }, 10);
    } else if (zoomLevel.current === 0 && zoomIntervalRef.current !== null) {
      zoomLevel.current = 0;
      clearInterval(zoomIntervalRef.current);
      zoomIntervalRef.current = null;
    }
  }, [zoomLevel, props.timeSpanManager, zoomStart]);

  return (
    <div
      ref={divRef}
      onMouseDown={handleDown}
      onMouseUp={handleUp}
      onMouseMove={handleMove}
      onMouseLeave={handleLeave}
      onContextMenu={(event) => event.preventDefault()}
    >
      {props.children}
    </div>
  );
}
