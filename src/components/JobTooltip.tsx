import React, { useCallback, useMemo } from "react";
import { Card, Timeline, Row, Col, Statistic, Divider } from "antd";
import Layout from "antd/lib/layout/layout";

import {
  CommandType,
  Job,
  Command,
  Point,
  ExecutionTarget,
  JobId,
  NodeId,
  JobFramesOfNode,
  SampledFramesOfNode,
  SampledJob,
  Task,
  JobFrameDict,
  AccessMode,
} from "../shared/types";

const TooltipWidth = 300;

export interface HoverInfo {
  jobId: JobId;
  job: Job | SampledJob;
  node: NodeId;
  isSampled: boolean;
}

export interface JobPackage {
  job: Job;
  command: Command;
  task: Task | null;
}

interface Props {
  jobFrames: JobFramesOfNode | null;
  sampledFrames: SampledFramesOfNode | null;
  hoverInfo: HoverInfo | null;
  position: Point;
}

/**
 * Get job, command and target related to the jobId.
 *
 * @param jobId The id of the job for which related data is searched.
 * @param jobFrames The stored frames that contain the searched data.
 */
export function getJobPackage(
  jobId: JobId,
  jobFrames: JobFrameDict
): JobPackage | null {
  for (const frameStartTime in jobFrames) {
    if (jobFrames[frameStartTime] === undefined) {
      return null;
    }
    if (jobFrames[frameStartTime].jobs[jobId] !== undefined) {
      const targetJob = jobFrames[frameStartTime].jobs[jobId];
      const targetCommand =
        jobFrames[frameStartTime].commands[targetJob.commandId];
      let targetTask: Task | null = null;
      if (targetCommand.taskId !== null) {
        targetTask = jobFrames[frameStartTime].tasks[targetCommand.taskId];
      }
      return { job: targetJob, command: targetCommand, task: targetTask };
    }
  }
  return null;
}

/**
 * Shows relevant data of a job by hovering over it in the timeline.
 *
 * @param props
 */
export default function JobTooltip(props: Props) {
  const enhanceListForVis = useCallback(
    (item: any, idx: number, list: Array<any>) => {
      const listLength = list.length;
      if (listLength === 1) return "[" + item.toString() + "]";
      if (idx === 0) return "[" + item.toString() + ", ";
      if (idx === listLength - 1) return item.toString() + "]";
      return item.toString() + ", ";
    },
    []
  );

  const jobPackage = useMemo((): JobPackage | null => {
    if (props.hoverInfo === null || props.jobFrames === null) return null;
    if (props.hoverInfo.isSampled) return null;
    return getJobPackage(
      props.hoverInfo.jobId,
      props.jobFrames[props.hoverInfo.node]
    );
  }, [props.hoverInfo, props.jobFrames]);

  const sampledJob = useMemo((): SampledJob | null => {
    if (props.hoverInfo !== null && props.hoverInfo.isSampled) {
      return props.hoverInfo.job as SampledJob;
    }
    return null;
  }, [props.hoverInfo]);

  const jobInfo = useMemo(() => {
    const items: Array<JSX.Element> = [];
    if (jobPackage === null) return items;
    let itemKey = 0;
    items.push(
      <p key={itemKey++}>
        <b>Command: </b> {jobPackage.job.commandId}
      </p>
    );
    if (jobPackage.command.type === CommandType.TASK) {
      items.push(
        <p key={itemKey++}>
          <b>Task: </b> {jobPackage.command.taskId}
        </p>
      );
      if (jobPackage.command.kernelId !== null)
        items.push(
          <p key={itemKey++}>
            <b>Kernel: </b> {jobPackage.command.kernelId}
          </p>
        );
      items.push(
        <p key={itemKey++}>
          <b>Execution range: </b>
          <br />[{jobPackage.command.executionRange?.min.toString()}] - [
          {jobPackage.command.executionRange?.max.toString()}]
        </p>
      );
    }
    if (
      jobPackage.command.type === CommandType.AWAIT_PUSH ||
      jobPackage.command.type === CommandType.PUSH
    ) {
      items.push(
        <p key={itemKey++}>
          <b>
            {jobPackage.command.type === CommandType.AWAIT_PUSH
              ? "Wait for node: "
              : "Send to node: "}{" "}
          </b>{" "}
          {jobPackage.command.otherNode}
        </p>
      );
      items.push(
        <p key={itemKey++}>
          <b>
            {jobPackage.command.type === CommandType.AWAIT_PUSH
              ? "Wait for buffer: "
              : "Send buffer: "}
          </b>{" "}
          {jobPackage.command.bufferId}
        </p>
      );
    }
    return items;
  }, [jobPackage]);

  const sampledJobInfo = useMemo(() => {
    const items: Array<JSX.Element> = [];
    if (sampledJob === null || sampledJob.jobIdList === undefined) return items;
    let itemKey = 0;
    items.push(
      <p key={itemKey++}>
        <b>Combinded Jobs: </b>
        {sampledJob.jobIdList.map(enhanceListForVis)}
      </p>
    );
    items.push(
      <p key={itemKey++}>
        <b>Kernels: </b>
        {Object.keys(sampledJob.kernels).map(enhanceListForVis)}
      </p>
    );
    return items;
  }, [sampledJob, enhanceListForVis]);

  const pollStatistics = useMemo(() => {
    const items: Array<JSX.Element> = [];
    if (jobPackage === null || jobPackage.job.pollStatistic === null)
      return items;
    let itemKey = 0;
    items.push(
      <Col span={8} key={itemKey++}>
        <Statistic
          title="Min(µs)"
          value={jobPackage.job.pollStatistic.minDuration}
          valueStyle={{ fontSize: "1.5em" }}
        />
      </Col>
    );
    items.push(
      <Col span={8} key={itemKey++}>
        <Statistic
          title="Max(µs)"
          value={jobPackage.job.pollStatistic.maxDuration}
          valueStyle={{ fontSize: "1.5em" }}
        />
      </Col>
    );
    items.push(
      <Col span={8} key={itemKey++}>
        <Statistic
          title="Avg(µs)"
          value={jobPackage.job.pollStatistic.avgDuration}
          valueStyle={{ fontSize: "1.5em" }}
        />
      </Col>
    );
    return items;
  }, [jobPackage]);

  const pollSamples = useMemo(() => {
    if (jobPackage === null || jobPackage.job.pollStatistic === null) return [];
    return (
      <Col span={24}>
        <Statistic
          title="Samples"
          value={jobPackage.job.pollStatistic.sampleCount}
          valueStyle={{ fontSize: "1.5em" }}
        />
      </Col>
    );
  }, [jobPackage]);

  const timeline = useMemo(() => {
    if (jobPackage === null && sampledJob === null) return <div></div>;
    const items: Array<JSX.Element> = [];
    let itemKey = 0;
    let timePointRef = 0;
    items.push(
      <Timeline.Item key={itemKey++} color="blue">
        <p>[0.0µs] Created</p>
      </Timeline.Item>
    );

    if (
      sampledJob !== null &&
      (sampledJob as SampledJob).execSpan !== undefined
    ) {
      items.push(
        <Timeline.Item key={itemKey++} color="red">
          <p>
            [{sampledJob.execSpan.end - sampledJob.execSpan.start}µs] Finished
          </p>
        </Timeline.Item>
      );
    } else if (
      jobPackage !== null &&
      jobPackage.command.type !== CommandType.AWAIT_PUSH &&
      jobPackage.task !== null &&
      jobPackage.task.target !== ExecutionTarget.HOST
    ) {
      timePointRef =
        jobPackage.job.submitTime - jobPackage.job.startTime - timePointRef;
      items.push(
        <Timeline.Item key={itemKey++} color="orange">
          <p>[{timePointRef}µs] Submit</p>
        </Timeline.Item>
      );
      timePointRef =
        jobPackage.job.submittedTime - jobPackage.job.startTime - timePointRef;
      items.push(
        <Timeline.Item key={itemKey++} color="green">
          <p>[{timePointRef}µs] Submitted</p>
        </Timeline.Item>
      );
    }
    if (jobPackage !== null) {
      timePointRef =
        jobPackage.job.finishTime - jobPackage.job.startTime - timePointRef;
      items.push(
        <Timeline.Item key={itemKey++} color="red">
          <p>[{timePointRef}µs] Finished</p>
        </Timeline.Item>
      );
    }
    return <Timeline>{items}</Timeline>;
  }, [jobPackage, sampledJob]);

  const bufferAccesses = useMemo((): JSX.Element => {
    if (jobPackage !== null && jobPackage.command.bufferAccesses.length !== 0) {
      let bufferAccessList: Array<JSX.Element> = [];
      let accessCount = 0;
      for (const bufferAccess of jobPackage.command.bufferAccesses) {
        bufferAccessList.push(
          <div key={accessCount} style={{ display: "flex" }}>
            <p style={{ marginRight: 10, fontSize: 12 }}>
              {AccessMode[bufferAccess.mode]}
            </p>
            <p style={{ fontSize: 12 }}>
              {"[" +
                bufferAccess.range.min.toString() +
                "]-[" +
                bufferAccess.range.max.toString() +
                "]"}
            </p>
          </div>
        );
        accessCount++;
      }
      return (
        <div>
          <h3>
            <b>Buffer Accesses</b>
          </h3>
          {bufferAccessList}
        </div>
      );
    }
    return <div></div>;
  }, [jobPackage]);

  const displayPosition = useMemo(() => {
    const tooltipOffset = TooltipWidth / 2;
    let newPosition: Point = props.position;
    if (props.position.x - tooltipOffset < 0) {
      newPosition.x = tooltipOffset;
    } else if (props.position.x + tooltipOffset > window.innerWidth) {
      newPosition.x = window.innerWidth - tooltipOffset;
    }
    return newPosition;
  }, [props.position]);

  const cardTitel = useMemo(() => {
    if (props.hoverInfo === null) return ``;
    if (props.jobFrames !== null) {
      return `Job ${props.hoverInfo.jobId}`;
    } else {
      return `Sampled Job ${props.hoverInfo?.jobId},...`;
    }
  }, [props.hoverInfo, props.jobFrames]);

  if (
    props.hoverInfo === null ||
    (jobPackage === null && sampledJob === null)
  ) {
    return <div></div>;
  }

  return (
    <Card
      title={cardTitel}
      style={{
        position: "absolute",
        width: 300,
        left: displayPosition.x,
        top: displayPosition.y + 10,
        transform:
          displayPosition.y + 10 > window.innerHeight / 2
            ? "translate(-50%, -103%)"
            : "translate(-50%, 0%)",
        zIndex: 1,
        borderColor: "darkgray",
        borderWidth: 2,
      }}
    >
      <Layout style={{ backgroundColor: "transparent" }}>
        <Row justify="center" align="top">
          <Col span={14}>
            {jobInfo !== null && <div>{jobInfo}</div>}
            {sampledJobInfo !== null && <div>{sampledJobInfo}</div>}
          </Col>
          <Col span={10}>{timeline}</Col>
        </Row>
        <Row>{bufferAccesses}</Row>
      </Layout>
      <Divider style={{ marginTop: 0, marginBottom: 0 }} />
      {jobPackage !== null && (
        <Layout style={{ backgroundColor: "transparent" }}>
          <h3>
            <b>Poll Statistics</b>
          </h3>
          <div style={{ display: "flex", textAlign: "center" }}>
            {pollStatistics}
          </div>
          <div style={{ textAlign: "center", marginTop: "5px" }}>
            {pollSamples}
          </div>
        </Layout>
      )}
    </Card>
  );
}
