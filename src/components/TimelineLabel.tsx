import React, { useCallback } from "react";
import styled from "styled-components";

import { Point, TimeSpan } from "../shared/types";

const TickContainer = styled.div`
  position: absolute;
`;

const Tick = styled.div.attrs<{ position: number }>((props) => ({
  style: {
    marginLeft: props.position,
  },
}))<{ position: number }>`
  position: absolute;
  width: 1px;
  height: 8px;
  background: #000;
`;

const TickTime = styled.div.attrs<{ position: number }>((props) => ({
  style: {
    marginLeft: props.position,
  },
}))<{ position: number }>`
  position: absolute;
  margin-top: 5px;
  transform: translateX(-50%);
`;

const TickGuideLine = styled.div.attrs<{ position: number; height: number }>(
  (props) => ({
    style: {
      marginLeft: props.position - 0.5,
      height: props.height,
    },
  })
)<{ position: number; height: number }>`
  position: absolute;
  width: 4px;
  border: none;
  border-left: 2px dashed #000000;
  color: #000000;
  z-index: -1;
  transform: translateY(-105%);
`;

interface TickData {
  tickCount: number;
  firstTickTime: number;
  tickOffsetTime: number;
  firstTickPos: number;
  tickOffsetPos: number;
}

interface Props {
  timelineSize: Point;
  displayedTimeSpan: TimeSpan;
  startTime: number;
  finishTime: number;
  visualizerHeight: number;
  drawNumbers: boolean;
}

export default function TimelineLabel(props: Props) {
  const tickProperties = useCallback(() => {
    const timeSpan = {
      start: props.displayedTimeSpan.start / 1000,
      end: props.displayedTimeSpan.end / 1000,
    };
    // Calculate time distance of ticks for given time range.
    const timeRange = Math.abs(timeSpan.end - timeSpan.start);
    const targetTicks = Math.round(props.timelineSize.x / 100);
    const initStepCount = timeRange / targetTicks;
    const normalizedSteps: Array<number> = [1, 2, 5, 10];
    const stepPower = Math.pow(
      10,
      -Math.floor(Math.log10(Math.abs(initStepCount)))
    );
    const normalizedStep = initStepCount * stepPower;
    let goodNormalizedStep: number = 0;
    const normalizedStepsSize = normalizedSteps.length;
    for (let i = 0; i < normalizedStepsSize; i++) {
      if (normalizedSteps[i] >= normalizedStep) {
        goodNormalizedStep = normalizedSteps[i];
        break;
      }
    }
    const step = goodNormalizedStep / stepPower;
    const timePixelRatio =
      props.timelineSize.x / (timeSpan.end - timeSpan.start);
    const firstTickTime = Math.ceil(timeSpan.start / step) * step;
    const firstTickPos = (firstTickTime - timeSpan.start) * timePixelRatio;
    const tickCount = Math.ceil(timeRange / step);
    const tickPosOffset = timePixelRatio * step;
    return {
      tickCount: tickCount,
      firstTickPos: firstTickPos,
      tickOffsetPos: tickPosOffset,
      firstTickTime: firstTickTime,
      tickOffsetTime: step,
    };
  }, [props.displayedTimeSpan, props.timelineSize]);

  const tickPrecision = useCallback(() => {
    const threshold = 0.000001;
    let stepValue = tickProperties().tickOffsetTime;
    let decimalCount = 0;
    while (Math.abs(stepValue - Math.round(stepValue)) > threshold) {
      decimalCount++;
      stepValue *= 10;
    }
    return decimalCount;
  }, [tickProperties]);

  if (props.displayedTimeSpan.end === 0 || props.finishTime === 0)
    return <div></div>;

  return (
    <TickContainer>
      {Array(tickProperties().tickCount)
        .fill(0)
        .map((_, i) => (
          <div key={i}>
            <TickGuideLine
              id="guidLine"
              position={
                i * tickProperties().tickOffsetPos +
                tickProperties().firstTickPos
              }
              height={props.visualizerHeight}
            />
            {props.drawNumbers && (
              <div>
                <Tick
                  position={
                    i * tickProperties().tickOffsetPos +
                    tickProperties().firstTickPos
                  }
                />
                <TickTime
                  position={
                    i * tickProperties().tickOffsetPos +
                    tickProperties().firstTickPos
                  }
                >
                  {(
                    i * tickProperties().tickOffsetTime +
                    tickProperties().firstTickTime
                  ).toFixed(tickPrecision()) + "ms"}
                </TickTime>
              </div>
            )}
          </div>
        ))}
    </TickContainer>
  );
}
