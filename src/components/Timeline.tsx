import React, {
  useEffect,
  useRef,
  useState,
  useMemo,
  useCallback,
} from "react";
import { Button, Tooltip, Modal, Input, Collapse, Select, Tag } from "antd";
import { CloseOutlined, DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { useSelector, useDispatch } from "react-redux";

import JobRenderer from "./JobRenderer";
import TimeSpanSelector from "./TimeSpanSelector";
import TimelineLabel from "./TimelineLabel";
import { useDebouncer } from "../util/hooks";
import JobTooltip, { HoverInfo } from "./JobTooltip";
import { RootState } from "../redux/store";
import {
  setLinkedTimeSpan,
  TimeSpanOfTimeline,
  setHighlightTimeSpan,
  showDependencyGraph,
} from "../redux/reducers/timelinesSlice";
import {
  getSampleLevel,
  setRunName,
} from "../redux/reducers/celerityDataSlice";
import {
  CelerityRun,
  TimeSpan,
  Point,
  RunId,
  Job,
  Command,
  Task,
  JobFrame,
  SampledJob,
  JobFramesOfNode,
  SampledFramesOfNode,
  JobFrameDict,
  SampledFrameDict,
  DependencyInfo,
  JobType,
} from "../shared/types";
import TimelineMouseHandler, {
  PixelSpan,
  SelectionType,
} from "./TimelineMouseHandler";
import { TimeSpanManager } from "./TimeSpanManager";
import { TimelineId } from "./TimelineContainer";
import config from "../shared/config";
import JobModal, { HighlightInfo } from "./JobModal";
import { getHEXColorOfType } from "../colorCoding";
import DependencyGraph from "./DependencyGraph";
import Statistics from "./Statistics";

const { Panel } = Collapse;
const { Option } = Select;

interface Props {
  timelineId: TimelineId;
  initialRun: RunId;
  requestTimeFrames: (
    runId: RunId,
    viewSpan: TimeSpan,
    sampleLevel?: number
  ) => void;
  requestSpecificFrames: (requestInfo: DependencyInfo) => void;
  removeTimeline: (timelineId: string) => void;
  requestRunDeletion: (runId: RunId, timelineId: TimelineId) => void;
  requestStatistics: (runId: RunId, timeSpan: TimeSpan) => void;
}

// VisualizerWidthFactor defines the proportional size of the visualizer compared to the entire view width
const VisualizerWidthFactor = 0.88;
const VisualizerHeight = 200;

export const TimelineXOffset = 80;

/**
 * Creates a visualization of all jobs within a run.
 *
 * @param props
 */
export default function Timeline(props: Props) {
  const dispatch = useDispatch();

  const [rendererSize, setRendererSize] = useState<Point>({
    x: window.innerWidth * VisualizerWidthFactor,
    y: VisualizerHeight,
  });

  const [displayedTimeSpan, setDisplayedTimeSpan] = useState<TimeSpan>({
    start: 0,
    end: 0,
  });
  const timeSpanManagerRef = useRef<TimeSpanManager | null>(null);

  const changeTimeSpanOnEvent = useCallback((newTimeSpan: TimeSpan) => {
    setDisplayedTimeSpan(newTimeSpan);
  }, []);

  /**
   * Init timeSpanManager.
   */
  useEffect(() => {
    if (timeSpanManagerRef.current === null) {
      timeSpanManagerRef.current = new TimeSpanManager(rendererSize.x);
      timeSpanManagerRef.current.addListener(
        "DisplayedTimeSpanChanged",
        changeTimeSpanOnEvent
      );
      timeSpanManagerRef.current.addListener(
        "RequestStatisticsInTimeSpan",
        (runId, timeSpan) => {
          props.requestStatistics(runId, timeSpan);
          dispatch(setHighlightTimeSpan({ nodeId: -1, timeSpan: timeSpan }));
        }
      );
    }
  }, [rendererSize.x, changeTimeSpanOnEvent, props, dispatch]);

  const [displayedRun, setDisplayedRun] = useState<CelerityRun>();

  const savedRuns = useSelector((state: RootState) => state.celerityRun.runs);
  /**
   * Set initial run.
   */
  useEffect(() => {
    if (props.initialRun !== "" && displayedRun === undefined) {
      const runForTimeline = savedRuns[props.initialRun];
      if (runForTimeline !== undefined) {
        setDisplayedRun(runForTimeline);
        if (timeSpanManagerRef.current !== null) {
          timeSpanManagerRef.current.setTimeSpanFromRun(runForTimeline);
        }
      }
    }
  }, [props.initialRun, savedRuns, displayedRun]);

  const handleResize = useCallback(() => {
    setRendererSize({
      x: window.innerWidth * VisualizerWidthFactor,
      y: VisualizerHeight,
    });
  }, []);

  useEffect(() => {
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [handleResize]);

  const rendererRef = useRef<HTMLDivElement>(null);
  const [hoverInfo, setHoverInfo] = useState<HoverInfo | null>(null);
  const timeFrames = useSelector(
    (state: RootState) => state.celerityRun.timeFrames
  );
  const [mouseSelectionSpan, setMouseSelectionSpan] = useState<PixelSpan>({
    start: 0,
    end: 0,
  });
  const [selectionType, setSelectionType] = useState<SelectionType>("Zoom");
  const isViewLinked = useSelector(
    (state: RootState) => state.timeline.linkedViews
  );
  const linkedTimeSpanAndInitiater = useSelector(
    (state: RootState) => state.timeline.linkedTimeSpan
  );
  const linkedChangeDebouncer = useDebouncer(200);
  // If displayed time span changes and view is linked, store time span with redux.
  useEffect(() => {
    if (
      isViewLinked &&
      timeSpanManagerRef.current !== null &&
      timeSpanManagerRef.current.wasChangedByUser()
    ) {
      timeSpanManagerRef.current.resetChangedByUser();
      const newTimelineAndTimeSpan: TimeSpanOfTimeline = {
        timelineId: props.timelineId,
        timeSpan: displayedTimeSpan,
      };
      if (timeSpanManagerRef.current !== null)
        linkedChangeDebouncer(() => {
          dispatch(setLinkedTimeSpan(newTimelineAndTimeSpan));
        });
    }
  }, [
    dispatch,
    isViewLinked,
    props.timelineId,
    linkedTimeSpanAndInitiater,
    displayedTimeSpan,
    linkedChangeDebouncer,
  ]);

  // If time span is changed in redux and change is not cause from this timeline,
  // change time span to stored value.
  useEffect(() => {
    if (
      isViewLinked &&
      props.timelineId !== linkedTimeSpanAndInitiater.timelineId &&
      timeSpanManagerRef.current !== null
    ) {
      timeSpanManagerRef.current.setTimeSpan(
        linkedTimeSpanAndInitiater.timeSpan
      );
    }
  }, [props.timelineId, linkedTimeSpanAndInitiater, isViewLinked]);

  const handleRemoveTimeline = useCallback(() => {
    props.removeTimeline(props.timelineId);
  }, [props]);

  const handleDeleteRun = useCallback(() => {
    props.requestRunDeletion(props.initialRun, props.timelineId);
  }, [props]);

  const [isModalVisible, setModalVisible] = useState<boolean>(false);
  const editedRunName = useRef<string>("");
  const handleAcceptRename = useCallback(() => {
    dispatch(
      setRunName({
        id: props.initialRun,
        name: editedRunName.current,
      })
    );
    setModalVisible(false);
  }, [dispatch, props.initialRun]);

  const handleCancelRename = useCallback(() => {
    editedRunName.current = "";
    setModalVisible(false);
  }, []);

  const baseFrameLengths = useSelector(
    (state: RootState) => state.celerityRun.baseFrameLength
  );
  const baseFrameLength = useMemo(() => {
    if (baseFrameLengths === undefined || displayedRun === undefined)
      return undefined;
    return baseFrameLengths[displayedRun.id];
  }, [baseFrameLengths, displayedRun]);

  const [jobFrames, setJobFrames] = useState<JobFramesOfNode | undefined>(
    undefined
  );
  const [sampledFrames, setSampledFrames] = useState<
    SampledFramesOfNode | undefined
  >(undefined);
  const expectingFramesWithSamplingLevel = useRef<TimeSpan>({
    start: 0,
    end: 0,
  });

  /**
   * Get all frame start times within the displayedTimeSpan.
   */
  const getFrameStartTimesInTimeSpan = useCallback(
    (sampleLevel: number) => {
      let missingFrameStartTimes: Array<number> = [];
      let timeFrameStart =
        Math.floor(displayedTimeSpan.start / sampleLevel) * sampleLevel;
      while (timeFrameStart < displayedTimeSpan.end) {
        missingFrameStartTimes.push(timeFrameStart);
        timeFrameStart += sampleLevel;
      }
      return missingFrameStartTimes;
    },
    [displayedTimeSpan]
  );

  /**
   * Check if timeFrames or sampledFrames are missing withing the
   * displayedTimeSpan and and return a list of missing frame start
   * times.
   */
  const getMissingFrames = useCallback(
    (sampleLevel: number) => {
      if (
        displayedRun !== undefined &&
        timeFrames[displayedRun.id] !== undefined
      ) {
        let sampleLevelMissing = false;
        let missingFrameStartTimes: Array<number> = [];
        let timeFrameStart =
          Math.floor(displayedTimeSpan.start / sampleLevel) * sampleLevel;
        const baseNode = 0;
        let framesOfBaseNode: JobFrameDict | SampledFrameDict | undefined =
          undefined;
        // Determine if looking for timeFrames or sampledFrames.
        if (
          sampleLevel === baseFrameLengths[displayedRun.id] ||
          config.DISABLE_SAMPLING
        ) {
          framesOfBaseNode = timeFrames[displayedRun.id].jobFrames[baseNode];
        } else {
          const targetSampleLevel =
            timeFrames[displayedRun.id].sampleLevels[sampleLevel];
          if (targetSampleLevel === undefined) sampleLevelMissing = true;
          else framesOfBaseNode = targetSampleLevel[baseNode];
        }

        if (
          sampleLevelMissing ||
          framesOfBaseNode === undefined ||
          Object.keys(framesOfBaseNode).length === 0
        ) {
          return getFrameStartTimesInTimeSpan(sampleLevel);
        }
        let checkedAllFrames = false;
        // Check if all frames within the given time span are stored.
        while (!checkedAllFrames) {
          if (framesOfBaseNode[timeFrameStart] === undefined) {
            missingFrameStartTimes.push(timeFrameStart);
          }
          timeFrameStart += sampleLevel;
          let timeFrameEnd: number = displayedTimeSpan.end;
          if (
            displayedTimeSpan.end >
            displayedRun.endTime - displayedRun.startTime
          ) {
            timeFrameEnd = displayedRun.endTime - displayedRun.startTime;
          }
          if (timeFrameStart > timeFrameEnd) {
            checkedAllFrames = true;
          }
        }
        return missingFrameStartTimes;
      }
      return getFrameStartTimesInTimeSpan(sampleLevel);
    },
    [
      baseFrameLengths,
      displayedRun,
      timeFrames,
      getFrameStartTimesInTimeSpan,
      displayedTimeSpan,
    ]
  );

  const areTimeFramesAvailable = useCallback(
    (sampleLevel: number) => {
      if (getMissingFrames(sampleLevel).length === 0) {
        return true;
      }
      return false;
    },
    [getMissingFrames]
  );

  const frameRequestDebouncer = useDebouncer(200);
  const requestTimeFramesIfNotAvailable = useCallback(
    (viewSpan: TimeSpan, sampleLevel: number) => {
      if (displayedRun === undefined) {
        return;
      }
      const entireRunSpan: TimeSpan = {
        start: 0,
        end: displayedRun.endTime - displayedRun.startTime,
      };
      if (
        !areTimeFramesAvailable(sampleLevel) &&
        expectingFramesWithSamplingLevel.current !== entireRunSpan
      ) {
        if (getMissingFrames(sampleLevel).length === 1) {
          const missingFrameStart = getMissingFrames(sampleLevel)[0];
          const spanOfMissingFrame: TimeSpan = {
            start: missingFrameStart,
            end: missingFrameStart + sampleLevel,
          };

          frameRequestDebouncer(() => {
            props.requestTimeFrames(
              displayedRun.id,
              spanOfMissingFrame,
              sampleLevel
            );
          });
        } else {
          let requestSpan: TimeSpan = Object.assign({}, viewSpan);
          if (requestSpan.end > entireRunSpan.end)
            requestSpan.end = entireRunSpan.end;
          frameRequestDebouncer(() => {
            props.requestTimeFrames(displayedRun.id, requestSpan);
          });
          expectingFramesWithSamplingLevel.current = requestSpan;
        }
      }
    },
    [
      displayedRun,
      props,
      areTimeFramesAvailable,
      getMissingFrames,
      frameRequestDebouncer,
    ]
  );

  /**
   * Check if timeFrames or sampledFrames are available within
   * displayedTimeSpan, otherwise request them.
   */
  useEffect(() => {
    if (
      displayedRun !== undefined &&
      timeFrames[displayedRun.id] !== undefined &&
      baseFrameLength !== undefined
    ) {
      const sampleLevel = getSampleLevel(displayedTimeSpan, baseFrameLength);
      if (areTimeFramesAvailable(sampleLevel)) {
        expectingFramesWithSamplingLevel.current = { start: 0, end: 0 };
      } else {
        requestTimeFramesIfNotAvailable(displayedTimeSpan, sampleLevel);
        return;
      }

      if (sampleLevel === baseFrameLengths[displayedRun.id]) {
        setJobFrames(timeFrames[displayedRun.id].jobFrames);
        setSampledFrames(undefined);
      } else {
        setJobFrames(undefined);
        setSampledFrames(timeFrames[displayedRun.id].sampleLevels[sampleLevel]);
        setHighlightedJobs(null);
      }
    }
  }, [
    displayedRun,
    displayedTimeSpan,
    timeFrames,
    baseFrameLengths,
    timeSpanManagerRef,
    requestTimeFramesIfNotAvailable,
    areTimeFramesAvailable,
    baseFrameLength,
  ]);

  const [selectJobInfo, setSelectJobInfo] = useState<HoverInfo | null>(null);
  // If job is selected in timeline, check if its available, otherwise request
  // timeFrame containing it.
  const handleJobSelect = useCallback(
    (selectedJob: HoverInfo | null) => {
      if (
        selectedJob !== null &&
        displayedRun !== undefined &&
        baseFrameLength !== undefined
      ) {
        if (selectedJob.isSampled) {
          const jobStart =
            (selectedJob.job as SampledJob).execSpan.start -
            displayedRun.startTime;
          const jobEnd =
            (selectedJob.job as SampledJob).execSpan.end -
            displayedRun.startTime;
          const requestFrameCount = Math.ceil(
            (jobEnd - jobStart) / baseFrameLength
          );
          let targetFrameStart =
            Math.floor(jobStart / baseFrameLength) * baseFrameLength;
          let allFramesAvailable = true;
          for (
            let frameStart = targetFrameStart, idx = 0;
            idx < requestFrameCount;
            frameStart += baseFrameLength, idx++
          ) {
            if (
              timeFrames[displayedRun.id].jobFrames[frameStart] === undefined
            ) {
              allFramesAvailable = false;
              break;
            }
          }
          if (!allFramesAvailable) {
            props.requestTimeFrames(
              displayedRun.id,
              { start: jobStart, end: jobEnd },
              baseFrameLength
            );
          }
        } else {
          const jobStart =
            (selectedJob.job as Job).startTime - displayedRun.startTime;
          const jobEnd =
            (selectedJob.job as Job).finishTime - displayedRun.startTime;
          let targetFrameStart = 0;
          while (targetFrameStart + baseFrameLength < baseFrameLength)
            targetFrameStart += baseFrameLength;
          if (
            timeFrames[displayedRun.id].jobFrames[targetFrameStart] ===
            undefined
          ) {
            props.requestTimeFrames(
              displayedRun.id,
              { start: jobStart, end: jobEnd + 1 },
              baseFrameLength
            );
          }
        }
      }
      setSelectJobInfo(selectedJob);
    },
    [baseFrameLength, displayedRun, props, timeFrames]
  );

  const [highlightedJobs, setHighlightedJobs] = useState<HighlightInfo | null>(
    null
  );
  const [expectingJobInSpan, setExpectingJobInSpan] =
    useState<HighlightInfo | null>(null);

  /**
   * Highlights job and zooms in by double click on it and then clicking
   * the Job Id. If sampled, zooming in can require timeFrames that need
   * to be request.
   */
  const handleJobHighlight = useCallback(
    (highlightInfo: HighlightInfo | null) => {
      if (
        highlightInfo === null ||
        jobFrames === null ||
        displayedRun === undefined ||
        baseFrameLength === undefined
      ) {
        setHighlightedJobs(null);
        return;
      }

      // The timeFrame containing the target job is guaranteed to
      // exist, since it is required to display the table in JobModal.
      if (highlightInfo.requestType === "Job") {
        setExpectingJobInSpan(highlightInfo);
        return;
      }

      let frameStartTime =
        Math.floor(highlightInfo.inRange.start / baseFrameLength) *
        baseFrameLength;
      const allJobFrames = timeFrames[displayedRun.id].jobFrames;

      let requestSpecificFrames = false;
      if (
        allJobFrames === undefined ||
        allJobFrames[highlightInfo.node] === undefined
      ) {
        requestSpecificFrames = true;
      } else {
        const framesOfNode = allJobFrames[highlightInfo.node];
        let targetCmd: Command | Task | undefined = undefined;
        const depType: keyof JobFrame =
          highlightInfo.requestType === "Command" ? "commands" : "tasks";
        while (frameStartTime < highlightInfo.inRange.end) {
          if (
            framesOfNode[frameStartTime] !== undefined &&
            framesOfNode[frameStartTime][depType][highlightInfo.jobId] !==
              undefined
          ) {
            targetCmd =
              framesOfNode[frameStartTime][depType][highlightInfo.jobId];
            break;
          }
          frameStartTime += baseFrameLength;
        }

        if (targetCmd !== undefined) {
          for (const dependee of targetCmd.dependees) {
            let frameStartIter = frameStartTime;
            while (frameStartIter >= 0 && !requestSpecificFrames) {
              if (framesOfNode[frameStartIter] === undefined) {
                requestSpecificFrames = true;
                break;
              }
              if (
                framesOfNode[frameStartIter][depType][dependee.id] === undefined
              ) {
                frameStartIter -= baseFrameLength;
              } else {
                break;
              }
            }
            if (requestSpecificFrames) break;
          }
        } else {
          requestSpecificFrames = true;
        }
      }
      if (requestSpecificFrames) {
        props.requestSpecificFrames({
          inRange: highlightInfo.inRange,
          targetId: highlightInfo.jobId,
          node: highlightInfo.node,
          requestType: highlightInfo.requestType,
          dependencyDirection: "Dependee",
          runId: displayedRun.id,
        });
      }
    },
    [jobFrames, baseFrameLength, displayedRun, timeFrames, props]
  );

  /**
   * Check if requested frames from {@link handleJobHighlight} are available yet.
   */
  useEffect(() => {
    if (
      expectingJobInSpan !== null &&
      baseFrameLength !== undefined &&
      displayedRun !== undefined
    ) {
      let frameStartTime =
        Math.floor(expectingJobInSpan.inRange.start / baseFrameLength) *
        baseFrameLength;
      let foundFrameWithJob = false;
      // Check if all frames in expected time span are available.
      while (frameStartTime < expectingJobInSpan.inRange.end) {
        const framesOfNode =
          timeFrames[displayedRun.id].jobFrames[expectingJobInSpan.node];
        if (framesOfNode !== undefined) {
          if (framesOfNode[frameStartTime] !== undefined) {
            const targetJob =
              framesOfNode[frameStartTime].jobs[expectingJobInSpan.jobId];
            if (targetJob !== undefined) {
              foundFrameWithJob = true;
              const jobExecTime = targetJob.finishTime - targetJob.startTime;
              const viewMargin = jobExecTime * 0.05;
              let viewStart = targetJob.startTime - displayedRun.startTime;
              viewStart =
                viewStart - viewMargin < 0 ? 0 : viewStart - viewMargin;
              let viewEnd =
                targetJob.finishTime - displayedRun.startTime + viewMargin;
              timeSpanManagerRef.current?.setTimeSpan({
                start: viewStart,
                end: viewEnd,
              });
              setHighlightedJobs(expectingJobInSpan);
              break;
            }
          }
        }
        frameStartTime += baseFrameLength;
      }
      if (!foundFrameWithJob) return;
    }
  }, [timeFrames, expectingJobInSpan, baseFrameLength, displayedRun]);

  const [visibleTypes, setVisibleTypes] = useState<Array<JobType>>([]);
  const [highlightedKernel, setHighlightedKernel] = useState<string>("");

  const cmdVisualizationPerNode: JSX.Element = useMemo(() => {
    const frames = jobFrames !== undefined ? jobFrames : sampledFrames;
    if (frames === undefined || displayedRun === undefined) {
      return <div></div>;
    }

    let result: Array<JSX.Element> = [];
    const nodeAmount = Object.keys(displayedRun.jobVisLines).length;
    for (let nodeId = nodeAmount - 1; nodeId >= 0; nodeId--) {
      const jobVisLinesOfNode = displayedRun.jobVisLines[nodeId];

      if (
        jobVisLinesOfNode === undefined ||
        timeSpanManagerRef.current === null
      ) {
        return <div></div>;
      }

      result.push(
        <Panel
          key={nodeId}
          style={{
            display: "flex",
            marginBottom: 10,
            paddingTop: 6,
            paddingBottom: 0,
          }}
          header={`Node ${nodeId}`}
        >
          <div ref={rendererRef}>
            <JobRenderer
              viewSize={rendererSize}
              node={nodeId}
              run={displayedRun}
              jobFrames={jobFrames === undefined ? null : jobFrames[nodeId]}
              sampledFrames={
                sampledFrames === undefined ? null : sampledFrames[nodeId]
              }
              jobVisLines={jobVisLinesOfNode}
              displayedTimeSpan={displayedTimeSpan}
              highlightJob={highlightedJobs}
              hiddenTypes={visibleTypes}
              highlightedKernel={highlightedKernel}
              onJobHover={setHoverInfo}
              onJobSelect={handleJobSelect}
            />
            <TimeSpanSelector
              viewSize={{
                x: rendererSize.x,
                y: jobVisLinesOfNode * 12,
              }}
              selectionTimeSpan={mouseSelectionSpan}
              selectionType={selectionType}
            />
            <TimelineLabel
              timelineSize={rendererSize}
              startTime={displayedRun.startTime}
              finishTime={displayedRun.endTime}
              displayedTimeSpan={displayedTimeSpan}
              visualizerHeight={jobVisLinesOfNode * 12}
              drawNumbers={true}
            />
          </div>
          {nodeId === 0 && (
            <div>
              <Statistics />
              <DependencyGraph
                requestIfNotAvailable={props.requestSpecificFrames}
              />
            </div>
          )}
        </Panel>
      );
    }

    return (
      <Collapse defaultActiveKey={["0"]} ghost style={{ paddingBottom: 0 }}>
        {result}
      </Collapse>
    );
  }, [
    displayedRun,
    rendererSize,
    handleJobSelect,
    mouseSelectionSpan,
    displayedTimeSpan,
    jobFrames,
    sampledFrames,
    highlightedJobs,
    visibleTypes,
    highlightedKernel,
    selectionType,
    props.requestSpecificFrames,
  ]);

  const handleFilterJobType = useCallback((selected: Array<JobType>) => {
    setVisibleTypes([...selected]);
  }, []);

  const typeFilter = useMemo((): JSX.Element => {
    let optionList: Array<JSX.Element> = [];
    const typeList: Array<JobType> = [
      "deviceTask",
      "hostTask",
      "awaitPush",
      "push",
      "horizon",
    ];
    let typeCount = 0;
    for (const jobType of typeList) {
      optionList.push(
        <Option key={typeCount} value={jobType} label={jobType}>
          <Tag color={getHEXColorOfType(jobType)}>{jobType}</Tag>
        </Option>
      );
      typeCount += 1;
    }
    return (
      <Select
        style={{ width: 250 }}
        mode="multiple"
        placeholder="Show Selected Types"
        onChange={handleFilterJobType}
      >
        {optionList}
      </Select>
    );
  }, [handleFilterJobType]);

  const handleHighlightKernel = useCallback((kernel: string) => {
    if (kernel === undefined) setHighlightedKernel("");
    else setHighlightedKernel(kernel);
  }, []);

  const kernelFilter = useMemo((): JSX.Element => {
    if (displayedRun === undefined) return <div></div>;
    let optionList: Array<JSX.Element> = [];
    let kernelCount = 0;
    for (const kernel of displayedRun.kernels) {
      optionList.push(
        <Option key={kernelCount} value={kernel} label={kernel}>
          {kernel}
        </Option>
      );
      kernelCount += 1;
    }
    return (
      <Select
        style={{ width: 200 }}
        placeholder="Highlight Kernel"
        onChange={handleHighlightKernel}
        allowClear
      >
        {optionList}
      </Select>
    );
  }, [displayedRun, handleHighlightKernel]);

  const runNames = useSelector(
    (state: RootState) => state.celerityRun.availableRuns
  );
  const displayedRunName = useMemo(() => {
    const targetName = runNames[props.initialRun];
    if (targetName === undefined) return "Undefined";
    return targetName;
  }, [runNames, props.initialRun]);

  const highlightedTimeSpan = useSelector(
    (state: RootState) => state.timeline.highlightTimeSpan
  );

  const dependencyGraphInfo = useSelector(
    (state: RootState) => state.timeline.showDependencyGraph
  );
  const timelineButtons = useMemo(() => {
    return (
      <div style={{ display: "flex", marginTop: 40 }}>
        <Button
          type="primary"
          onClick={() => {
            timeSpanManagerRef.current?.resetZoom();
          }}
          style={{ position: "relative" }}
        >
          Reset Zoom
        </Button>
        {highlightedJobs !== null && (
          <Button
            danger
            type="default"
            onClick={() => setHighlightedJobs(null)}
            style={{ position: "relative", marginLeft: 10 }}
          >
            Reset Highlighting
          </Button>
        )}
        {highlightedTimeSpan !== null && (
          <Button
            danger
            type="default"
            onClick={() => dispatch(setHighlightTimeSpan(null))}
            style={{ position: "relative", marginLeft: 10 }}
          >
            Close Statistics
          </Button>
        )}
        {dependencyGraphInfo !== null && (
          <Button
            danger
            type="default"
            onClick={() => dispatch(showDependencyGraph(null))}
            style={{ position: "relative", marginLeft: 10 }}
          >
            Close Dependency Graph
          </Button>
        )}
      </div>
    );
  }, [highlightedJobs, highlightedTimeSpan, dependencyGraphInfo, dispatch]);

  const runEditButtons = useMemo(
    () => (
      <div style={{ width: "100%" }}>
        <div style={{ display: "flex" }}>
          <Tooltip title="Rename run">
            <Button
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
              onClick={() => setModalVisible(true)}
            />
          </Tooltip>
          <Tooltip title="Remove Timeline">
            <Button
              danger
              type="primary"
              shape="circle"
              icon={<CloseOutlined />}
              onClick={handleRemoveTimeline}
            />
          </Tooltip>
          <Tooltip title="Delete Run">
            <Button
              danger
              type="primary"
              shape="circle"
              icon={<DeleteOutlined />}
              onClick={handleDeleteRun}
            />
          </Tooltip>
        </div>
        <div
          style={{
            right: 0,
            position: "absolute",
            display: "flex",
          }}
        >
          <div
            style={{
              border: "1px solid",
              width: 200,
              height: 32,
            }}
          >
            <p
              style={{
                verticalAlign: "middle",
                textAlign: "center",
                marginTop: 4,
                fontSize: 10,
              }}
            >
              Currently only works with disabled sampling!
            </p>
          </div>
          <div>{typeFilter}</div>
          <div>{kernelFilter}</div>
        </div>
      </div>
    ),
    [handleDeleteRun, handleRemoveTimeline, typeFilter, kernelFilter]
  );

  const editRunNameModal = useMemo(
    () => (
      <Modal
        title="Rename Run"
        visible={isModalVisible}
        onOk={handleAcceptRename}
        onCancel={handleCancelRename}
      >
        <Input
          placeholder="New run name"
          onChange={(event) => (editedRunName.current = event.target.value)}
          onPressEnter={handleAcceptRename}
        />
      </Modal>
    ),
    [handleAcceptRename, handleCancelRename, isModalVisible]
  );

  const [mousePos, setMousePos] = useState<Point>({ x: 0, y: 0 });

  if (
    (jobFrames === undefined && sampledFrames === undefined) ||
    displayedRun === undefined ||
    timeSpanManagerRef.current === null
  ) {
    return null;
  }

  return (
    <TimelineMouseHandler
      rendererSize={rendererSize}
      timeSpanManager={timeSpanManagerRef.current}
      changeSelectionSpan={setMouseSelectionSpan}
      changeSelectionType={setSelectionType}
      mouseHoverPos={setMousePos}
    >
      <div style={{ display: "flex" }}>
        <h3>Run: {displayedRunName}</h3>
        {runEditButtons}
      </div>
      {editRunNameModal}
      {cmdVisualizationPerNode}
      {timelineButtons}
      <JobTooltip
        jobFrames={jobFrames === undefined ? null : jobFrames}
        sampledFrames={sampledFrames === undefined ? null : sampledFrames}
        hoverInfo={hoverInfo}
        position={mousePos}
      />
      <JobModal
        run={displayedRun}
        selectJobInfo={selectJobInfo}
        jobFrames={
          displayedRun === undefined ||
          timeFrames[displayedRun.id] === undefined
            ? null
            : timeFrames[displayedRun.id].jobFrames
        }
        onHighlightJob={handleJobHighlight}
      />
    </TimelineMouseHandler>
  );
}
