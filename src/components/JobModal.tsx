import React, { useCallback, useMemo } from "react";
import { Button, Modal, Table, Tag } from "antd";
import { ColumnsType } from "antd/lib/table";
import { useDispatch, useSelector } from "react-redux";

import {
  CommandId,
  CommandType,
  ExecutionTarget,
  JobFramesOfNode,
  Job,
  JobId,
  JobType,
  SampledJob,
  TimeSpan,
  NodeId,
  DependencyType,
  TaskId,
  CelerityRun,
} from "../shared/types";
import { RootState } from "../redux/store";
import { getJobPackage, HoverInfo, JobPackage } from "./JobTooltip";
import {
  setJobModalVisibility,
  showDependencyGraph,
} from "../redux/reducers/timelinesSlice";
import { getHEXColorOfType } from "../colorCoding";

export interface HighlightInfo {
  jobId: JobId;
  inRange: TimeSpan;
  requestType: DependencyType;
  node: NodeId;
}

interface TableJob {
  key: number;
  type: JobType;
  jobId: number;
  commandId: CommandId;
  taskId: TaskId | null;
  kernel: string | null;
  execTime: number;
  pollSamples: number | undefined;
  pollAvg: number | undefined;
  pollMin: number | undefined;
  pollMax: number | undefined;
}

interface Props {
  run: CelerityRun;
  jobFrames: JobFramesOfNode | null;
  selectJobInfo: HoverInfo | null;
  onHighlightJob: (highlightJob: HighlightInfo) => void;
}

/**
 * Popup window that shows more details about a job and
 * allows to show the dependency graphs.
 *
 * @param props
 */
export default function JobModal(props: Props) {
  const modalVisibility = useSelector(
    (state: RootState) => state.timeline.showJobModal
  );

  const dispatch = useDispatch();

  const handleHighlightJob = useCallback(
    (jobId: JobId, highlightType: DependencyType) => {
      if (props.selectJobInfo === null) return;
      let highlightInfo: HighlightInfo = {
        jobId,
        inRange: { start: 0, end: 0 },
        requestType: highlightType,
        node: props.selectJobInfo.node,
      };
      if (props.selectJobInfo.isSampled) {
        const sampledExec = (props.selectJobInfo.job as SampledJob).execSpan;
        highlightInfo.inRange = {
          start: sampledExec.start - props.run.startTime,
          end: sampledExec.end - props.run.startTime,
        };
      } else {
        highlightInfo.inRange = {
          start:
            (props.selectJobInfo.job as Job).startTime - props.run.startTime,
          end:
            (props.selectJobInfo.job as Job).finishTime - props.run.startTime,
        };
      }
      dispatch(setJobModalVisibility(false));
      if (
        highlightInfo.requestType === "Command" ||
        highlightInfo.requestType === "Task"
      ) {
        const dependencyType: DependencyType =
          highlightInfo.requestType === "Command" ? "Command" : "Task";
        dispatch(
          showDependencyGraph({
            runId: props.run.id,
            dependencyType,
            targetId: highlightInfo.jobId,
            inRange: highlightInfo.inRange,
            node: highlightInfo.node,
          })
        );
      }
      props.onHighlightJob(highlightInfo);
    },
    [props, dispatch]
  );

  const columns = useMemo((): ColumnsType<TableJob> => {
    return [
      {
        title: "Job Id",
        dataIndex: "jobId",
        key: "jobId",
        render: (jobId: JobId) => {
          if (props.selectJobInfo === null)
            return <Button size={"small"}>Undefined</Button>;
          return (
            <Button
              size={"small"}
              onClick={() => handleHighlightJob(jobId, "Job")}
            >
              {jobId}
            </Button>
          );
        },
      },
      {
        title: "Type",
        dataIndex: "type",
        key: "type",
        render: (type: JobType) => {
          const color = getHEXColorOfType(type);
          return <Tag color={color}>{type}</Tag>;
        },
      },
      {
        title: "Kernel",
        dataIndex: "kernel",
        key: "kernel",
      },
      {
        title: "Exec Time (μs)",
        dataIndex: "execTime",
        key: "execTime",
        sorter: (a, b) => a.execTime - b.execTime,
        sortDirections: ["descend", "ascend"],
      },
      {
        title: "Poll Samples",
        dataIndex: "pollSamples",
        key: "pollSamples",
        sorter: (a, b) => {
          if (a.pollSamples !== undefined && b.pollSamples !== undefined) {
            return a.pollSamples - b.pollSamples;
          }
          return 1;
        },
      },
      {
        title: "Poll Max (μs)",
        dataIndex: "pollMax",
        key: "pollMax",
        sorter: (a, b) => {
          if (a.pollMax !== undefined && b.pollMax !== undefined) {
            return a.pollMax - b.pollMax;
          }
          return 1;
        },
      },
      {
        title: "Poll Avg (μs)",
        dataIndex: "pollAvg",
        key: "pollAvg",
        sorter: (a, b) => {
          if (a.pollAvg !== undefined && b.pollAvg !== undefined) {
            return a.pollAvg - b.pollAvg;
          }
          return 1;
        },
      },
      {
        title: "Poll Min (μs)",
        dataIndex: "pollMin",
        key: "pollMin",
        sorter: (a, b) => {
          if (a.pollMin !== undefined && b.pollMin !== undefined) {
            return a.pollMin - b.pollMin;
          }
          return 1;
        },
        align: "center",
      },
      {
        title: "Command Dependencies",
        dataIndex: "commandId",
        key: "commandDeps",
        render: (command: CommandId) => {
          return (
            <Button
              type="default"
              size={"small"}
              onClick={() => handleHighlightJob(command, "Command")}
            >
              Show
            </Button>
          );
        },
      },
      {
        title: "Task",
        dataIndex: "taskId",
        key: "taskId",
        render: (taskId: TaskId | null) => {
          if (taskId === null) return <div></div>;
          else
            return (
              <Button
                type="primary"
                size={"small"}
                onClick={() => handleHighlightJob(taskId, "Task")}
              >
                Show
              </Button>
            );
        },
      },
    ];
  }, [props, handleHighlightJob]);

  const parseJobToTableInfo = useCallback(
    (jobId: JobId, jobPackage: JobPackage): Array<TableJob> => {
      // Get related command and info to add to table info
      let jobType: JobType = "hostTask";
      switch (jobPackage.command.type) {
        case CommandType.TASK:
          jobType =
            jobPackage.task?.target === ExecutionTarget.DEVICE
              ? "deviceTask"
              : "hostTask";
          break;
        case CommandType.AWAIT_PUSH:
          jobType = "awaitPush";
          break;
        case CommandType.PUSH:
          jobType = "push";
          break;
        case CommandType.HORIZON:
          jobType = "horizon";
          break;
      }
      return [
        {
          key: jobId,
          jobId,
          type: jobType,
          commandId: jobPackage.job.commandId,
          taskId: jobPackage.command.taskId,
          kernel:
            jobPackage.command.kernelId === ""
              ? null
              : jobPackage.command.kernelId,
          execTime: jobPackage.job.finishTime - jobPackage.job.startTime,
          pollSamples: jobPackage.job.pollStatistic?.sampleCount,
          pollMax: jobPackage.job.pollStatistic?.maxDuration,
          pollAvg: jobPackage.job.pollStatistic?.avgDuration,
          pollMin: jobPackage.job.pollStatistic?.minDuration,
        },
      ];
    },
    []
  );

  const tableData = useMemo(() => {
    if (
      modalVisibility &&
      props.selectJobInfo !== null &&
      props.jobFrames !== null &&
      props.jobFrames[props.selectJobInfo.node] !== undefined
    ) {
      if (props.selectJobInfo.isSampled) {
        let newTableData: Array<TableJob> = [];
        for (const jobIdInSampled of (props.selectJobInfo.job as SampledJob)
          .jobIdList) {
          const sampleJobInfo = getJobPackage(
            jobIdInSampled,
            props.jobFrames[props.selectJobInfo.node]
          );
          if (sampleJobInfo === null) return null;
          newTableData = newTableData.concat(
            parseJobToTableInfo(jobIdInSampled, sampleJobInfo)
          );
        }
        return newTableData;
      }
      const jobInfo = getJobPackage(
        props.selectJobInfo.jobId,
        props.jobFrames[props.selectJobInfo.node]
      );
      if (jobInfo === null) return null;
      return parseJobToTableInfo(props.selectJobInfo.jobId, jobInfo);
    }
  }, [modalVisibility, props, parseJobToTableInfo]);

  const hideModal = useCallback(() => {
    dispatch(setJobModalVisibility(false));
  }, [dispatch]);

  if (tableData === null) return <div />;

  return (
    <Modal
      title={`Job List`}
      centered
      visible={modalVisibility}
      onCancel={hideModal}
      footer={[
        <Button key="close" onClick={hideModal}>
          Close
        </Button>,
      ]}
      width={window.innerWidth - 20}
    >
      <Table columns={columns} dataSource={tableData} />
    </Modal>
  );
}
