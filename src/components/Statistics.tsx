import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { Row, Col, Select, Divider, Table, Button } from "antd";

import {
  RunId,
  NodeId,
  CmdValuePerType,
  JobType,
  KernelExecTimeOfTarget,
} from "../shared/types";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/store";
import { getHEXColorOfType } from "../colorCoding";
import { ResponsiveBar } from "@nivo/bar";
import { ResponsiveSunburst } from "@nivo/sunburst";
import { setHighlightTimeSpan } from "../redux/reducers/timelinesSlice";

const { Option } = Select;

export interface StatisticRequest {
  runId: RunId;
  node: NodeId;
}

interface SunburstEntry {
  name: string;
  time?: number;
  children?: Array<SunburstEntry>;
}

/**
 * Component that displays statistics when user selected area with mouse.
 * Statistics include: Execution ratio, execution time per command type,
 * job count, average execution time per type and standard deviation.
 */
export default function Statistics() {
  const [, forceRerender] = useState<Object>({});
  useEffect(() => {
    window.addEventListener("resize", forceRerender);
    return () => {
      window.removeEventListener("resize", forceRerender);
    };
  }, []);

  const highlightTimeSpan = useSelector(
    (state: RootState) => state.timeline.highlightTimeSpan
  );

  const savedRuns = useSelector((state: RootState) => state.celerityRun.runs);

  const rawStatistics = useSelector(
    (state: RootState) => state.celerityRun.statistics
  );

  const visible = useMemo(() => {
    return highlightTimeSpan !== null;
  }, [highlightTimeSpan]);

  type NodeType = "Node" | "All";
  const [selectedNode, setSelectedNode] = useState<number>(0);
  const getKernelTimeData = useCallback(
    (nodeType: NodeType, jobType: JobType): [Array<SunburstEntry>, number] => {
      if (
        rawStatistics === null ||
        savedRuns[rawStatistics.runId] === undefined
      )
        return [[], 0];
      const kernelTimes: Array<SunburstEntry> = [];

      let kernelExecTimes: KernelExecTimeOfTarget;
      if (nodeType === "Node")
        kernelExecTimes = rawStatistics.nodeKernelExecTime[selectedNode];
      else kernelExecTimes = rawStatistics.totalKernelExecTime;

      let kernelExecSum = 0;
      const kernelTarget: keyof KernelExecTimeOfTarget =
        jobType === "hostTask" ? "host" : "device";
      for (const kernelId in kernelExecTimes[kernelTarget]) {
        let newSunburstEntry: SunburstEntry = {
          // Workaround: Add empty space for hostTasks, since name duplicates lead to error!
          name: kernelTarget === "host" ? kernelId + " " : kernelId,
          time: kernelExecTimes[kernelTarget][kernelId],
        };
        kernelTimes.push(newSunburstEntry);
        kernelExecSum += kernelExecTimes[kernelTarget][kernelId];
      }
      return [kernelTimes, kernelExecSum];
    },
    [rawStatistics, savedRuns, selectedNode]
  );

  const tickUnitInSecRef = useRef<boolean>(false);
  const getSunburstData = useCallback(
    (nodeType: NodeType): SunburstEntry => {
      if (rawStatistics === null) return { name: "Error" };
      tickUnitInSecRef.current = false;
      const sunburstEntries: Array<SunburstEntry> = [];
      let execTimes: CmdValuePerType;
      if (nodeType === "Node")
        execTimes = rawStatistics.nodeExecTime[selectedNode];
      else execTimes = rawStatistics.totalExecTime;

      let kernelTimes: [Array<SunburstEntry>, number] | null = null;
      for (const jobType in execTimes) {
        if (jobType === "deviceTask" || jobType === "hostTask")
          kernelTimes = getKernelTimeData(nodeType, jobType);
        else kernelTimes = null;
        if (execTimes[jobType as JobType] > 1e6)
          tickUnitInSecRef.current = true;
        let newSunburstEntry: SunburstEntry = {
          name: jobType,
          // Since sunburst adds up all children, we have to subtract them.
          time:
            kernelTimes === null
              ? execTimes[jobType as JobType]
              : execTimes[jobType as JobType] - kernelTimes[1],
          children: kernelTimes === null ? undefined : kernelTimes[0],
        };
        sunburstEntries.push(newSunburstEntry);
      }
      return {
        name: "Execution Times",
        children: sunburstEntries,
      };
    },
    [rawStatistics, getKernelTimeData, selectedNode]
  );

  interface BarData {
    data: Array<Object>;
    nodeList: Array<string>;
  }

  const getBarData = useCallback((): BarData => {
    if (rawStatistics === null) return { data: [], nodeList: [] };
    const execTimesPerType: { [key: string]: Object } = {};
    let nodeList: Array<string> = [];
    const dummyObj: CmdValuePerType = {
      awaitPush: 0,
      deviceTask: 0,
      horizon: 0,
      hostTask: 0,
      push: 0,
    };
    for (const jobType in dummyObj) {
      execTimesPerType[jobType] = { jobType };
    }
    for (const nodeId in rawStatistics.nodeExecTime) {
      const nodeIdx: string = "Node " + nodeId.toString();
      nodeList.push(nodeIdx);
      for (const jobType in rawStatistics.nodeExecTime[nodeId]) {
        execTimesPerType[jobType] = {
          ...execTimesPerType[jobType],
          [nodeIdx]:
            rawStatistics.nodeExecTime[nodeId][
              jobType as keyof CmdValuePerType
            ],
        };
      }
    }

    let barEntryList: Array<Object> = [];
    for (const objIdx in execTimesPerType) {
      barEntryList.push(execTimesPerType[objIdx]);
    }
    return { data: barEntryList, nodeList };
  }, [rawStatistics]);

  const getTimeLabel = useCallback((time: number | string) => {
    if (time === 0) return "";
    if (typeof time === "string") return time;
    const timeUnits = ["μs", "ms", "sec"];
    let unitIdx = 0;
    let adaptedTime = time;
    while (adaptedTime >= 1000 && unitIdx < 2) {
      adaptedTime /= 1000;
      unitIdx++;
    }
    return adaptedTime.toFixed(2) + " " + timeUnits[unitIdx];
  }, []);

  const getTicks = useCallback((time: number) => {
    if (time === 0) return "";
    let convertedTime = time / 1000;
    if (tickUnitInSecRef.current) convertedTime /= 1000;
    return convertedTime.toFixed(1);
  }, []);

  const getTickUnit = useCallback(() => {
    if (tickUnitInSecRef.current) return "sec";
    return "ms";
  }, []);

  const nodeOptions = useMemo(() => {
    if (!visible || rawStatistics === null) return [];
    let nodeOptionList: Array<JSX.Element> = [];
    for (let nodeId = 0; nodeId < rawStatistics.nodeExecTime.length; nodeId++) {
      nodeOptionList.push(
        <Option key={nodeId} value={nodeId}>
          Node {nodeId}
        </Option>
      );
    }
    return nodeOptionList;
  }, [rawStatistics, visible]);

  const tableColumns = useMemo(
    () => [
      { tite: "Job Type", dataIndex: "jobType", key: "jobType" },
      { title: "Job Count", dataIndex: "jobCount", key: "jobCount" },
      { title: "Avg [μs]", dataIndex: "avg", key: "avg" },
      { title: "STD", dataIndex: "std", key: "std" },
    ],
    []
  );

  interface TableEntry {
    key: JobType;
    jobType: JobType;
    jobCount: number;
    avg: string;
    std: string;
  }

  const tableData = useMemo((): Array<TableEntry> => {
    if (rawStatistics === null) return [];
    let newTableData: Array<TableEntry> = [];
    for (const jobType in rawStatistics.totalAvgExec) {
      const realJobType = jobType as JobType;
      let newTableEntry: TableEntry = {
        key: realJobType,
        jobType: realJobType,
        jobCount: rawStatistics.totalJobCount[realJobType],
        avg: rawStatistics.totalAvgExec[realJobType].toFixed(),
        std: rawStatistics.cmdDeviation[realJobType].toFixed(),
      };
      newTableData.push(newTableEntry);
    }
    return newTableData;
  }, [rawStatistics]);

  const dispatch = useDispatch();

  if (!visible || rawStatistics === null) {
    return <div></div>;
  }
  return (
    <div style={{ width: "100%", position: "inherit" }}>
      <Divider style={{ marginBottom: 5 }} />
      <Row>
        <Col span={6}>
          <div style={{ textAlign: "center" }}>
            <h3>Execution Ratio</h3>
          </div>
        </Col>
        <Col span={10}>
          <div style={{ textAlign: "center" }}>
            <h3>Execution Time</h3>
          </div>
        </Col>
        <Col span={8}>
          <div style={{ textAlign: "center" }}>
            <h3>Additional Info</h3>
          </div>
        </Col>
      </Row>
      <Divider style={{ marginTop: 5, marginBottom: 10 }} />
      <Row>
        <Col span={3}>
          <Row>
            <Select
              defaultValue="Node 0"
              style={{ width: 120 }}
              onChange={(nodeId) => setSelectedNode(Number(nodeId))}
            >
              {nodeOptions}
            </Select>
          </Row>
          <Row>
            <div style={{ height: 150, width: "100%" }}>
              <ResponsiveSunburst
                data={getSunburstData("Node")}
                id="name"
                value="time"
                colors={({ id }) => getHEXColorOfType(id as JobType)}
                childColor={{ from: "color", modifiers: [["darker", 0.3]] }}
              />
            </div>
          </Row>
        </Col>
        <Col span={3}>
          <Row>
            <Select
              defaultValue="All Nodes"
              style={{ width: 120 }}
              disabled={true}
            ></Select>
          </Row>
          <Row>
            <div style={{ height: 150, width: "100%" }}>
              <ResponsiveSunburst
                data={getSunburstData("All")}
                id="name"
                value="time"
                colors={({ id }) => getHEXColorOfType(id as JobType)}
                childColor={{ from: "color", modifiers: [["darker", 0.3]] }}
              />
            </div>
          </Row>
        </Col>
        <Col span={10}>
          <div style={{ height: 300, width: "90%" }}>
            <ResponsiveBar
              data={getBarData().data}
              keys={getBarData().nodeList}
              indexBy="jobType"
              margin={{ top: 5, left: 100, bottom: 50 }}
              padding={0.2}
              colors={{ scheme: "set2" }}
              layout="horizontal"
              axisLeft={{
                tickSize: 5,
                tickPadding: 0,
                tickRotation: 0,
                legend: "Job Types",
                legendPosition: "middle",
                legendOffset: -70,
              }}
              axisBottom={{
                tickSize: 5,
                tickPadding: 0,
                tickRotation: 0,
                legend: "Exec Time [" + getTickUnit() + "]",
                legendPosition: "middle",
                legendOffset: 30,
                format: (val) => getTicks(val as number),
              }}
              enableLabel={false}
              tooltipFormat={(val) => getTimeLabel(val)}
              enableGridX={true}
              enableGridY={false}
              legends={[
                {
                  dataFrom: "keys",
                  anchor: "top-left",
                  direction: "row",
                  justify: false,
                  translateX: 0,
                  translateY: -5,
                  itemsSpacing: 50,
                  symbolSpacing: 2,
                  itemWidth: 10,
                  itemHeight: 10,
                  itemDirection: "left-to-right",
                  itemOpacity: 0.85,
                  symbolSize: 10,
                  effects: [
                    {
                      on: "hover",
                      style: {
                        itemOpacity: 1,
                      },
                    },
                  ],
                },
              ]}
              animate={false}
            />
          </div>
        </Col>
        <Col span={8}>
          <Table
            columns={tableColumns}
            dataSource={tableData}
            pagination={false}
          />
        </Col>
        <Divider type="vertical" />
      </Row>
      <Row>
        <Button
          danger
          style={{ left: "95%", transform: "translateX(-100%)" }}
          onClick={() => dispatch(setHighlightTimeSpan(null))}
        >
          Close Statistics
        </Button>
      </Row>
      <Divider style={{ marginTop: 1 }} />
    </div>
  );
}
