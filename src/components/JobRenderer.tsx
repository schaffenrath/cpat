import React, {
  useState,
  useLayoutEffect,
  useRef,
  useEffect,
  useCallback,
} from "react";
import * as THREE from "three";
import { Vector3 } from "three";
import { useDispatch, useSelector } from "react-redux";

import {
  getRGBColorOfType,
  getKernelColor,
  HIGHLIGHT_PATTERN_COUNT,
  HIGHLIGHT_RGB_COLOR,
  getSelectionColor,
} from "../colorCoding";
import { setJobModalVisibility } from "../redux/reducers/timelinesSlice";

import { vertexShader, fragmentShader } from "../Shaders";
import {
  Job,
  JobId,
  Command,
  TaskId,
  Task,
  ExecutionTarget,
  CommandType,
  KernelId,
  Point,
  TimeSpan,
  CelerityRun,
  JobFrameDict,
  SampledJob,
  SampledFrameDict,
  CmdValuePerType,
  ExecTimeOfKernel,
  CommandId,
  JobType,
} from "../shared/types";
import { useThrottler } from "../util/hooks";
import { HoverInfo } from "./JobTooltip";
import { HighlightInfo } from "./JobModal";
import { RootState } from "../redux/store";

type TypeKeys = keyof CmdValuePerType;
type DrawType = "Job" | "JobHighlight" | "TimeSpanHighlight";

/**
 * Draws the all jobs in the timeline.
 */
class Renderer {
  private zoom = 1;
  private renderReady = false;
  private renderer: THREE.WebGLRenderer;
  private planes!: THREE.InstancedMesh;
  private highlightPlane!: THREE.InstancedMesh;
  private highlightSpanMesh!: THREE.Mesh;
  private scene: THREE.Scene;
  private camera: THREE.OrthographicCamera;
  private raycaster: THREE.Raycaster;
  private jobGeometry: THREE.PlaneBufferGeometry;
  private highlightGeometry: THREE.PlaneBufferGeometry;
  private baseShape: THREE.Shape;
  private jobMaterial: THREE.ShaderMaterial;
  private jobDrawSize = { x: 1, y: 10 };
  private viewportSize: Point;
  private numJobs: number = 0;

  constructor(initSize: Point) {
    this.viewportSize = initSize;
    this.renderer = new THREE.WebGLRenderer({
      antialias: true,
      alpha: true,
      premultipliedAlpha: false,
    });
    this.renderer.setClearColor(0xeeeeee, 0.5);
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(this.viewportSize.x, this.viewportSize.y);
    this.scene = new THREE.Scene();
    this.camera = new THREE.OrthographicCamera(
      this.viewportSize.x / -2,
      this.viewportSize.x / 2,
      this.viewportSize.y / 2,
      this.viewportSize.y / -2,
      1,
      10
    );
    this.camera.position.z = 1;
    this.camera.zoom = this.zoom;
    this.camera.updateProjectionMatrix();
    this.raycaster = new THREE.Raycaster();
    this.jobGeometry = new THREE.PlaneBufferGeometry(
      this.jobDrawSize.x,
      this.jobDrawSize.y,
      1,
      1
    );
    this.highlightGeometry = new THREE.PlaneBufferGeometry(
      this.jobDrawSize.x,
      this.jobDrawSize.y,
      1,
      1
    );
    this.baseShape = new THREE.Shape()
      .moveTo(0, 0)
      .lineTo(this.jobDrawSize.x, 0)
      .lineTo(this.jobDrawSize.x, this.jobDrawSize.y)
      .lineTo(0, this.jobDrawSize.y)
      .lineTo(0, 0);
    this.jobMaterial = new THREE.ShaderMaterial({
      vertexShader: vertexShader,
      fragmentShader,
      alphaTest: 0.9,
    });
  }

  /**
   * Reset parts of visualization to get rid of highlighting.
   *
   * @param itemCount Number of jobs drawn.
   * @param drawType The information that should be reset.
   */
  resetPlanes(itemCount: number, drawType: DrawType) {
    if (drawType === "JobHighlight") {
      this.scene.remove(this.highlightPlane);
      this.highlightPlane = new THREE.InstancedMesh(
        this.highlightGeometry,
        this.jobMaterial,
        itemCount
      );
      this.scene.add(this.highlightPlane);
    } else if (drawType === "Job") {
      this.scene.remove(this.planes);
      this.planes = new THREE.InstancedMesh(
        this.jobGeometry,
        this.jobMaterial,
        itemCount
      );
      this.scene.add(this.planes);
    } else if (drawType === "TimeSpanHighlight") {
      this.scene.remove(this.highlightSpanMesh);
    }
  }

  getJobDrawColor(
    commandOfJob: Command,
    run: CelerityRun,
    taskMap: Map<TaskId, Task> | undefined
  ): number | undefined {
    switch (commandOfJob.type) {
      // In task case find the related task to find the execution target (important for visualization)
      case CommandType.TASK:
        if (commandOfJob.taskId === null) break;
        const taskOfCommand = taskMap?.get(commandOfJob.taskId);
        if (taskOfCommand === undefined) break;
        if (commandOfJob.kernelId === null) {
          return taskOfCommand.target === ExecutionTarget.DEVICE ? 0 : 1;
        }
        let newRGBColor = getKernelColor(commandOfJob.kernelId, run.kernels);
        const combinedColor =
          newRGBColor.x + newRGBColor.y * 256 + newRGBColor.z * 256 ** 2;
        return combinedColor;
      case CommandType.PUSH:
        return 2;
      case CommandType.AWAIT_PUSH:
        return 3;
      case CommandType.HORIZON:
        return 143 + 100 * 256 + 234 * 256 ** 2;
      default:
        return commandOfJob.type;
    }
  }

  getKernelColorByRatio(
    sampledKernels: ExecTimeOfKernel,
    allKernels: Array<KernelId>
  ) {
    if (Object.keys(sampledKernels).length === 0) return new Vector3(0);
    let sortedKernels: Array<[KernelId, number]> = [];
    let totalKernelExecTime = 0;
    for (const kernelId in sampledKernels) {
      totalKernelExecTime += sampledKernels[kernelId];
    }
    for (const kernelId in sampledKernels) {
      sortedKernels.push([
        kernelId,
        sampledKernels[kernelId] / totalKernelExecTime,
      ]);
    }
    sortedKernels.sort((a, b) => a[1] - b[1]);

    let mixedKernelRGBColors = new Vector3(0);
    for (const [kernelId, kernelExecRatio] of sortedKernels) {
      if (kernelExecRatio !== 0) {
        const kernelRGBColor = getKernelColor(kernelId, allKernels);
        mixedKernelRGBColors.x += kernelExecRatio * kernelRGBColor.x;
        mixedKernelRGBColors.y += kernelExecRatio * kernelRGBColor.y;
        mixedKernelRGBColors.z += kernelExecRatio * kernelRGBColor.z;
      }
    }
    return mixedKernelRGBColors;
  }

  getColorByTypeRatio(
    typeRatios: CmdValuePerType,
    sampledKernels: ExecTimeOfKernel,
    allKernels: Array<KernelId>
  ) {
    const mixedKernelRGBColor = this.getKernelColorByRatio(
      sampledKernels,
      allKernels
    );
    let sortedTypes: Array<[TypeKeys, number]> = [];
    let totalTypeExecTime = 0;
    for (const jobType in typeRatios) {
      totalTypeExecTime += typeRatios[jobType as keyof CmdValuePerType];
    }
    for (const jobType in typeRatios) {
      sortedTypes.push([
        jobType as TypeKeys,
        typeRatios[jobType as TypeKeys] / totalTypeExecTime,
      ]);
    }
    sortedTypes.sort((a, b) => a[1] - b[1]);

    let mixedTypeRGBColor: THREE.Vector3 = new Vector3(0);

    for (const [jobType, execRatio] of sortedTypes) {
      const typeColor =
        jobType === "deviceTask"
          ? mixedKernelRGBColor
          : getRGBColorOfType(jobType);
      mixedTypeRGBColor.x += execRatio * typeColor.x;
      mixedTypeRGBColor.y += execRatio * typeColor.y;
      mixedTypeRGBColor.z += execRatio * typeColor.z;
    }

    const finalColor = mixedTypeRGBColor;
    return (
      Math.round(finalColor.x) +
      Math.round(finalColor.y) * 256 +
      Math.round(finalColor.z) * 256 ** 2
    );
  }

  setFrames(
    jobList: Array<[JobId, Job | SampledJob]>,
    run: CelerityRun,
    areSampled: boolean,
    commandMap?: Map<CommandId, Command> | undefined,
    taskMap?: Map<TaskId, Task> | undefined
  ) {
    this.resetPlanes(jobList.length, "Job");
    this.numJobs = jobList.length;

    const drawObj = new THREE.Object3D();
    const colorBuffer = new Int32Array(jobList.length);
    let jobCounter = 0;
    jobList.forEach(([jobId, job]) => {
      let drawXPos = 0;
      if (areSampled) {
        drawObj.scale.set(
          (job as SampledJob).execSpan.end - (job as SampledJob).execSpan.start,
          1,
          1
        );
        drawXPos =
          (job as SampledJob).execSpan.start -
          run.startTime +
          drawObj.scale.x / 2;
      } else {
        drawObj.scale.set(
          (job as Job).finishTime - (job as Job).startTime,
          1,
          1
        );
        drawXPos = (job as Job).startTime - run.startTime + drawObj.scale.x / 2;
      }

      drawObj.position.set(
        drawXPos,
        job.lineInVis * (this.jobDrawSize.y * 1.2) + this.jobDrawSize.y / 2,
        0
      );

      if (areSampled) {
        const jobColor = this.getColorByTypeRatio(
          (job as SampledJob).typeRatio,
          (job as SampledJob).kernels,
          run.kernels
        );
        colorBuffer[jobCounter] = jobColor;
      } else {
        if (commandMap === undefined || taskMap === undefined)
          throw new Error(`Job frames are undefinded!`);
        const commandOfJob = commandMap.get((job as Job).commandId);
        if (commandOfJob === undefined)
          throw new Error(
            `Command ${
              (job as Job).commandId
            } corresponding to job ${jobId} was not found`
          );

        const jobColor = this.getJobDrawColor(commandOfJob, run, taskMap);
        if (jobColor === undefined) return;
        colorBuffer[jobCounter] = jobColor;
      }

      drawObj.updateMatrix();
      this.planes.setMatrixAt(jobCounter, drawObj.matrix);
      jobCounter += 1;
    });

    this.jobGeometry.setAttribute(
      "customColor",
      new THREE.InstancedBufferAttribute(colorBuffer, 1)
    );

    const opacityArray = new Array(this.numJobs).fill(1);
    const opacityBuffer = new Float32Array(opacityArray as any);
    this.jobGeometry.setAttribute(
      "opacity",
      new THREE.InstancedBufferAttribute(opacityBuffer, 1)
    );

    // Initialize raycaster to avoid false intersections
    this.raycaster.setFromCamera({ x: -1, y: -1 }, this.camera);
    if (this.renderReady) {
      this.scheduleRerender();
    }
  }

  setHighlighted = (jobIndices: Array<number>, hide?: boolean) => {
    if (this.jobGeometry === null) return;

    const opacity = hide === true ? 0 : 0.2;
    const opacityArray = new Array(this.numJobs).fill(
      jobIndices.length > 0 ? opacity : 1
    );
    for (const idx of jobIndices) {
      opacityArray[idx] = 1;
    }
    const opacityBuffer = new Float32Array(
      opacityArray as any[] /* NOCOMMIT ?? */
    );
    this.jobGeometry.setAttribute(
      "opacity",
      new THREE.InstancedBufferAttribute(opacityBuffer, 1)
    );
    if (this.renderReady) {
      this.scheduleRerender();
    }
  };

  highlightJobs = (
    highlightJob: HighlightInfo,
    highlightJobIdx: number | null,
    jobList: Array<[JobId, Job | SampledJob]>,
    run: CelerityRun
  ) => {
    if (highlightJobIdx === null || jobList[highlightJobIdx] === undefined)
      return;

    if (highlightJob.requestType === "Job") {
      const targetJob = jobList[highlightJobIdx][1] as Job;
      const jobExec = targetJob.finishTime - targetJob.startTime;
      const unitTime = jobExec / (HIGHLIGHT_PATTERN_COUNT * 2);
      this.resetPlanes(HIGHLIGHT_PATTERN_COUNT, "JobHighlight");
      const drawObj = new THREE.Object3D();
      const colorBuffer = new Int32Array(HIGHLIGHT_PATTERN_COUNT);
      for (let idx = 0; idx < HIGHLIGHT_PATTERN_COUNT; idx++) {
        drawObj.scale.set(unitTime, 1, 1);
        let drawXPos =
          targetJob.startTime -
          run.startTime +
          2 * idx * unitTime +
          drawObj.scale.x / 2;
        drawObj.position.set(
          drawXPos,
          targetJob.lineInVis * (this.jobDrawSize.y * 1.2) +
            this.jobDrawSize.y / 2,
          0
        );
        colorBuffer[idx] =
          Math.round(HIGHLIGHT_RGB_COLOR.x) +
          Math.round(HIGHLIGHT_RGB_COLOR.y) * 256 +
          Math.round(HIGHLIGHT_RGB_COLOR.z) * 256 ** 2;
        drawObj.updateMatrix();
        this.highlightPlane.setMatrixAt(idx, drawObj.matrix);
      }
      this.highlightGeometry.setAttribute(
        "customColor",
        new THREE.InstancedBufferAttribute(colorBuffer, 1)
      );
      if (this.renderReady) {
        this.scheduleRerender();
      }
    }
  };

  highlightTimeSpan = (timeSpan: TimeSpan, visLines: number) => {
    if (timeSpan.end !== 0) {
      let highlightSpanGeometry = new THREE.ShapeGeometry(this.baseShape);
      highlightSpanGeometry.scale(
        timeSpan.end - timeSpan.start,
        visLines * 12,
        1
      );
      this.highlightSpanMesh = new THREE.Mesh(
        highlightSpanGeometry,
        new THREE.MeshBasicMaterial({
          color: new THREE.Color(getSelectionColor("Statistic")),
          opacity: 0.2,
          transparent: true,
        })
      );
      this.highlightSpanMesh.position.set(timeSpan.start, 0, 0);
      this.scene.add(this.highlightSpanMesh);
      this.scheduleRerender();
    } else {
      this.resetHighlightSpan();
    }
  };

  resetHighlight = () => {
    this.resetPlanes(0, "JobHighlight");
    this.scheduleRerender();
  };

  resetHighlightSpan = () => {
    this.resetPlanes(0, "TimeSpanHighlight");
  };

  startRendering = (container: HTMLDivElement) => {
    container.appendChild(this.renderer.domElement);
    this.renderReady = true;
    this.scheduleRerender();
  };

  setRenderAreaSize = (newSize: Point) => {
    this.renderer.setSize(newSize.x, newSize.y);
    this.scheduleRerender();
  };

  setRenderTimeSpan = (timeSpan: TimeSpan) => {
    const viewAreaSize = timeSpan.end - timeSpan.start;
    this.camera.left = -viewAreaSize / 2;
    this.camera.right = viewAreaSize / 2;
    this.camera.position.x = timeSpan.start + viewAreaSize / 2;
    const viewportSize = new THREE.Vector2();
    this.renderer.getSize(viewportSize);
    this.camera.position.y = viewportSize.y / 2;
    this.camera.updateProjectionMatrix();
    this.scheduleRerender();
  };
  /**
   * Returns the drawing index of the job under the curser
   * @param pos Position of the mouse
   */
  getJobIdxAtPosition = (pos: Point): number | null => {
    const ndcPos = {
      x: (pos.x / this.viewportSize.x) * 2 - 1,
      y: -(pos.y / this.viewportSize.y) * 2 + 1,
    };
    this.raycaster.setFromCamera(ndcPos, this.camera);
    const intersections = this.raycaster.intersectObject(this.planes);
    // Since the visualization is 2D and not overlapping,
    // the intersected job must be the first element
    if (intersections.length > 0) {
      const intersectionId = intersections[0].instanceId;
      if (intersectionId !== undefined) return intersectionId;
    }
    return null;
  };

  private scheduleRerender = () => {
    window.requestAnimationFrame(() => {
      this.renderer.render(this.scene, this.camera);
    });
  };
}

interface Props {
  viewSize: Point;
  node: number;
  run: CelerityRun;
  jobFrames: JobFrameDict | null;
  sampledFrames: SampledFrameDict | null;
  jobVisLines: number;
  displayedTimeSpan: TimeSpan;
  highlightJob: HighlightInfo | null;
  hiddenTypes: Array<JobType>;
  highlightedKernel: string;
  onJobHover: (hoverInfo: HoverInfo | null) => void;
  onJobSelect: (hoverInfo: HoverInfo | null) => void;
}

export default function JobRenderer(props: Props) {
  const containerRef = useRef<HTMLDivElement>(null);

  const rendererRef = useRef<Renderer | null>(null);
  if (rendererRef.current === null) {
    rendererRef.current = new Renderer({
      x: props.viewSize.x,
      y: props.jobVisLines * 12,
    });
  }
  const renderer = rendererRef.current;
  const [isInitialized, setInitialized] = useState(false);

  useLayoutEffect(() => {
    if (!isInitialized) {
      renderer.startRendering(containerRef.current!);
      setInitialized(true);
    }
  }, [isInitialized, renderer]);

  useEffect(() => {
    renderer.setRenderTimeSpan(props.displayedTimeSpan);
  }, [props.displayedTimeSpan, renderer]);

  // jobList is an array, since it is often accessed by index.
  const jobListRef = useRef<Array<[JobId, Job | SampledJob]>>([]);
  const jobIdxMap = useRef<Map<JobId, number>>(new Map());
  const [highlightJobIdx, setHighlightJobIdx] = useState<number | null>(null);
  const commandMapRef = useRef<Map<CommandId, Command> | null>(null);
  const taskMapRef = useRef<Map<TaskId, Task> | null>(null);

  /**
   * Set timeFrames that should be visualized.
   */
  useEffect(() => {
    if (props.jobFrames !== null) {
      const isSampled = false;
      // Need to store added jobIds, since jobs can occur in multiple raw frames.
      let addedJobs: Set<JobId> = new Set();
      jobListRef.current = [];
      jobIdxMap.current = new Map();
      let commandMap = new Map();
      let taskMap = new Map();
      let jobCount = 0;
      for (const frameStartTime in props.jobFrames) {
        const jobFrame = props.jobFrames[frameStartTime];
        for (const jobId in jobFrame.jobs) {
          if (addedJobs.has(Number(jobId))) continue;
          jobIdxMap.current.set(Number(jobId), jobCount);
          addedJobs.add(Number(jobId));
          jobListRef.current.push([Number(jobId), jobFrame.jobs[jobId]]);
          if (
            props.highlightJob !== null &&
            props.highlightJob.jobId === Number(jobId)
          ) {
            setHighlightJobIdx(jobCount);
          }
          jobCount++;
          const commandOfJob =
            jobFrame.commands[jobFrame.jobs[jobId].commandId];
          commandMap.set(jobFrame.jobs[jobId].commandId, commandOfJob);
          if (commandOfJob.taskId !== null) {
            taskMap.set(
              commandOfJob.taskId,
              jobFrame.tasks[commandOfJob.taskId]
            );
          }
        }
      }
      commandMapRef.current = commandMap;
      taskMapRef.current = taskMap;
      renderer.setFrames(
        jobListRef.current,
        props.run,
        isSampled,
        commandMap,
        taskMap
      );
    }
  }, [props.jobFrames, props.run, renderer, props.highlightJob]);

  /**
   * Set sampledFrames that should be visualized.
   */
  useEffect(() => {
    if (props.sampledFrames !== null) {
      const isSampled = true;
      let addedJobs: Set<JobId> = new Set();
      jobListRef.current = [];
      for (const frameStartTime in props.sampledFrames) {
        for (const jobId in props.sampledFrames[frameStartTime]) {
          if (addedJobs.has(Number(jobId))) continue;
          addedJobs.add(Number(jobId));
          jobListRef.current.push([
            Number(jobId),
            props.sampledFrames[frameStartTime][jobId],
          ]);
        }
      }
      renderer.setFrames(jobListRef.current, props.run, isSampled);
    }
  }, [props.sampledFrames, props.run, renderer]);

  /**
   * Set render area.
   */
  useEffect(() => {
    renderer.setRenderAreaSize({
      x: props.viewSize.x,
      y: props.jobVisLines * 12,
    });
  }, [props.viewSize, props.jobVisLines, renderer]);

  const highlightTimeSpan = useSelector(
    (state: RootState) => state.timeline.highlightTimeSpan
  );
  /**
   * Highlight jobs.
   */
  useLayoutEffect(() => {
    if (props.highlightJob !== null) {
      renderer.highlightJobs(
        props.highlightJob,
        highlightJobIdx,
        jobListRef.current,
        props.run
      );
    } else {
      renderer.resetHighlight();
    }
    if (
      highlightTimeSpan !== null &&
      (highlightTimeSpan.nodeId === -1 ||
        highlightTimeSpan.nodeId === props.node)
    ) {
      renderer.highlightTimeSpan(highlightTimeSpan.timeSpan, props.jobVisLines);
    } else {
      renderer.resetHighlightSpan();
    }
  }, [
    props.highlightJob,
    props.run,
    props.node,
    props.jobVisLines,
    renderer,
    highlightJobIdx,
    highlightTimeSpan,
  ]);

  const highlightDependency = useSelector(
    (state: RootState) => state.timeline.highlightDependency
  );

  /**
   * Highlight dependencies.
   */
  useEffect(() => {
    if (
      highlightDependency !== null &&
      highlightDependency.node === props.node
    ) {
      if (
        highlightDependency.requestType === "Command" &&
        commandMapRef.current !== null
      ) {
        const targetCommand = commandMapRef.current.get(
          highlightDependency.targetId
        );
        if (targetCommand === undefined)
          throw new Error(
            `Cannot find command ${highlightDependency.targetId} for highlighting!`
          );
        let highlightedIdx: Array<number> = [
          jobIdxMap.current.get(highlightDependency.targetId) as number,
        ];
        const targetDependencies =
          highlightDependency.dependencyDirection === "Dependee"
            ? targetCommand.dependees
            : targetCommand.dependers;

        for (const dep of targetDependencies) {
          highlightedIdx.push(jobIdxMap.current.get(dep.id) as number);
        }
        renderer.setHighlighted(highlightedIdx);
      }
    }
  }, [highlightDependency, props.node, renderer]);

  // Querying the hovered job (a raycast) is (somewhat) expensive,
  // so we throttle it to once per 100ms.
  const throttleMousePos = useThrottler<{
    x: number;
    y: number;
    rect: DOMRect;
  }>(100);

  const getHoverInfo = useCallback(
    (hoverJobIdx: number | null) => {
      if (hoverJobIdx === null) {
        props.onJobHover(null);
        return null;
      }

      const isSampled = props.sampledFrames === null ? false : true;

      return {
        jobId: jobListRef.current[hoverJobIdx][0],
        job: jobListRef.current[hoverJobIdx][1],
        node: props.node,
        isSampled,
      };
    },
    [props]
  );

  // Highlight jobs related to selected kernel id.
  useEffect(() => {
    if (commandMapRef.current === null) return;
    const highlighted: Array<number> = [];
    commandMapRef.current.forEach((command, commanId) => {
      if (
        command.kernelId !== null &&
        command.kernelId === props.highlightedKernel
      ) {
        //NOTE: Currently only works because each command has the same id as its related job.
        highlighted.push(jobIdxMap.current.get(commanId) as number);
      }
    });
    renderer.setHighlighted(highlighted);
  }, [props.highlightedKernel, renderer]);

  // Only show selected types.
  useEffect(() => {
    if (commandMapRef.current === null) return;
    const visible: Array<number> = [];
    commandMapRef.current.forEach((command, commandId) => {
      for (const hiddenType of props.hiddenTypes) {
        let shouldHide = false;
        if (hiddenType === "deviceTask" || hiddenType === "hostTask") {
          if (command.taskId !== null && taskMapRef.current !== null) {
            const task = taskMapRef.current.get(command.taskId);
            if (task === undefined)
              throw new Error(`Failed to get task of command ${commandId}!`);
            if (
              (hiddenType === "deviceTask" &&
                task.target === ExecutionTarget.DEVICE) ||
              (hiddenType === "hostTask" &&
                task.target === ExecutionTarget.HOST)
            ) {
              shouldHide = true;
            }
          }
        } else if (
          hiddenType === "awaitPush" &&
          command.type === CommandType.AWAIT_PUSH
        ) {
          shouldHide = true;
        } else if (hiddenType === "push" && command.type === CommandType.PUSH) {
          shouldHide = true;
        } else if (
          hiddenType === "horizon" &&
          command.type === CommandType.HORIZON
        ) {
          shouldHide = true;
        }
        if (shouldHide) {
          visible.push(jobIdxMap.current.get(commandId) as number);
        }
      }
    });
    const hideTypes = true;
    renderer.setHighlighted(visible, hideTypes);
  }, [renderer, props.hiddenTypes]);

  const handleClick = useCallback(
    (e: React.MouseEvent<HTMLDivElement>) => {
      const x = e.pageX;
      const y = e.pageY;
      const rect = e.currentTarget.getBoundingClientRect();
      const relativeX = x - rect.left - window.scrollX;
      const relativeY = y - rect.top - window.scrollY;
      const hoverJobIdx = renderer.getJobIdxAtPosition({
        x: relativeX,
        y: relativeY,
      });

      const findSuccessors = e.altKey;

      if (
        hoverJobIdx != null &&
        props.sampledFrames === null /* only works on unsampled data */ &&
        commandMapRef.current !== null // This whole thing is a dirty HACK
      ) {
        const job = jobListRef.current[hoverJobIdx][1] as Job;
        const highlighted = [hoverJobIdx];

        const cmd = commandMapRef.current.get(job.commandId);
        if (cmd != null) {
          for (const dep of findSuccessors ? cmd.dependers : cmd.dependees) {
            // Now find corresponding job (or rather, it's index in the jobs array, which is what we need...)
            for (let j = 0; j < jobListRef.current.length; ++j) {
              if ((jobListRef.current[j][1] as Job).commandId === dep.id) {
                highlighted.push(j);
                break;
              }
            }
          }
        }
        renderer.setHighlighted(highlighted);
      } else {
        renderer.setHighlighted([]);
      }
    },
    [renderer, props.sampledFrames]
  );

  const handleMouseMove = useCallback(
    (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      throttleMousePos(
        {
          x: e.pageX,
          y: e.pageY,
          rect: e.currentTarget.getBoundingClientRect(),
        },
        ({ x, y, rect }) => {
          const relativeX = x - rect.left - window.scrollX;
          const relativeY = y - rect.top - window.scrollY;
          const hoverJobIdx = renderer.getJobIdxAtPosition({
            x: relativeX,
            y: relativeY,
          });
          props.onJobHover(getHoverInfo(hoverJobIdx));
        }
      );
    },
    [throttleMousePos, props, renderer, getHoverInfo]
  );

  const dispatch = useDispatch();
  const handleDoubleClick = useCallback(
    (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      let boundingRect = e.currentTarget.getBoundingClientRect();
      const hoverJobIdx = renderer.getJobIdxAtPosition({
        x: e.pageX - boundingRect.left - window.scrollX,
        y: e.pageY - boundingRect.top - window.scrollY,
      });
      const hoverInfo = getHoverInfo(hoverJobIdx);
      props.onJobSelect(hoverInfo);
      if (hoverInfo !== null) dispatch(setJobModalVisibility(true));
    },
    [dispatch, props, getHoverInfo, renderer]
  );

  const handleMouseLeave = useCallback(() => {
    /**
     * Since {@link handleMouseMove} uses {@link throttleMousePos},
     * we need  to use it here as well to ensure that leave is handled
     * afterwards.
     */
    throttleMousePos({ x: 0, y: 0, rect: new DOMRect() }, () => {
      props.onJobHover(null);
    });
  }, [props, throttleMousePos]);

  return (
    <div
      onMouseMove={handleMouseMove}
      onMouseLeave={handleMouseLeave}
      onDoubleClick={handleDoubleClick}
      onClick={handleClick}
    >
      <div ref={containerRef} style={{ zIndex: 0 }} />
    </div>
  );
}
