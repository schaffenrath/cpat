import { TypedEmitter } from "tiny-typed-emitter";

import { CelerityRun, TimeSpan, RunId } from "../shared/types";
import { PixelSpan } from "./TimelineMouseHandler";

export const ViewPaddingFactor = 1.05;

interface TimeSpanManagerEvents {
  DisplayedTimeSpanChanged: (newTimeSpan: TimeSpan) => void;
  RequestStatisticsInTimeSpan: (runId: RunId, newTimeSpan: TimeSpan) => void;
}

/**
 * Keeps track of the displayed time span of a related timeline. Handles
 * synced time span then timelines are linked.
 */
export class TimeSpanManager extends TypedEmitter<TimeSpanManagerEvents> {
  private rendererWidth: number;
  private displayedTimeSpan: TimeSpan;
  private referenceTimeSpan: TimeSpan;
  private prevUsedRun: CelerityRun | null = null;
  // Necessary to determine if time span changed through linkage.
  private changedByUser: boolean = false;

  constructor(rendererWidth: number) {
    super();
    this.rendererWidth = rendererWidth;
    this.displayedTimeSpan = { start: 0, end: 0 };
    this.referenceTimeSpan = { start: 0, end: 0 };
  }

  private getTimeSpanLength = (): number => {
    return this.displayedTimeSpan.end - this.displayedTimeSpan.start;
  };

  /**
   * Get entire time span of a specific run.
   */
  private getRunTimeSpan = (): TimeSpan | null => {
    if (this.prevUsedRun === null) return null;
    const endPoint =
      (this.prevUsedRun.endTime - this.prevUsedRun.startTime) *
      ViewPaddingFactor;
    return { start: 0, end: endPoint };
  };

  emitChange = () => {
    this.emit("DisplayedTimeSpanChanged", this.displayedTimeSpan);
  };

  /**
   * Set time span to a specific value.
   * This is never directly caused by a user.
   *
   * @param newTimeSpan New target time span.
   */
  setTimeSpan = (newTimeSpan: TimeSpan) => {
    this.displayedTimeSpan = newTimeSpan;
    this.changedByUser = false;
    this.emitChange();
  };

  /**
   * Set time span based on length of a run.
   * This is never caused directly by a user.
   *
   * @param run The information about the run to set the time span.
   */
  setTimeSpanFromRun = (run: CelerityRun) => {
    this.prevUsedRun = run;
    const runTimeSpan = this.getRunTimeSpan();
    if (runTimeSpan !== null) {
      this.displayedTimeSpan = runTimeSpan;

      this.changedByUser = false;
      this.emitChange();
    }
  };

  getTimeSpan = (): TimeSpan => {
    return this.displayedTimeSpan;
  };

  createReferenceTimeSpan = () => {
    this.referenceTimeSpan = Object.assign({}, this.displayedTimeSpan);
  };

  /**
   * Shift time span based on mouse interaction through the user.
   *
   * @param offset The offset caused by mouse movement.
   */
  shiftTimeSpan = (offset: number) => {
    const shiftFactor =
      (offset / this.rendererWidth) * this.getTimeSpanLength();
    const runTimeSpan = this.getRunTimeSpan();
    if (runTimeSpan === null) return;

    const shiftInNegative = this.displayedTimeSpan.start + shiftFactor <= 0;
    const shiftOutOfEndPoint =
      this.displayedTimeSpan.end + shiftFactor > runTimeSpan.end;

    if (shiftInNegative) {
      const newEndPoint =
        this.displayedTimeSpan.end - this.displayedTimeSpan.start;
      this.displayedTimeSpan = { start: 0, end: newEndPoint };
    } else if (shiftOutOfEndPoint) {
      const newStartPoint =
        this.displayedTimeSpan.start -
        (this.displayedTimeSpan.end - runTimeSpan.end);
      this.displayedTimeSpan = { start: newStartPoint, end: runTimeSpan.end };
    } else {
      this.displayedTimeSpan = {
        start: this.referenceTimeSpan.start + shiftFactor,
        end: this.referenceTimeSpan.end + shiftFactor,
      };
    }
    this.changedByUser = true;
    this.emitChange();
  };

  /**
   * Zoom in or out time span based on mouse interaction through the user.
   *
   * @param zoomDirection Determines if zoom in or zoom out.
   * @param horizontalMousePos The current horizontal position of the mouse cursor.
   */
  zoomTimeSpan = (zoomDirection: number, horizontalMousePos: number) => {
    const zoomTimeDif = this.getTimeSpanLength() * zoomDirection * 0.1;
    let newStartPoint =
      this.displayedTimeSpan.start + zoomTimeDif * horizontalMousePos;
    let newEndPoint =
      this.displayedTimeSpan.end - zoomTimeDif * (1 - horizontalMousePos);

    const runTimeSpan = this.getRunTimeSpan();
    if (runTimeSpan === null) return;

    const zoomedTooFarOut = newEndPoint - newStartPoint > runTimeSpan.end;
    const zoomedInNegative = newStartPoint < 0;
    const zoomedOutOfEndPoint = newEndPoint > runTimeSpan.end;

    if (zoomedTooFarOut) {
      if (zoomedInNegative) {
        if (zoomedOutOfEndPoint) {
          newStartPoint = 0;
          newEndPoint = runTimeSpan.end;
        } else {
          newEndPoint -= newStartPoint;
          newStartPoint = 0;
        }
      }
      if (zoomedOutOfEndPoint) {
        if (newStartPoint + (runTimeSpan.end - newEndPoint) < 0) {
          newStartPoint = 0;
          newEndPoint = runTimeSpan.end;
        } else {
          newStartPoint += runTimeSpan.end - newEndPoint;
          newEndPoint = runTimeSpan.end;
        }
      }
    } else {
      if (zoomedInNegative) {
        newEndPoint -= newStartPoint;
        newStartPoint = 0;
      } else if (zoomedOutOfEndPoint) {
        newStartPoint += runTimeSpan.end - newEndPoint;
        newEndPoint = runTimeSpan.end;
      }
    }
    this.displayedTimeSpan = { start: newStartPoint, end: newEndPoint };
    this.changedByUser = true;
    this.emitChange();
  };

  resetZoom = () => {
    const runTimeSpan = this.getRunTimeSpan();
    if (runTimeSpan !== null) this.displayedTimeSpan = runTimeSpan;
    this.changedByUser = true;
    this.emitChange();
  };

  wasChangedByUser = (): boolean => {
    return this.changedByUser;
  };

  resetChangedByUser = () => {
    this.changedByUser = false;
  };

  /**
   * Convert the pixel position of the mouse cursor to a time point in the timeline.
   *
   * @param pixelPos
   */
  private convertPixelPosToTimePoint = (pixelPos: number): number => {
    return (
      (pixelPos / this.rendererWidth) * this.getTimeSpanLength() +
      this.getTimeSpan().start
    );
  };

  /**
   * Convert the selected screen area in pixels to a time span for the timeline.
   *
   * @param selectionSpan
   */
  mouseSelectionToTimeSpan = (selectionSpan: PixelSpan): TimeSpan => {
    const runTimeSpan = this.getRunTimeSpan();
    if (runTimeSpan === null) return { start: 0, end: 0 };
    let newStartPoint = this.convertPixelPosToTimePoint(selectionSpan.start);
    let newEndPoint = this.convertPixelPosToTimePoint(selectionSpan.end);
    if (newStartPoint < 0) newStartPoint = 0;
    if (newEndPoint > runTimeSpan.end) newEndPoint = runTimeSpan.end;
    return { start: newStartPoint, end: newEndPoint };
  };

  setTimeSpanFromMouseSelection = (mouseSelection: PixelSpan) => {
    const timeSpanFromMouse = this.mouseSelectionToTimeSpan(mouseSelection);
    this.displayedTimeSpan = timeSpanFromMouse;
    this.changedByUser = true;
    this.emitChange();
  };

  setStatisticsSpanFromMouse = (mouseSelection: PixelSpan) => {
    const timeSpanFromMouse = this.mouseSelectionToTimeSpan(mouseSelection);
    if (this.prevUsedRun !== null) {
      this.emit(
        "RequestStatisticsInTimeSpan",
        this.prevUsedRun.id,
        timeSpanFromMouse
      );
    }
  };
}
