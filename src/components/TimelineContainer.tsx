import React, {
  useMemo,
  useEffect,
  useCallback,
  useState,
  useRef,
} from "react";
import { v4 as uuidv4 } from "uuid";
import { Button, Select, Row, Col } from "antd";
import { LinkOutlined, DisconnectOutlined } from "@ant-design/icons";
import { useSelector, useDispatch } from "react-redux";

import Timeline from "./Timeline";
import TimelineLegend from "./TimelineLegend";
import { RootState } from "../redux/store";
import {
  setDisplayedRun,
  removeDisplayedRun,
  setLinkedTimeSpan,
  toggleViewLink,
} from "../redux/reducers/timelinesSlice";
import {
  clearMostRecentRuns,
  deleteRun,
} from "../redux/reducers/celerityDataSlice";
import { RunId, DependencyInfo, TimeSpan } from "../shared/types";
import { ViewType } from "../views/ViewHandler";
import { ViewPaddingFactor } from "./TimeSpanManager";
import Spinner from "./Spinner";

const { Option, OptGroup } = Select;

export type TimelineId = string;

interface Props {
  showView: (newView: ViewType) => void;
  requestRun: (runId: RunId) => void;
  requestRunDeletion: (runId: RunId) => void;
  requestTimeFrames: (
    runId: RunId,
    viewSpan: TimeSpan,
    sampleLevel?: number
  ) => void;
  requestSpecificFrames: (requestInfo: DependencyInfo) => void;
  requestStatistics: (runId: RunId, timeSpan: TimeSpan) => void;
}

export default function TimelineContainer(props: Props) {
  const [, forceRerender] = useState<Object>({});

  const dispatch = useDispatch();

  const availableRuns = useSelector(
    (state: RootState) => state.celerityRun.availableRuns
  );

  const timelinesOfRuns = useRef<Map<TimelineId, JSX.Element>>(new Map());
  /**
   * Remove a displayed timeline from the view and force a rerender.
   * This does not affect the data stored in the frontend.
   */
  const handleTimelineDeletion = useCallback(
    (timelineId: TimelineId) => {
      timelinesOfRuns.current.delete(timelineId);
      dispatch(removeDisplayedRun(timelineId));

      // If the only displayed timeline is removed, go to the Run-Selection-View
      if (
        timelinesOfRuns.current.size === 0 &&
        Object.keys(availableRuns).length !== 0
      )
        props.showView("RunSelection");
      forceRerender({});
    },
    [dispatch, props, availableRuns]
  );

  /**
   * Handle request to delete run entirely (in backend and frontend)
   */
  const handleRunDeletionRequest = useCallback(
    (runId: RunId, timelineId: TimelineId) => {
      handleTimelineDeletion(timelineId);
      dispatch(deleteRun(runId));
      props.requestRunDeletion(runId);
      if (timelinesOfRuns.current.size === 0) props.showView("Initial");
    },
    [props, handleTimelineDeletion, dispatch, timelinesOfRuns]
  );

  const [addRunWhenAvailable, setRunAddedInFuture] = useState<string>("");

  const timelineCountRef = useRef<number>(0);

  /**
   * Add a new timeline to the view.
   */
  const addTimeline = useCallback(
    (runId: RunId) => {
      const timelineId = uuidv4();
      const newTimeline = (
        <div
          key={timelineCountRef.current}
          style={{ display: "flex", marginBottom: 15 }}
        >
          <Timeline
            timelineId={timelineId}
            initialRun={runId}
            removeTimeline={handleTimelineDeletion}
            requestRunDeletion={handleRunDeletionRequest}
            requestTimeFrames={props.requestTimeFrames}
            requestSpecificFrames={props.requestSpecificFrames}
            requestStatistics={props.requestStatistics}
          />
        </div>
      );
      timelineCountRef.current += 1;
      timelinesOfRuns.current.set(timelineId, newTimeline);
      dispatch(setDisplayedRun({ timelineId: timelineId, runId: runId }));
      forceRerender({});
    },
    [
      dispatch,
      handleTimelineDeletion,
      handleRunDeletionRequest,
      props.requestTimeFrames,
      props.requestSpecificFrames,
      props.requestStatistics,
    ]
  );

  const savedRuns = useSelector((state: RootState) => state.celerityRun.runs);
  /**
   * Setup new timeline by requesting data from backend if not available locally.
   */
  const setupNewTimeline = useCallback(
    (runId: RunId) => {
      if (savedRuns[runId] === undefined) {
        setRunAddedInFuture(runId);
        props.requestRun(runId);
      } else {
        addTimeline(runId);
      }
    },
    [addTimeline, props, savedRuns]
  );

  const displayedRuns = useSelector(
    (state: RootState) => state.timeline.displayedRuns
  );

  const mainRun = useSelector(
    (state: RootState) => state.celerityRun.selectedMainRun
  );

  /**
   * Creates the timeline for the only or selected (from selection view) run.
   */
  useEffect(() => {
    if (timelinesOfRuns.current.size === 0 && addRunWhenAvailable === "") {
      timelinesOfRuns.current.clear();
      const newRunForTimeline: RunId =
        mainRun !== "" ? mainRun : Object.keys(availableRuns)[0];
      setupNewTimeline(newRunForTimeline);
    }
  }, [mainRun, setupNewTimeline, addRunWhenAvailable, availableRuns]);

  const mostRecentAddedRuns = useSelector(
    (state: RootState) => state.celerityRun.mostRecentRuns
  );

  /**
   * Create timelines for newly received runs.
   */
  useEffect(() => {
    if (mostRecentAddedRuns.length !== 0 && displayedRuns.length !== 0) {
      for (const recentRun of mostRecentAddedRuns) {
        setupNewTimeline(recentRun);
      }
      dispatch(clearMostRecentRuns());
    }
  }, [mostRecentAddedRuns, dispatch, setupNewTimeline, displayedRuns]);

  /**
   * If run was request, check if it's available now and add the timeline.
   */
  useEffect(() => {
    if (addRunWhenAvailable !== "") {
      if (savedRuns[addRunWhenAvailable] !== undefined) {
        setRunAddedInFuture("");
        setupNewTimeline(addRunWhenAvailable);
      }
    }
  }, [savedRuns, addRunWhenAvailable, setupNewTimeline]);

  const [displayAvailRuns, setDisplayAvailRuns] = useState<boolean>(false);
  const handleAddTimelineClick = useCallback(() => {
    setDisplayAvailRuns((prevVal) => !prevVal);
  }, []);

  const handleRunSelection = useCallback(
    (runId: string) => {
      setDisplayAvailRuns(false);
      setupNewTimeline(runId);
    },
    [setupNewTimeline]
  );

  /**
   * When the time span of timelines is linked, the initial time span is set to the first timeline.
   */
  const handleViewLinkToggle = useCallback(() => {
    const firstRun = savedRuns[displayedRuns[0].runId];
    if (firstRun === undefined)
      throw new Error(
        `Cannot access first celerity run which should be available.`
      );
    dispatch(
      setLinkedTimeSpan({
        timelineId: displayedRuns[0].timelineId,
        timeSpan: {
          start: 0,
          end: (firstRun.endTime - firstRun.startTime) * ViewPaddingFactor,
        },
      })
    );
    dispatch(toggleViewLink());
  }, [dispatch, savedRuns, displayedRuns]);

  const convertRunIdsToSelectOptions = useCallback(
    (runIds: Array<RunId>) => {
      let options: Array<JSX.Element> = [];
      runIds.forEach((runId) => {
        options.push(
          <Option key={uuidv4()} value={runId}>
            {availableRuns[runId]}
          </Option>
        );
      });
      return options;
    },
    [availableRuns]
  );

  const availableRunItems: Array<JSX.Element> = useMemo(() => {
    let result: Array<JSX.Element> = [];
    let displayed: Array<RunId> = [];
    let notDisplayed: Array<RunId> = [];

    // Separate available runs in already displayed and not displayed to distinct in selection menu.
    Object.keys(availableRuns).forEach((availRunId) => {
      const isRunDisplayed =
        displayedRuns.find(
          (displayedRun) => displayedRun.runId === availRunId
        ) !== undefined;
      if (isRunDisplayed) displayed.push(availRunId);
      else notDisplayed.push(availRunId);
    });

    let notDisplayedRunOptions: Array<JSX.Element> =
      convertRunIdsToSelectOptions(notDisplayed);
    let displayedRunOptions: Array<JSX.Element> =
      convertRunIdsToSelectOptions(displayed);

    result.push(
      <OptGroup key={uuidv4()} label="Not displayed">
        {notDisplayedRunOptions}
      </OptGroup>
    );
    result.push(
      <OptGroup key={uuidv4()} label="Already displayed">
        {displayedRunOptions}
      </OptGroup>
    );

    return result;
  }, [availableRuns, displayedRuns, convertRunIdsToSelectOptions]);

  const isToggleViewLinked = useSelector(
    (state: RootState) => state.timeline.linkedViews
  );
  const buttons = useMemo(() => {
    return (
      <div style={{ display: "flex" }}>
        <div>
          <Button
            onClick={handleViewLinkToggle}
            style={{ position: "relative" }}
          >
            Linked Timelines:
            {isToggleViewLinked ? <LinkOutlined /> : <DisconnectOutlined />}
          </Button>
        </div>
        <div style={{}}>
          <div style={{ display: "flex" }}>
            <Button onClick={handleAddTimelineClick}>+ Add Timeline</Button>
            {displayAvailRuns && (
              <div style={{ display: "flex" }}>
                <Select
                  style={{ width: 300 }}
                  onChange={handleRunSelection}
                  placeholder={"Select run for timeline"}
                >
                  {availableRunItems}
                </Select>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }, [
    isToggleViewLinked,
    handleViewLinkToggle,
    handleAddTimelineClick,
    availableRunItems,
    handleRunSelection,
    displayAvailRuns,
  ]);

  if (Object.keys(savedRuns).length === 0)
    return (
      <div>
        <Spinner></Spinner>
      </div>
    );

  return (
    <div>
      <TimelineLegend />
      {Array.from(timelinesOfRuns.current.values())}
      {buttons}
      <Row>
        <Col span={4}>
          <p>
            <b>Shift + Drag:</b>
            <br /> Zoom in area.{" "}
          </p>
        </Col>
        <Col span={4}>
          <p>
            <b>Right click + Drag:</b>
            <br /> Zoom in.{" "}
          </p>
        </Col>
        <Col span={4}>
          <p>
            <b>Ctrl + Drag:</b>
            <br /> Show statistics.{" "}
          </p>
        </Col>
        <Col span={4}>
          <p>
            <b>Double click:</b>
            <br /> Show job info.{" "}
          </p>
        </Col>
        <Col span={4}>
          <p>
            <b>Job + Click:</b>
            <br /> Show predecessors.{" "}
          </p>
        </Col>
        <Col span={4}>
          <p>
            <b>Alt + Job + Click:</b>
            <br /> Show successors.{" "}
          </p>
        </Col>
      </Row>
    </div>
  );
}
