import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { useDispatch, useSelector } from "react-redux";
import { Network, Node, Edge, Options } from "vis-network";
import { DataSet } from "vis-data";
import { Divider, Row, Col, Table, Button, Switch, Tag } from "antd";

import {
  Command,
  Task,
  JobFrame,
  DependencyKind,
  CommandType,
  ExecutionTarget,
  KernelId,
  CommandId,
  TaskId,
  NodeId,
  BufferId,
  BufferAccess,
  AccessMode,
  DependencyInfo,
  JobType,
  JobFrameDict,
  DependencyType,
} from "../shared/types";
import { RootState } from "../redux/store";
import {
  convertRGBtoHex,
  getDependencyColor,
  getHEXColorOfType,
  getKernelColor,
} from "../colorCoding";
import {
  highlightDependency,
  showDependencyGraph,
} from "../redux/reducers/timelinesSlice";
import { ColumnsType } from "antd/lib/table";
import Modal from "antd/lib/modal/Modal";
import styled from "styled-components";

const DottedLine = styled.div`
    border: 2px dotted black;
    width: 20px; height 1px;
    transform: translateY(10px);
    margin-right: 5px;
  `;
const ColoredLine = styled.div.attrs<{ lineColor: string }>((props) => ({
  style: {
    borderColor: props.lineColor,
  },
}))<{ lineColor: string }>`
    border: 2px solid;
    width: 20px; height 1px;
    transform: translateY(10px);
    margin-right: 5px;
  `;

const LegendEntry = styled.div`
  display: inline-block;
  margin-left: 0.5em;
  margin-top: 0.2em;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 12px;
  color: #5b5b5b;
`;

interface Props {
  requestIfNotAvailable: (requestInfo: DependencyInfo) => void;
}
/**
 * Creates a dependency graph.
 *
 * @param props
 */
export default function (props: Props) {
  const dispatch = useDispatch();
  const dependencyGraphInfo = useSelector(
    (state: RootState) => state.timeline.showDependencyGraph
  );
  const savedRuns = useSelector((state: RootState) => state.celerityRun.runs);
  const timeFrames = useSelector(
    (state: RootState) => state.celerityRun.timeFrames
  );
  const baseFrameLengthsOfRuns = useSelector(
    (state: RootState) => state.celerityRun.baseFrameLength
  );

  const baseFrameLength = useMemo(
    () =>
      dependencyGraphInfo !== null
        ? baseFrameLengthsOfRuns[dependencyGraphInfo.runId]
        : 0,
    [baseFrameLengthsOfRuns, dependencyGraphInfo]
  );

  const graphRef = useRef<HTMLDivElement>(null);

  const getColorOfTarget = useCallback(
    (
      target: Command | Task,
      dependencyType: DependencyType,
      jobFrame?: JobFrame,
      kernels?: Array<KernelId>
    ) => {
      if (dependencyGraphInfo === null) return;
      if (dependencyType === "Command") {
        const cmd = target as Command;
        if (cmd.type === CommandType.TASK) {
          if (kernels === undefined)
            throw new Error(`Run must be provided if command has type Task!`);
          const taskOfCmd = jobFrame?.tasks[cmd.taskId as number];
          if (taskOfCmd === undefined)
            throw new Error(`Unable to find task ${cmd.taskId}!`);
          return taskOfCmd.target === ExecutionTarget.DEVICE
            ? convertRGBtoHex(getKernelColor(cmd.kernelId as string, kernels))
            : getHEXColorOfType("hostTask");
        } else {
          switch (cmd.type) {
            case CommandType.AWAIT_PUSH:
              return getHEXColorOfType("awaitPush");
            case CommandType.PUSH:
              return getHEXColorOfType("push");
            case CommandType.HORIZON:
              return getHEXColorOfType("horizon");
          }
        }
      }
    },
    [dependencyGraphInfo]
  );

  const displayNodeRef = useRef<CommandId | TaskId | null>(null);

  type FrameStart = number;
  const graphNodeMapRef = useRef<Map<CommandId | TaskId, FrameStart>>(
    new Map()
  );

  const showDependeesOnClick = useRef<boolean>(true);

  interface DependencyOfTarget {
    depender: Command | Task;
    inTimeFrame: number;
    dependees: Array<[CommandId | TaskId, Command | Task]>;
  }

  const dependencyInfoRef = useRef<DependencyOfTarget | null>(null);

  const updateDependencyInfo = useCallback(() => {
    if (
      dependencyGraphInfo === null ||
      timeFrames[dependencyGraphInfo.runId] === undefined ||
      timeFrames[dependencyGraphInfo.runId].jobFrames === undefined
    ) {
      return null;
    }
    const jobFrames =
      timeFrames[dependencyGraphInfo.runId].jobFrames[dependencyGraphInfo.node];
    // Necessary conversion to keyof JobFrame for accessing jobFrames.
    let depType: keyof JobFrame =
      dependencyGraphInfo.dependencyType === "Command" ? "commands" : "tasks";
    let foundTarget: Command | Task | undefined = undefined;
    let searchTimeFrame: number;
    if (
      displayNodeRef.current !== null &&
      graphNodeMapRef.current.get(displayNodeRef.current) !== undefined
    ) {
      searchTimeFrame = graphNodeMapRef.current.get(
        displayNodeRef.current
      ) as number;
      if (jobFrames[searchTimeFrame] === undefined) {
        addDependencyInFuture.current = displayNodeRef.current;
        return;
      }
      foundTarget = jobFrames[searchTimeFrame][depType][displayNodeRef.current];
    } else {
      searchTimeFrame =
        Math.floor(dependencyGraphInfo.inRange.start / baseFrameLength) *
        baseFrameLength;
      while (foundTarget === undefined) {
        if (searchTimeFrame > dependencyGraphInfo.inRange.end) break;
        if (
          jobFrames[searchTimeFrame][depType][dependencyGraphInfo.targetId] !==
          undefined
        ) {
          foundTarget =
            jobFrames[searchTimeFrame][depType][dependencyGraphInfo.targetId];
        } else {
          searchTimeFrame += baseFrameLength;
          if (searchTimeFrame > dependencyGraphInfo.inRange.end) {
            break;
          }
        }
      }
    }

    const targetList: Array<[CommandId | TaskId, Command | Task]> = [];
    if (foundTarget !== undefined) {
      let dependencies = showDependeesOnClick.current
        ? foundTarget.dependees
        : foundTarget.dependers;
      let foundDependee = false;
      for (const dependee of dependencies) {
        foundDependee = false;
        let frameStartTimeIter = searchTimeFrame;
        let frameStartCondition = showDependeesOnClick.current
          ? frameStartTimeIter >= 0
          : frameStartTimeIter <
            savedRuns[dependencyGraphInfo.runId].endTime -
              savedRuns[dependencyGraphInfo.runId].startTime;
        while (frameStartCondition && !foundDependee) {
          if (jobFrames[frameStartTimeIter] !== undefined) {
            const target = jobFrames[frameStartTimeIter][depType][dependee.id];
            if (target !== undefined) {
              targetList.push([dependee.id, target]);
              foundDependee = true;
            }
          }
          if (showDependeesOnClick.current)
            frameStartTimeIter -= baseFrameLength;
          else frameStartTimeIter += baseFrameLength;
          frameStartCondition = showDependeesOnClick.current
            ? frameStartTimeIter >= 0
            : frameStartTimeIter <
              savedRuns[dependencyGraphInfo.runId].endTime -
                savedRuns[dependencyGraphInfo.runId].startTime;
        }
      }

      if (foundDependee) {
        dependencyInfoRef.current = {
          depender: foundTarget,
          inTimeFrame: searchTimeFrame,
          dependees: targetList,
        };
      } else {
        dependencyInfoRef.current = null;
        if (addDependencyInFuture.current === null)
          addDependencyInFuture.current = displayNodeRef.current;
      }
      return;
    }
    dependencyInfoRef.current = null;
  }, [dependencyGraphInfo, timeFrames, baseFrameLength, savedRuns]);

  interface GraphDataSet {
    target: CommandId | TaskId;
    nodes: DataSet<Node>;
    edges: DataSet<Edge>;
  }

  const graphDataRef = useRef<GraphDataSet>({
    target: -1,
    nodes: new DataSet(),
    edges: new DataSet(),
  });

  const initGraphData = useCallback(() => {
    if (dependencyGraphInfo === null) {
      return;
    }
    if (
      dependencyInfoRef.current === null ||
      timeFrames[dependencyGraphInfo.runId].jobFrames === undefined
    ) {
      addDependencyInFuture.current = dependencyGraphInfo.targetId;
      return;
    }

    const jobFrames =
      timeFrames[dependencyGraphInfo.runId].jobFrames[dependencyGraphInfo.node];
    let depType: keyof JobFrame =
      dependencyGraphInfo.dependencyType === "Command" ? "commands" : "tasks";
    const kernels = savedRuns[dependencyGraphInfo.runId].kernels;
    let nodes: Array<Node> = [];
    let edges: Array<Edge> = [];
    if (depType === "commands") {
      graphNodeMapRef.current.set(
        dependencyGraphInfo.targetId,
        dependencyInfoRef.current.inTimeFrame
      );

      nodes.push({
        id: dependencyGraphInfo.targetId,
        label: dependencyGraphInfo.targetId.toString(),
        color: getColorOfTarget(
          dependencyInfoRef.current.depender as Command,
          dependencyGraphInfo.dependencyType,
          jobFrames[dependencyInfoRef.current.inTimeFrame],
          kernels
        ),
      });
    } else {
      graphNodeMapRef.current.set(
        dependencyGraphInfo.targetId,
        dependencyInfoRef.current.inTimeFrame
      );
      nodes.push({
        id: dependencyGraphInfo.targetId,
        label: dependencyGraphInfo.targetId.toString(),
        shape: "box",
        color: "#aaaaaa",
        margin: { top: 10, right: 10, bottom: 10, left: 10 },
      });
    }

    for (const dependee of dependencyInfoRef.current.depender.dependees) {
      let frameStartBack = dependencyInfoRef.current.inTimeFrame;
      let foundDependee = false;
      while (frameStartBack >= 0 && !foundDependee) {
        if (jobFrames[frameStartBack] === undefined) {
          return;
        }
        const target = jobFrames[frameStartBack][depType][dependee.id];
        if (target !== undefined) {
          if (depType === "commands") {
            graphNodeMapRef.current.set(dependee.id, frameStartBack);
            nodes.push({
              id: dependee.id,
              label: dependee.id.toString(),
              color: getColorOfTarget(
                target,
                dependencyGraphInfo.dependencyType,
                jobFrames[frameStartBack],
                kernels
              ),
            });
            edges.push({
              from: dependee.id,
              to: dependencyGraphInfo.targetId,
              dashes: dependee.kind === DependencyKind.ANTI_DEP,
              color: getDependencyColor(dependee.kind),
            });
          } else if (depType === "tasks") {
            graphNodeMapRef.current.set(dependee.id, frameStartBack);
            nodes.push({
              id: dependee.id,
              label: dependee.id.toString(),
              shape: "box",
              color: "#aaaaaa",
              margin: { top: 10, right: 10, bottom: 10, left: 10 },
            });
          }
          edges.push({
            from: dependee.id,
            to: dependencyGraphInfo.targetId,
            dashes: dependee.kind === DependencyKind.ANTI_DEP,
            color: getDependencyColor(dependee.kind),
          });
          foundDependee = true;
        }
        frameStartBack -= baseFrameLength;
      }
    }

    graphDataRef.current.target = dependencyGraphInfo.targetId;
    graphDataRef.current.nodes.clear();
    graphDataRef.current.edges.clear();

    graphDataRef.current.nodes.add(nodes);
    graphDataRef.current.edges.add(edges);
    addDependencyInFuture.current = null;
  }, [
    dependencyInfoRef,
    dependencyGraphInfo,
    timeFrames,
    baseFrameLength,
    savedRuns,
    getColorOfTarget,
  ]);

  const addDependencyInFuture = useRef<CommandId | TaskId | null>(null);

  const addDependency = useCallback(() => {
    if (dependencyGraphInfo === null || displayNodeRef.current === null) return;

    let newNodes: Array<Node> = [];
    let newEdges: Array<Edge> = [];
    let clickedFrameStart = graphNodeMapRef.current.get(
      displayNodeRef.current
    ) as number;
    let foundDependency = false;
    if (dependencyInfoRef.current !== null) {
      const kernels = savedRuns[dependencyGraphInfo.runId].kernels;
      const depType: keyof JobFrame =
        dependencyGraphInfo.dependencyType === "Command" ? "commands" : "tasks";

      let jobFrames =
        timeFrames[dependencyGraphInfo.runId].jobFrames[
          dependencyGraphInfo.node
        ];

      if (clickedFrameStart === undefined)
        throw new Error(
          `Graph node ${displayNodeRef.current} not in node map!`
        );
      if (jobFrames[clickedFrameStart] === undefined) {
        addDependencyInFuture.current = displayNodeRef.current;
        return;
      }

      let clickedTarget =
        jobFrames[clickedFrameStart][depType][displayNodeRef.current];

      if (clickedTarget === undefined) {
        return;
      }

      const dependencyInDirection = showDependeesOnClick.current
        ? clickedTarget.dependees
        : clickedTarget.dependers;

      if (dependencyInDirection.length !== 0) {
        for (const dep of dependencyInDirection) {
          let searchFrameTime = dependencyInfoRef.current.inTimeFrame;

          let searchCondition = showDependeesOnClick.current
            ? searchFrameTime >= 0
            : searchFrameTime <
              savedRuns[dependencyGraphInfo.runId].endTime -
                savedRuns[dependencyGraphInfo.runId].startTime;
          while (searchCondition) {
            if (
              jobFrames[searchFrameTime] !== undefined &&
              jobFrames[searchFrameTime][depType][dep.id] !== undefined
            ) {
              foundDependency = true;
              if (depType === "commands") {
                if (!graphNodeMapRef.current.has(dep.id)) {
                  graphNodeMapRef.current.set(dep.id, searchFrameTime);
                  newNodes.push({
                    id: dep.id,
                    label: dep.id.toString(),
                    color: getColorOfTarget(
                      jobFrames[searchFrameTime][depType][dep.id],
                      dependencyGraphInfo.dependencyType,
                      jobFrames[searchFrameTime],
                      kernels
                    ),
                  });
                }
                let orig = showDependeesOnClick.current
                  ? dep.id
                  : displayNodeRef.current;
                let dest = showDependeesOnClick.current
                  ? displayNodeRef.current
                  : dep.id;
                newEdges.push({
                  from: orig,
                  to: dest,
                  dashes: dep.kind === DependencyKind.ANTI_DEP,
                  color: getDependencyColor(dep.kind),
                });
              } else {
                if (!graphNodeMapRef.current.has(dep.id)) {
                  graphNodeMapRef.current.set(dep.id, searchFrameTime);
                  newNodes.push({
                    id: dep.id,
                    label: dep.id.toString(),
                    shape: "box",
                    color: "#aaaaaa",
                    margin: { top: 10, right: 10, bottom: 10, left: 10 },
                  });
                }
                let orig = showDependeesOnClick.current
                  ? dep.id
                  : displayNodeRef.current;
                let dest = showDependeesOnClick.current
                  ? displayNodeRef.current
                  : dep.id;
                newEdges.push({
                  from: orig,
                  to: dest,
                  dashes: dep.kind === DependencyKind.ANTI_DEP,
                  color: getDependencyColor(dep.kind),
                });
              }
              break;
            }
            if (showDependeesOnClick.current)
              searchFrameTime -= baseFrameLength;
            else searchFrameTime += baseFrameLength;

            searchCondition = showDependeesOnClick.current
              ? searchFrameTime >= 0
              : searchFrameTime <
                savedRuns[dependencyGraphInfo.runId].endTime -
                  savedRuns[dependencyGraphInfo.runId].startTime;
          }
          if (!foundDependency) {
            break;
          }
        }
      } else {
        // If no dependencies exist, don't request any.
        foundDependency = true;
      }
    }

    if (
      !foundDependency &&
      addDependencyInFuture.current === displayNodeRef.current
    ) {
      if (clickedFrameStart === undefined)
        clickedFrameStart = dependencyGraphInfo.inRange.start;

      addDependencyInFuture.current = displayNodeRef.current;

      props.requestIfNotAvailable({
        targetId: displayNodeRef.current,
        node: dependencyGraphInfo.node,
        requestType:
          dependencyGraphInfo.dependencyType === "Command" ? "Command" : "Task",
        dependencyDirection: showDependeesOnClick.current
          ? "Dependee"
          : "Depender",
        inRange: {
          start: clickedFrameStart,
          end: clickedFrameStart + baseFrameLength,
        },
        runId: dependencyGraphInfo.runId,
      });
    }

    if (graphDataRef.current !== null) {
      addDependencyInFuture.current = null;
      graphDataRef.current.nodes.add(newNodes);
      graphDataRef.current.edges.add(newEdges);
    }
  }, [
    dependencyInfoRef,
    dependencyGraphInfo,
    baseFrameLength,
    timeFrames,
    savedRuns,
    props,
    getColorOfTarget,
  ]);

  const [bufferAccessVisible, setBufferAccessVisible] =
    useState<boolean>(false);

  interface TableBufferAccessData {
    key: number;
    id: number;
    accessMode: string;
    accessRange: string;
  }

  const [commandForBufferAccess, setCommandForBufferAccess] =
    useState<Array<BufferAccess> | null>(null);

  const tableBufferAccessColumns =
    useMemo((): ColumnsType<TableBufferAccessData> => {
      return [
        { title: "Id", dataIndex: "id", key: "id" },
        { title: "Access Mode", dataIndex: "accessMode", key: "accessMode" },
        { title: "Access Range", dataIndex: "accessRange", key: "accessRange" },
      ];
    }, []);

  const tableBufferAccessData = useMemo((): Array<TableBufferAccessData> => {
    if (!bufferAccessVisible || commandForBufferAccess === null) return [];
    let newTableBufferAccessData: Array<TableBufferAccessData> = [];
    for (const bufferAccess of commandForBufferAccess) {
      newTableBufferAccessData.push({
        key: bufferAccess.id,
        id: bufferAccess.id,
        accessMode: AccessMode[bufferAccess.mode],
        accessRange:
          "[" + bufferAccess.range.min + "] - [" + bufferAccess.range.max + "]",
      });
    }
    return newTableBufferAccessData;
  }, [bufferAccessVisible, commandForBufferAccess]);

  const handleShowBufferAccess = useCallback((command: Array<BufferAccess>) => {
    setCommandForBufferAccess(command);
    setBufferAccessVisible(true);
  }, []);

  interface TableCommandData {
    key: number;
    commandId: CommandId;
    type: JobType;
    otherNode: NodeId | null;
    bufferId: BufferId | null;
    kernelId: KernelId | null;
    execRange: string | null;
    bufferAccess: Array<BufferAccess> | null;
  }

  interface TableTaskData {
    key: number;
    taskId: TaskId;
    kernelId: KernelId;
  }

  const tableCommandColumns = useMemo((): ColumnsType<TableCommandData> => {
    if (
      dependencyGraphInfo === null ||
      dependencyGraphInfo.dependencyType !== "Command"
    )
      return [];
    return [
      {
        title: "Command Id",
        dataIndex: "commandId",
        key: "commandId",
        render: (cmdId: number) => {
          if (cmdId === displayNodeRef.current)
            return (
              <p>
                <b>{cmdId}</b>
              </p>
            );
          return <p>{cmdId}</p>;
        },
      },
      {
        title: "Type",
        dataIndex: "type",
        key: "type",
        render: (type: JobType) => {
          const color = getHEXColorOfType(type);
          return <Tag color={color}>{type}</Tag>;
        },
      },
      { title: "Other Node", dataIndex: "otherNode", key: "otherNode" },
      { title: "Buffer Id", dataIndex: "bufferId", key: "bufferId" },
      { title: "Kernel Id", dataIndex: "kernelId", key: "kernelId" },
      {
        title: "Execution Range",
        dataIndex: "execRange",
        key: "execRange",
      },
      {
        title: "Buffer Accesses",
        dataIndex: "bufferAccess",
        key: "bufferAccess",
        render: (command: Array<BufferAccess> | null) => {
          if (command === null) return <div></div>;
          return (
            <Button
              type="default"
              size={"small"}
              onClick={() => handleShowBufferAccess(command)}
            >
              Show
            </Button>
          );
        },
      },
    ];
  }, [dependencyGraphInfo, handleShowBufferAccess]);

  const tableTaskColumns = useMemo((): ColumnsType<TableTaskData> => {
    if (
      dependencyGraphInfo === null ||
      dependencyGraphInfo.dependencyType !== "Task"
    )
      return [];
    return [
      {
        title: "Task Id",
        dataIndex: "taskId",
        key: "taskId",
        render: (taskId: number) => {
          if (taskId === displayNodeRef.current)
            return (
              <p>
                <b>{taskId}</b>
              </p>
            );
          return <p>{taskId}</p>;
        },
      },
      { title: "Kernel Id", dataIndex: "kernelId", key: "kernelId" },
    ];
  }, [dependencyGraphInfo]);

  const getJobTypeFromCommand = useCallback(
    (
      commandId: CommandId,
      commandType: CommandType,
      jobFrames: JobFrameDict,
      taskId: TaskId | null
    ): JobType => {
      switch (commandType) {
        case CommandType.TASK:
          if (
            jobFrames[graphNodeMapRef.current.get(commandId) as number] ===
            undefined
          ) {
            return "deviceTask";
          }
          return jobFrames[graphNodeMapRef.current.get(commandId) as number]
            .tasks[taskId as TaskId].target === ExecutionTarget.DEVICE
            ? "deviceTask"
            : "hostTask";
        case CommandType.AWAIT_PUSH:
          return "awaitPush";
        case CommandType.PUSH:
          return "push";
        case CommandType.HORIZON:
          return "horizon";
        default:
          return "deviceTask";
      }
    },
    []
  );

  const [tableCommandData, setTableCommandData] = useState<
    Array<TableCommandData>
  >([]);
  const [tableTaskData, setTableTaskData] = useState<Array<TableTaskData>>([]);

  const updateCommandData = useCallback(() => {
    if (
      dependencyInfoRef.current === null ||
      dependencyGraphInfo === null ||
      dependencyGraphInfo.dependencyType !== "Command" ||
      displayNodeRef.current === null
    )
      return;

    let jobFrames =
      timeFrames[dependencyGraphInfo.runId].jobFrames[dependencyGraphInfo.node];
    let newTableData: Array<TableCommandData> = [];
    const depender = dependencyInfoRef.current.depender as Command;
    let dependerJobType: JobType = getJobTypeFromCommand(
      displayNodeRef.current,
      depender.type,
      jobFrames,
      depender.taskId
    );
    newTableData.push({
      key: displayNodeRef.current,
      commandId: displayNodeRef.current,
      type: dependerJobType,
      execRange:
        depender.executionRange !== null
          ? "[" +
            depender.executionRange.min +
            "] - [" +
            depender.executionRange.max +
            "]"
          : null,
      bufferAccess:
        depender.bufferAccesses.length !== 0 ? depender.bufferAccesses : null,
      bufferId: depender.bufferId,
      kernelId: depender.kernelId,
      otherNode: depender.otherNode,
    });
    for (const dependeeTarget of dependencyInfoRef.current.dependees) {
      const dependee = dependeeTarget[1] as Command;
      let depndeeJobType: JobType = getJobTypeFromCommand(
        dependeeTarget[0],
        dependee.type,
        jobFrames,
        dependee.taskId
      );
      newTableData.push({
        key: dependeeTarget[0],
        commandId: dependeeTarget[0],
        bufferId: dependee.bufferId,
        type: depndeeJobType,
        kernelId: dependee.kernelId,
        otherNode: dependee.otherNode,
        execRange:
          dependee.executionRange !== null
            ? "[" +
              dependee.executionRange.min +
              "] - [" +
              dependee.executionRange.max +
              "]"
            : null,
        bufferAccess:
          dependee.bufferAccesses.length !== 0 ? dependee.bufferAccesses : null,
      });
    }

    setTableTaskData([]);
    setTableCommandData(newTableData);
  }, [
    dependencyInfoRef,
    dependencyGraphInfo,
    getJobTypeFromCommand,
    timeFrames,
  ]);

  const updateTaskData = useCallback(() => {
    if (
      dependencyInfoRef.current === null ||
      dependencyGraphInfo === null ||
      dependencyGraphInfo.dependencyType !== "Task" ||
      displayNodeRef.current === null
    ) {
      return;
    }
    let newTableData: Array<TableTaskData> = [];
    const depender = dependencyInfoRef.current.depender as Task;

    newTableData.push({
      key: displayNodeRef.current,
      taskId: displayNodeRef.current,
      kernelId: depender.name.substring(
        depender.name.indexOf("(") + 1,
        depender.name.indexOf(")")
      ),
    });

    for (const dependeeTarget of dependencyInfoRef.current.dependees) {
      const dependee = dependeeTarget[1] as Task;
      newTableData.push({
        key: dependeeTarget[0],
        taskId: dependeeTarget[0],
        kernelId: dependee.name.substring(
          dependee.name.indexOf("(") + 1,
          dependee.name.indexOf(")")
        ),
      });
    }
    setTableCommandData([]);
    setTableTaskData(newTableData);
  }, [dependencyGraphInfo, dependencyInfoRef]);

  const DependencyTable = useMemo((): JSX.Element => {
    if (tableCommandData.length !== 0) {
      return (
        <Table
          columns={tableCommandColumns}
          dataSource={tableCommandData}
          pagination={{ pageSize: 4 }}
        />
      );
    }
    if (tableTaskData.length !== 0) {
      return (
        <Table
          columns={tableTaskColumns}
          dataSource={tableTaskData}
          pagination={{ pageSize: 4 }}
        />
      );
    }
    return <div></div>;
  }, [tableTaskColumns, tableTaskData, tableCommandData, tableCommandColumns]);

  const networkGraphRef = useRef<Network | null>(null);

  const handleGraphClick = useCallback(
    (params) => {
      if (
        params.nodes !== undefined &&
        params.nodes[0] !== undefined &&
        dependencyGraphInfo
      ) {
        displayNodeRef.current = params.nodes[0];
        let depInfo: DependencyInfo = {
          targetId: params.nodes[0],
          dependencyDirection: showDependeesOnClick.current
            ? "Dependee"
            : "Depender",
          inRange: dependencyGraphInfo.inRange,
          node: dependencyGraphInfo.node,
          requestType: dependencyGraphInfo.dependencyType,
          runId: dependencyGraphInfo.runId,
        };
        depInfo.targetId = params.nodes[0];
        dispatch(highlightDependency(depInfo));
        addDependency();
        updateDependencyInfo();
        updateCommandData();
        updateTaskData();
      }
    },
    [
      dependencyGraphInfo,
      dispatch,
      addDependency,
      updateDependencyInfo,
      updateCommandData,
      updateTaskData,
    ]
  );

  /**
   * Build graph.
   */
  useEffect(() => {
    if (dependencyGraphInfo === null || networkGraphRef.current !== null)
      return;
    const options: Options = {
      autoResize: true,
      width: "100%",
      interaction: { selectable: true, tooltipDelay: 100 },
      manipulation: {
        enabled: true,
      },
      nodes: {
        shape: "dot",
        scaling: {
          min: 10,
          max: 30,
          label: {
            min: 8,
            max: 30,
            drawThreshold: 0,
            maxVisible: 30,
          },
        },
        font: {
          size: 18,
          face: "Tahoma",
        },
      },
      edges: {
        width: 2,
        color: { inherit: "from" },
        arrows: {
          to: {
            enabled: true,
          },
        },
        smooth: { enabled: true, type: "continuous", roundness: 1 },
      },
    };
    if (graphDataRef.current !== null) {
      displayNodeRef.current = dependencyGraphInfo.targetId;
      updateDependencyInfo();
      initGraphData();
      updateCommandData();
      updateTaskData();
      networkGraphRef.current = new Network(
        graphRef.current as HTMLElement,
        {
          nodes: graphDataRef.current.nodes,
          edges: graphDataRef.current.edges,
        },
        options
      );
      networkGraphRef.current.on("click", handleGraphClick);
    }
  }, [
    dependencyGraphInfo,
    handleGraphClick,
    timeFrames,
    dispatch,
    initGraphData,
    updateDependencyInfo,
    updateCommandData,
    updateTaskData,
  ]);

  /**
   * Update graphs on new target.
   */
  useLayoutEffect(() => {
    if (dependencyGraphInfo !== null) {
      if (displayNodeRef.current === null)
        displayNodeRef.current = dependencyGraphInfo.targetId;
      updateDependencyInfo();
      if (graphDataRef.current.nodes.length === 0) initGraphData();
      else addDependency();
      updateTaskData();
      updateCommandData();
    } else {
      displayNodeRef.current = null;
    }
  }, [
    dependencyGraphInfo,
    updateDependencyInfo,
    addDependency,
    initGraphData,
    updateCommandData,
    updateTaskData,
  ]);

  /**
   * If requested data arrived, update graphs.
   */
  useEffect(() => {
    if (addDependencyInFuture.current !== null && timeFrames !== null) {
      displayNodeRef.current = addDependencyInFuture.current;
      if (graphDataRef.current.nodes.length === 0) {
        updateDependencyInfo();
        initGraphData();
      } else {
        updateDependencyInfo();
        addDependency();
        updateTaskData();
        updateCommandData();
      }
    }
  }, [
    initGraphData,
    timeFrames,
    addDependency,
    updateDependencyInfo,
    updateCommandData,
    updateTaskData,
  ]);

  /**
   * If user selects different graph, reset everything.
   */
  useLayoutEffect(() => {
    graphDataRef.current.nodes.clear();
    graphDataRef.current.edges.clear();
    graphDataRef.current.target = -1;
    displayNodeRef.current = null;
    graphNodeMapRef.current = new Map();
    networkGraphRef.current = null;
    showDependeesOnClick.current = true;
    addDependencyInFuture.current = null;
  }, [dependencyGraphInfo]);

  const graphLegend = useMemo((): JSX.Element => {
    return (
      <div style={{ marginLeft: 2 }}>
        <div style={{ display: "flex" }}>
          <DottedLine />
          <LegendEntry>Anti Dep</LegendEntry>
        </div>
        <div style={{ display: "flex" }}>
          <ColoredLine
            lineColor={getDependencyColor(DependencyKind.ORDER_DEP)}
          />
          <LegendEntry>Ordered Dep</LegendEntry>
        </div>
        <div style={{ display: "flex" }}>
          <ColoredLine
            lineColor={getDependencyColor(DependencyKind.TRUE_DEP)}
          />
          <LegendEntry>True Dep</LegendEntry>
        </div>
        <div style={{ display: "flex" }}>
          <ColoredLine
            lineColor={getDependencyColor(DependencyKind.UNRECOGNIZED)}
          />
          <LegendEntry>Unrecognized Dep</LegendEntry>
        </div>
      </div>
    );
  }, []);

  if (dependencyGraphInfo === null) return <div></div>;

  return (
    <div style={{ height: 350 }}>
      <Modal
        title="Buffer Accesses"
        centered
        visible={bufferAccessVisible}
        onCancel={() => setBufferAccessVisible(false)}
        footer={[
          <Button key="close" onClick={() => setBufferAccessVisible(false)}>
            Close
          </Button>,
        ]}
      >
        <Table
          columns={tableBufferAccessColumns}
          dataSource={tableBufferAccessData}
        ></Table>
      </Modal>
      <Divider style={{ marginBottom: 5 }} />
      <Row>
        <Col span={10}>
          <div style={{ textAlign: "center" }}>
            <h3 style={{ transform: "translateY(20%)" }}>
              {dependencyGraphInfo?.dependencyType === "Command"
                ? "Command Dependency Graph"
                : "Task Dependency Graph"}
            </h3>
          </div>
        </Col>
        <Col span={14}>
          <div style={{ textAlign: "center" }}>
            <h3 style={{ transform: "translateY(20%)" }}>
              {dependencyGraphInfo?.dependencyType === "Command"
                ? "Command Dependency Table"
                : "Task Dependency Table"}
            </h3>
          </div>
        </Col>
      </Row>
      <Divider style={{ marginTop: 5, marginBottom: 5 }} />
      <Row>
        <Col span={10}>
          <div style={{ display: "flex", position: "relative" }}>
            <div
              style={{
                width: 130,
                height: 110,
                position: "absolute",
                border: "1px solid #aaaaaa",
                right: "0%",
                // transform: "translateX(-100%)",
              }}
            >
              {graphLegend}
            </div>
            <div
              ref={graphRef}
              style={{
                height: 350,
                width: "100%",
                borderStyle: "solid",
                borderWidth: 1,
                borderColor: "#aaaaaa",
              }}
            />
            <div
              style={{
                height: 10,
                position: "absolute",
                background: "white",
                marginLeft: 2,
                marginTop: 2,
                opacity: 0.5,
              }}
            >
              <p>OnClick show: </p>
            </div>
            <Switch
              title="On select show"
              checkedChildren="Dependees"
              unCheckedChildren="Dependers"
              defaultChecked={showDependeesOnClick.current}
              onChange={(isSelected: boolean) => {
                showDependeesOnClick.current = isSelected;
              }}
              style={{ position: "absolute", marginLeft: 90, marginTop: 3 }}
            ></Switch>
          </div>
        </Col>
        <Col span={14}>{DependencyTable}</Col>
      </Row>
      <Divider style={{ marginTop: 5 }} />
      <Row>
        <Button
          danger
          style={{ left: "95%", transform: "translateX(-100%)" }}
          onClick={() => {
            dispatch(showDependencyGraph(null));
          }}
        >
          Close Dependency View
        </Button>
      </Row>
      <Divider style={{ marginTop: 5 }} />
    </div>
  );
}
