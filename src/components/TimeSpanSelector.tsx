import React, { useMemo } from "react";
import styled from "styled-components";

import { getSelectionColor } from "../colorCoding";
import { Point, TimeSpan } from "../shared/types";
import { SelectionType } from "./TimelineMouseHandler";

const SelectionArea = styled.div.attrs<{
  position: TimeSpan;
  height: number;
  color: string;
}>((props) => ({
  style: {
    marginLeft: props.position.start,
    height: props.height,
    width: props.position.end - props.position.start,
    backgroundColor: props.color,
  },
}))<{ position: TimeSpan; height: number; color: string }>`
  position: absolute;
  color: black;
  pointer-events: none;
  opacity: 0.2;
  transform: translateY(-105%);
`;

interface Props {
  viewSize: Point;
  selectionTimeSpan: TimeSpan;
  selectionType: SelectionType;
}

/**
 * Highlights selected area by mouse of the timeline.
 *
 * @param props
 */
export default function TimeSpanSelector(props: Props) {
  const highlightColor = useMemo(() => {
    return getSelectionColor(props.selectionType);
  }, [props.selectionType]);

  if (props.selectionTimeSpan.end === 0) return <div></div>;

  return (
    <SelectionArea
      id="selectionEnd"
      height={props.viewSize.y}
      position={props.selectionTimeSpan}
      color={highlightColor}
      style={{
        visibility: props.selectionTimeSpan.end !== 0 ? "visible" : "hidden",
      }}
    />
  );
}
