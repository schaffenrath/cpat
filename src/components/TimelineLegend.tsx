import React from "react";
import styled from "styled-components";

import { getHEXColorOfType } from "../colorCoding";
import { CmdValuePerType } from "../shared/types";

const LegendContainer = styled.div`
  display: flex;
  position: relative;
  margin-left: 70px;
  top: 0;
  border: 2px solid black;
  width: 500px;
  height: 25px;
  background: #ffff;
  z-index: 1;
`;

const LegendEntryColorSample = styled.div.attrs<{ sampleColor: string }>(
  (props) => ({
    style: {
      background: props.sampleColor,
    },
  })
)<{ sampleColor: string }>`
  display: inline-block;
  width: 10px;
  height: 10px;
  margin-top: 0.2em;
  margin-left: 1.5em;
`;

const LegendEntry = styled.div`
  display: inline-block;
  margin-left: 0.5em;
  margin-top: 0.2em;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 12px;
`;

export default function TimelineLegend() {
  const jobType = ["hostTask", "deviceTask", "push", "awaitPush", "horizon"];
  const jobTypeName = [
    "Host Task",
    "Device Task",
    "Push",
    "Await Push",
    "Horizon",
  ];

  const legendEntries: Array<JSX.Element> = [];
  const typeCount = jobType.length;
  for (let i = 0; i < typeCount; i++) {
    legendEntries.push(
      <div key={i}>
        <LegendEntryColorSample
          sampleColor={getHEXColorOfType(jobType[i] as keyof CmdValuePerType)}
        ></LegendEntryColorSample>
        <LegendEntry>{jobTypeName[i]}</LegendEntry>
      </div>
    );
  }
  return <LegendContainer>{legendEntries}</LegendContainer>;
}
