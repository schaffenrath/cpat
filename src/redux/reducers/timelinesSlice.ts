import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import {
  CommandId,
  NodeId,
  TaskId,
  TimeSpan,
  DependencyInfo,
  DependencyType,
} from "../../shared/types";

type RunId = string;
type TimelineId = string;

export interface RunOfTimeline {
  timelineId: TimelineId;
  runId: RunId;
}

export interface TimeSpanOfTimeline {
  timelineId: string;
  timeSpan: TimeSpan;
}

export interface TimeSpanOfNode {
  nodeId: NodeId;
  timeSpan: TimeSpan;
}

export interface DisplayDependencyInfo {
  runId: RunId;
  targetId: CommandId | TaskId;
  dependencyType: DependencyType;
  inRange: TimeSpan;
  node: NodeId;
}

export interface TimelineState {
  displayedRuns: Array<RunOfTimeline>;
  linkedTimeSpan: TimeSpanOfTimeline;
  linkedViews: boolean;
  showJobModal: boolean;
  showJobHighlight: boolean;
  highlightDependency: DependencyInfo | null;
  highlightTimeSpan: TimeSpanOfNode | null;
  showDependencyGraph: DisplayDependencyInfo | null;
}

let initialState: TimelineState = {
  displayedRuns: [],
  linkedTimeSpan: { timelineId: "", timeSpan: { start: 0, end: 0 } },
  linkedViews: false,
  showJobModal: false,
  showJobHighlight: false,
  highlightDependency: null,
  highlightTimeSpan: null,
  showDependencyGraph: null,
};

const timelinesSlice = createSlice({
  name: "timeline",
  initialState,
  reducers: {
    setDisplayedRun(state, action: PayloadAction<RunOfTimeline>) {
      const foundIdx = state.displayedRuns.findIndex(
        (timeline) => timeline.timelineId === action.payload.timelineId
      );
      if (foundIdx === -1)
        state.displayedRuns.push({
          timelineId: action.payload.timelineId,
          runId: action.payload.runId,
        });
      else state.displayedRuns[foundIdx].runId = action.payload.runId;
    },
    removeDisplayedRun(state, action: PayloadAction<TimelineId>) {
      const foundIdx = state.displayedRuns.findIndex(
        (timeline) => timeline.timelineId === action.payload
      );
      if (foundIdx === -1)
        throw new Error(`Run, that should be removed is not found.`);
      state.displayedRuns.splice(foundIdx, 1);
    },
    clearDisplayedRuns(state) {
      state.displayedRuns = [];
    },
    setLinkedTimeSpan(state, action: PayloadAction<TimeSpanOfTimeline>) {
      state.linkedTimeSpan = action.payload;
    },
    toggleViewLink(state) {
      state.linkedViews = !state.linkedViews;
    },
    setJobModalVisibility(state, action: PayloadAction<boolean>) {
      state.showJobModal = action.payload;
    },
    setJobHighlight(state, action: PayloadAction<boolean>) {
      state.showJobHighlight = action.payload;
    },
    setHighlightTimeSpan(state, action: PayloadAction<TimeSpanOfNode | null>) {
      state.highlightTimeSpan = action.payload;
    },
    showDependencyGraph(
      state,
      action: PayloadAction<DisplayDependencyInfo | null>
    ) {
      state.showDependencyGraph = action.payload;
    },
    highlightDependency(state, action: PayloadAction<DependencyInfo | null>) {
      state.highlightDependency = action.payload;
    },
  },
});

export const {
  setDisplayedRun,
  removeDisplayedRun,
  clearDisplayedRuns,
  setLinkedTimeSpan,
  toggleViewLink,
  setJobModalVisibility,
  setHighlightTimeSpan,
  showDependencyGraph,
  highlightDependency,
} = timelinesSlice.actions;
export default timelinesSlice.reducer;
