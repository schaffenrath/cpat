import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import config from "../../shared/config";

import {
  CelerityRun,
  RunDict,
  RunId,
  RunIdWithName,
  TimeSpan,
  StatisticPackage,
  SampledFramesOfNode,
  JobFramesOfNode,
  JobFramePayload,
  SampledFramePayload,
} from "../../shared/types";

type NameDict = { [key: string]: string };
type CountDict = { [key: string]: number };
type BaseFrameLengthDict = { [key: string]: number };

export type FrameSampleLevelDict = { [key: number]: SampledFramesOfNode };

/**
 * Determine the target sample level based on the view span and the timeFrame length.
 *
 * @param viewSpan The given view span.
 * @param baseFrameLength The length of all timeFrames.
 */
export function getSampleLevel(viewSpan: TimeSpan, baseFrameLength: number) {
  let sampleLevel = baseFrameLength;
  if (config.DISABLE_SAMPLING) return sampleLevel;
  while (sampleLevel * 2 < viewSpan.end - viewSpan.start) sampleLevel *= 2;
  return sampleLevel;
}

export interface TimeFrames {
  jobFrames: JobFramesOfNode;
  sampleLevels: FrameSampleLevelDict;
}

type TimeFramesOfRun = { [key: string]: TimeFrames };

// Arrays are used here instead of Maps, since redux recommends arrays for better debugging
interface CelerityRunState {
  selectedMainRun: RunId;
  nameCount: CountDict;
  availableRuns: NameDict;
  mostRecentRuns: Array<RunId>;
  runs: RunDict;
  timeFrames: TimeFramesOfRun;
  baseFrameLength: BaseFrameLengthDict;
  statistics: StatisticPackage | null;
}

const initialState: CelerityRunState = {
  selectedMainRun: "",
  nameCount: {},
  availableRuns: {},
  mostRecentRuns: [],
  runs: {},
  timeFrames: {},
  baseFrameLength: {},
  statistics: null,
};

const celerityRunSlice = createSlice({
  name: "celerityRun",
  initialState,
  reducers: {
    setMainRun(state, action: PayloadAction<RunId>) {
      state.selectedMainRun = action.payload;
    },
    setAvailableRuns(state, action: PayloadAction<Array<RunIdWithName>>) {
      // Add incremental ID to run name to make runs distinguishable.
      action.payload.forEach((availRun) => {
        let alreadySaved = state.availableRuns[availRun.id];
        if (alreadySaved === undefined) {
          if (Object.keys(state.runs).length !== 0)
            state.mostRecentRuns.push(availRun.id);
          const availNameCount = state.nameCount[availRun.name];
          if (availNameCount === undefined) {
            state.availableRuns[availRun.id] = availRun.name.concat(" #0");
            state.nameCount[availRun.name] = 1;
          } else {
            state.availableRuns[availRun.id] = availRun.name.concat(
              " #" + availNameCount
            );
            state.nameCount[availRun.name] += 1;
          }
        }
      });
    },
    clearMostRecentRuns(state) {
      state.mostRecentRuns = [];
    },
    addRunInfo(state, action: PayloadAction<CelerityRun>) {
      state.runs[action.payload.id] = action.payload;
    },
    deleteRun(state, action: PayloadAction<RunId>) {
      // If run is selectedMainRun, reset it
      if (state.selectedMainRun === action.payload) state.selectedMainRun = "";
      delete state.availableRuns[action.payload];
      delete state.runs[action.payload];
      delete state.baseFrameLength[action.payload];
      delete state.timeFrames[action.payload];
    },
    setRunName(state, action: PayloadAction<RunIdWithName>) {
      state.availableRuns[action.payload.id] = action.payload.name;
    },
    addJobFrames(state, action: PayloadAction<JobFramePayload>) {
      const runId = action.payload.runId;
      // Init if first data received.
      if (state.timeFrames[runId] === undefined) {
        state.timeFrames[runId] = {
          jobFrames: {},
          sampleLevels: {},
        };
        state.baseFrameLength[runId] = action.payload.baseFrameLength;
      }
      const newFrames = action.payload.framesPerNode;
      // Add received timeFrames to already stored set.
      for (const nodeId in newFrames) {
        if (state.timeFrames[runId].jobFrames[nodeId] === undefined) {
          state.timeFrames[runId].jobFrames[nodeId] = {};
        }
        for (const frameStartTime in newFrames[nodeId]) {
          state.timeFrames[runId].jobFrames[nodeId][frameStartTime] =
            newFrames[nodeId][frameStartTime];
        }
      }
    },
    addSampledFrames(state, action: PayloadAction<SampledFramePayload>) {
      const runId = action.payload.runId;
      // Init if first data received.
      if (state.timeFrames[runId] === undefined) {
        state.timeFrames[runId] = {
          jobFrames: {},
          sampleLevels: {},
        };
        state.baseFrameLength[runId] = action.payload.baseFrameLength;
      }
      const newFrames = action.payload.framesPerNode;
      const sampleLevel = action.payload.frameLength;
      // Add received sampledFrames to already stored set.
      for (const nodeId in newFrames) {
        if (state.timeFrames[runId].sampleLevels[sampleLevel] === undefined) {
          state.timeFrames[runId].sampleLevels[sampleLevel] = {};
        }
        if (
          state.timeFrames[runId].sampleLevels[sampleLevel][nodeId] ===
          undefined
        ) {
          state.timeFrames[runId].sampleLevels[sampleLevel][nodeId] = {};
        }
        for (const frameStartTime in newFrames[nodeId]) {
          state.timeFrames[runId].sampleLevels[sampleLevel][nodeId][
            frameStartTime
          ] = newFrames[nodeId][frameStartTime];
        }
      }
    },
    setStatistics(state, action: PayloadAction<StatisticPackage>) {
      state.statistics = action.payload;
    },
  },
});

export const {
  setMainRun,
  setAvailableRuns,
  clearMostRecentRuns,
  addRunInfo,
  deleteRun,
  setRunName,
  addJobFrames,
  addSampledFrames,
  setStatistics,
} = celerityRunSlice.actions;
export default celerityRunSlice.reducer;
