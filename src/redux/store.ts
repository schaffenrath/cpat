import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";

import celerityRunReducer from "./reducers/celerityDataSlice";
import timelineReducer from "./reducers/timelinesSlice";

const store = configureStore({
  reducer: {
    celerityRun: celerityRunReducer,
    timeline: timelineReducer,
  },
  middleware: getDefaultMiddleware({
    serializableCheck: false,
    /**
     * Comment in the following line for visualizing larger runs in debug mode!
     * Improves store speed significantly for time frames!
     */
    immutableCheck: false,
  }),
});
export default store;
export type RootState = ReturnType<typeof store.getState>;
