export const vertexShader = `
      // Defined the possible colors of jobs to avoid unnecessary transfer of color data
      // TODO: Assign color to constant variable for readability!
      // typeColors = [deviceTaskColor, hostTaskColor, pushColor, awaitPushColor]
      const vec3 typeColors[4] = vec3[4](vec3(0.4,0.49411764,0.9176470),vec3(0.40392157,0.8470588,0.4078431),vec3(0.96862745,0.23921569,0.156862745),vec3(0.980392,0.76827451,0.447058823));
      attribute float size;
      attribute int customColor;
      attribute float opacity;

      varying vec3 vColor;
      varying float vOpacity;

      void main() {

        // If multiple commands were collapsed because their length in the visualization 
        // would be smaller then a single pixel, the type color of the two longest commands are mixed
        if (customColor > 3) {
          int b = customColor / (256*256) ;
          int g = (customColor - b*256*256)/ 256;
          int r = customColor - b*256*256 - g*256;
          vColor = vec3(float(r)/255.0f,float(g)/255.0f,float(b)/255.0f);
        } else {
          vColor = typeColors[customColor];
        }

        vOpacity = opacity;
        gl_Position = projectionMatrix * viewMatrix * instanceMatrix * vec4(position, 1.0);
      }`;

export const fragmentShader = `
      varying vec3 vColor;
      varying float vOpacity;

      void main() {
        gl_FragColor = vec4(vColor, vOpacity);
      }
`;
