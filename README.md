## Setup

This project was created with [React](https://reactjs.org/), [NodeJS](https://nodejs.org/en/), [yarn](https://yarnpkg.com/) and [protobuf](https://developers.google.com/protocol-buffers/). Yarn allows you to install most necessary dependencies by simply running `yarn` in top directory.

Protobuf is not handled by `yarn` and has to be installed by the user. It can be downloaded here: [https://developers.google.com/protocol-buffers](https://developers.google.com/protocol-buffers/).

If you work on Windows, you might encounter the following error message: `protoc-gen-ts_proto: %1 is not a valid Win32 application`. To fix this open `package.json` and append `.cmd` to `.../.bin/protoc-gen-ts_proto` in line `51`.

## Start Application

Currently both React and NodeJS have to be started manually via command line. The following commands must be used.

### Start React

First run `yarn run start_react`. This will start a webserver in development mode and automatically open a window from your default
browser. If this is not the case, one can access the the view via [http://localhost:3000](http://localhost:3000).

### Start NodeJS

By running `yarn run start_node` you start the NodeJS server, which waits for a connection from the Celerity Runtime and forwards the received data to the React server.

## Run a Celerity Program

Celerity allows to export the collected trace in two different ways

#### Export Trace via FILE

Define the environment variable `CELERITY_TRACE_EXPORT=file://<filename>`. Celerity will create a trace file for each node. These files can simply by concatinated by just using `cat` (e.g: `cat trace_0 trace_1 trace_2 > trace`). Afterwards the single trace file can be dropped in the browser [http://localhost:3000](http://localhost:3000).

#### Export Trace via TCP Socket

Define the environment variable `CELERITY_TRACE_EXPORT=tcp://<ip-address>`.
The Celerity Runtime should automatically create a socket, connecting to NodeJS and transferring the collected tracing data. Afterwards the data should be displayed in the browser [http://localhost:3000](http://localhost:3000).
If you want to run Celerity on a different system and use SSH tunneling, make sure to forward the correct port `4503`. This port can also be changed in the `config.ts` file.

## Sampling

The implemented sampling algorithm has only been tested on the available applications and can therefore vary in the quality. In the `config.ts` file, three sampling constants are defined that can be modified to adjust the displayed sampling result.

| Sampling Constant          | Effect                                                                                                                                                                                           |
| -------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| SAMPLE_CONSTANT            | Describes how much information should be preserved in sampled result. A lower value means lower resolution". (Default: 2000)                                                                     |
| SAMPLE_FACTOR              | Amplifies the effect of the `SAMPLE_CONSTANT`. A lower value menas higher resolution (opposite of `SAMPLE_CONSTATN`). Was mainly introduced for development. Will be deleted maybe. (Default: 1) |
| SAMPLE_MIN_EXEC_TIME_RATIO | Describes how much IDLE time is allowed, when collapsing multiple jobs into a single sampled job. It is defined as a fraction (values from 0 to 1). (Default: 0.6)                               |
